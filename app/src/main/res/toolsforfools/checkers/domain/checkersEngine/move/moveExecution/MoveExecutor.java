package com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

public abstract class MoveExecutor {
    public abstract void executeMove(Move move);
}
