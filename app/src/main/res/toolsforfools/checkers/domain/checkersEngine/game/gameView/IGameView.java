package com.toolsforfools.checkers.domain.checkersEngine.game.gameView;

import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

public interface IGameView extends IPlayerView, IBoardView {
    void displayMove(Position oldPosition, Position newPosition);
    void removePiece(Position position);
    void upgradePiece(Position position);
}
