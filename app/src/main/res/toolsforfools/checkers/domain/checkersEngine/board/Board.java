package com.toolsforfools.checkers.domain.checkersEngine.board;


import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IBoardView;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.util.ArrayList;
import java.util.List;

public class Board {

    /** this class is 1 based indexed
     * always the white pieces is on top of the board and black pieces in the bottom
     *
     *
     * @param hight  the hieght of the board
     * @param widht  the width of the board
     * @param occupiedLines  to determine the number of occupied lines when the game is first initialized
     */

    private int hight = 8;
    private int width = 8;
    private int occupiedLines = 3;   //

    private Square[][] mBoard;

    private IBoardView boardView;

    public Board(IBoardView boardView) {
        this.boardView = boardView;
        initBoard();
    }

    public Board(int hight, int width, int occupiedLines,
                 IBoardView boardView) {
        this.hight = hight;
        this.width = width;
        this.occupiedLines = occupiedLines;
        this.boardView = boardView;
        initBoard();
    }

    private void initBoard(){
        mBoard = new Square[hight + 1][width + 1];

        for (int i = 1; i <= hight; i++){
            for (int j = 1; j <= width; j++){
                mBoard[i][j] = new Square(new Position(i, j));
            }
        }

        initWhitePieces();
        initBlackPieces();
    }



    private void initWhitePieces(){
        for(int i = 1; i <= occupiedLines; i ++){
            int n = (i % 2 == 0) ? 2 : 1;      // deciding from which colomn should start filling
            for(int j = n ; j <= width; j += 2){
                Position position = new Position(i , j);
                Piece piece = new Piece(position, PieceType.whitePiece);
                Square square = new Square(position, piece);

                mBoard[i][j] = square;
                boardView.putPieceInPosition(piece.getType(), piece.getPosition());
            }
        }
    }


    private void initBlackPieces(){
        for(int i = hight; i > hight - occupiedLines; i --){
            int n = (i % 2 == 0) ? 2 : 1;
            for(int j = n; j <= width; j += 2){
                Position position = new Position(i, j);
                Piece piece = new Piece(position, PieceType.blackPiece);
                Square square = new Square(position, piece);

                mBoard[i][j] = square;
                boardView.putPieceInPosition(piece.getType(), piece.getPosition());
            }
        }
    }

    public List<Piece> getPlayerPieces(Player.PlayerType playerType){
        List<Piece> pieceList = new ArrayList<>();
        for(int i = 1; i <= hight ; i++){
            for(int j = 1; j <= width; j++){
                Square square = mBoard[i][j];
                if (square.getPiece() != null) {
                    switch (playerType) {
                        case White: {
                            if (square.getPiece().getType() == PieceType.whitePiece || square.getPiece().getType() == PieceType.whiteKing){
                                pieceList.add(square.getPiece());
                            }
                            break;
                        }

                        case Black: {
                            if (square.getPiece().getType() == PieceType.blackPiece || square.getPiece().getType() == PieceType.blackKing){
                                pieceList.add(square.getPiece());
                            }
                            break;
                        }
                    }
                }
            }
        }
        return pieceList;

    }

    public Piece getPiece(Position position){
        if (!isValidPosition(position)) return null;
        return mBoard[position.getX()][position.getY()].getPiece();
    }

    private boolean isValidPosition(Position position){
       return position.getX() <= hight && position.getX() >= 1 && position.getY() <= width && position.getY() >= 1;
    }


    public Piece getWhitePiece(Position position){
        if (isValidPosition(position)){
            Piece piece = mBoard[position.getX()][position.getY()].getPiece();
            if (piece != null && (piece.getType() == PieceType.whitePiece || piece.getType() == PieceType.whiteKing)){
                return piece;
            }
        }
        return null;
    }

    public Piece getBlackPiece(Position position){
        if (isValidPosition(position)){
            Piece piece = mBoard[position.getX()][position.getY()].getPiece();
            if (piece != null && (piece.getType() == PieceType.blackPiece || piece.getType() == PieceType.blackKing)){
                return piece;
            }
        }
        return null;
    }

    public boolean canMove(Position position){
        return isValidPosition(position) && mBoard[position.getX()][position.getY()].getPiece() == null;
    }

    public void removePiece(Position position){
        mBoard[position.getX()][position.getY()].setPiece(null);
    }

    public void addPiece(Piece piece, Position position){
        mBoard[position.getX()][position.getY()].setPiece(piece);
    }

    public boolean isReachedTheEnd(Piece piece){
        switch (piece.getType()){
            case whitePiece:
            case whiteKing:{
                if (piece.getPosition().getX() == hight){
                    return true;
                }
                break;
            }

            case blackKing:
            case blackPiece:{
                if (piece.getPosition().getX() == 1){
                    return true;
                }
                break;
            }
        }
        return false;
    }
}
