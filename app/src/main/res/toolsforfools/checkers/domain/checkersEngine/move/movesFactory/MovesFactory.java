package com.toolsforfools.checkers.domain.checkersEngine.move.movesFactory;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.util.List;

public interface  MovesFactory  {
     List<Move> getAvailableMoves(List<Piece> pieceList);
     List<Move> getAvailableMoves(Piece piece);
}
