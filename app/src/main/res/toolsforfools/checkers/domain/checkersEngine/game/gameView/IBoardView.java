package com.toolsforfools.checkers.domain.checkersEngine.game.gameView;

import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;

public interface IBoardView {
    void putPieceInPosition(PieceType pieceType, Position position);
}
