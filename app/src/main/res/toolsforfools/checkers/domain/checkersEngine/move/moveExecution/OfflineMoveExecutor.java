package com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

public class OfflineMoveExecutor extends MoveExecutor {
    private MoveExecutionListener moveExecutionListener;
    private Board board;

    public OfflineMoveExecutor(MoveExecutionListener moveExecutionListener, Board board) {
        this.moveExecutionListener = moveExecutionListener;
        this.board = board;
    }

    @Override
    public void executeMove(Move move) {
        final Move moveCopy;
        if (move instanceof EatMove){
            moveCopy = new EatMove((EatMove) move);
        }else {
            moveCopy = new Move(move);
        }

        board.removePiece(move.getSourcePiece().getPosition());
        board.addPiece(move.getSourcePiece(), move.getDestination());
        move.getSourcePiece().setPosition(move.getDestination());

        boolean isUpgraded = false;
        if (board.isReachedTheEnd(move.getSourcePiece())){
            switch (move.getSourcePiece().getType()){
                case whitePiece:{
                    move.getSourcePiece().setType(PieceType.whiteKing);
                    isUpgraded = true;
                    break;
                }

                case blackPiece:{
                    move.getSourcePiece().setType(PieceType.blackKing);
                    isUpgraded = true;
                    break;
                }
            }
        }

        if (move instanceof EatMove){
            board.removePiece(((EatMove) move).getVictemPiece().getPosition());
            moveExecutionListener.removePieceFromPlayer((((EatMove) move).getVictemPiece().getType() == PieceType.whiteKing || ((EatMove) move).getVictemPiece().getType() == PieceType.whitePiece ) ?
                            Player.PlayerType.White : Player.PlayerType.Black
                    , ((EatMove) move).getVictemPiece());
        }

        moveExecutionListener.onMoveExecuted(moveCopy);
        if (isUpgraded){
            moveExecutionListener.onPieceUpgraded(move.getSourcePiece());
        }
    }
}
