package com.toolsforfools.checkers.domain.checkersEngine.board;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;

public class Square {
    private Position mPosition;
    private Piece mPiece;

    public Square(Position mPosition, Piece mPiece) {
        this.mPosition = mPosition;
        this.mPiece = mPiece;
    }

    public Square(Position mPosition) {
        this.mPosition = mPosition;
    }

    public Position getmPosition() {
        return mPosition;
    }

    public Piece getPiece() {
        return mPiece;
    }

    public void setPiece(Piece piece) {
        this.mPiece = piece;
    }
}
