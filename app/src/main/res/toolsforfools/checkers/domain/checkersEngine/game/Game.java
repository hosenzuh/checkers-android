package com.toolsforfools.checkers.domain.checkersEngine.game;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutionListener;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.OfflineMoveExecutor;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFactory.MovesFactory;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFactory.StandardCheckersMoveFactory;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFilter.EatMovesFilter;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFilter.IMovesFilter;
import com.toolsforfools.checkers.domain.checkersEngine.player.HumanPlayer;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.util.List;

public class Game {

    private Board board;
    private Player whitePlayer;
    private Player blackPlayer;

    private Player currentPlayer;
    private GameSettings gameSettings;

    private TurnManger turnManger;

    private IGameView gameView;


    public Game(GameSettings gameSettings, IGameView gameView) {
        this.gameSettings = gameSettings;
        this.gameView = gameView;
    }

    public Game(IGameView gameView) {
        this.gameView = gameView;
        gameSettings = new GameSettings();
    }

    public void startGame(){
        turnManger = new OfflineTurnManger();
        turnManger.start();
    }

    public List<Move> getPieceMoves(Position position){
        for (Piece piece : currentPlayer.getPlayerPieces()){
            if (piece.getPosition().equals(position)){
                return piece.getMoveList();
            }
        }
        return null;
    }

    public void selectMove(Move selectedMove){
        currentPlayer.chooseMove(selectedMove);
    }

    public static abstract class TurnManger implements MoveExecutionListener {
        protected MoveExecutor moveExecutor;

        public TurnManger() {
            setUpGame();
        }

        protected abstract void setUpGame();
        protected abstract List<Move> getCurrentPlayerMoves();
        protected abstract void startNewTurn();
        protected abstract boolean checkWin();
        protected abstract void start();

    }


    public class OfflineTurnManger extends TurnManger{
        private MovesFactory movesFactory;
        private IMovesFilter movesFilter;

        @Override
        protected void setUpGame() {
            board = new Board(gameSettings.getBoardHeight(), gameSettings.getBoardWidth(),
                    gameSettings.getOccupiedLines(), gameView);
            moveExecutor = new OfflineMoveExecutor(this, board);
            movesFactory = new StandardCheckersMoveFactory(board);
            whitePlayer = new HumanPlayer(moveExecutor, Player.PlayerType.White,board.getPlayerPieces(Player.PlayerType.White) );
            blackPlayer = new HumanPlayer(moveExecutor, Player.PlayerType.Black, board.getPlayerPieces(Player.PlayerType.Black));
            movesFilter = new EatMovesFilter();

        }

        @Override
        public void onMoveExecuted(Move move) {
            gameView.displayMove(move.getSourcePiece().getPosition(), move.getDestination(), moveAfterExecution);
            currentPlayer.updatePosition(move.getSourcePiece(), move.getDestination());
            if (move instanceof EatMove){
            move.getSourcePiece().setPosition(move.getDestination());
            List<Move> moveList = movesFilter.filter(movesFactory.getAvailableMoves(move.getSourcePiece()));
            if (moveList != null && moveList.size() > 0) {
                currentPlayer.showMoves(moveList);
            }else {
                startNewTurn();
            }
            }else {
                startNewTurn();
            }
        }

        @Override
        public void removePieceFromPlayer(Player.PlayerType playerType, Piece piece) {
            gameView.removePiece(piece.getPosition());
            switch (playerType){
                case White:{
                    whitePlayer.removePiece(piece);
                    break;
                }
                case Black:{
                    blackPlayer.removePiece(piece);
                }
            }
        }

        @Override
        public void onPieceUpgraded(Piece piece) {
            gameView.upgradePiece(piece.getPosition());
        }

        @Override
        protected List<Move> getCurrentPlayerMoves() {
            List<Move> movesList = movesFactory.getAvailableMoves(currentPlayer.getPlayerPieces());
            List<Move> filteredList = movesFilter.filter(movesList);
            if (filteredList != null && filteredList.size() > 0) return filteredList;
            return movesList;
        }

        @Override
        protected void startNewTurn() {
            if (checkWin()){
                gameView.displayPlayerWin();
                return;
            }
            if (currentPlayer.getPlayerType() == Player.PlayerType.White){
                currentPlayer = blackPlayer;
            }else {
                currentPlayer = whitePlayer;
            }
            currentPlayer.showMoves(getCurrentPlayerMoves());
            gameView.startPlayerTurn(currentPlayer.getPlayerType());
        }

        @Override
        protected void start() {
            switch (gameSettings.getFirstTurn()){
                case Black:{
                    currentPlayer = blackPlayer;
                    break;
                }

                case White:{
                    currentPlayer = whitePlayer;
                    break;
                }
            }

            currentPlayer.showMoves(getCurrentPlayerMoves());
            gameView.startPlayerTurn(currentPlayer.getPlayerType());

        }

        @Override
        protected boolean checkWin() {
            if (currentPlayer.getPlayerType() == Player.PlayerType.White){
                return blackPlayer.getPlayerPieces().size() == 0;
            }else {
                return whitePlayer.getPlayerPieces().size() == 0;
            }

        }

    }


    public enum GameType{
        offlineMultiPlayer
    }

}
