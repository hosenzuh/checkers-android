package com.toolsforfools.checkers.domain.checkersEngine.board;

public enum PieceType {
    blackPiece,
    blackKing,
    whitePiece,
    whiteKing

}
