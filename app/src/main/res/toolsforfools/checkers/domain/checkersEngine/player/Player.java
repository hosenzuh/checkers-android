package com.toolsforfools.checkers.domain.checkersEngine.player;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Player {
    public MoveExecutor moveExecutor;
    private PlayerType playerType;
    private List<Piece> playerPieces;

    public Player(MoveExecutor moveExecutor, PlayerType playerType,
                  List<Piece> playerPieces) {
        this.moveExecutor = moveExecutor;
        this.playerType = playerType;
        this.playerPieces = playerPieces;
    }

    public void showMoves(List<Move> availableMoves){
        setMovesToPieces(availableMoves);
        makeMove(availableMoves);
    }

    private void setMovesToPieces(List<Move> availableMoves){
        for (Piece piece : playerPieces){
            List<Move> pieceMoves = new ArrayList<>();
            for (Move move : availableMoves){
                if (piece.getPosition().equals(move.getSourcePiece().getPosition())){
                    pieceMoves.add(move);
                }
            }
            piece.setMoveList(pieceMoves);
        }
    }

    public List<Piece> getPlayerPieces() {
        return playerPieces;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void chooseMove(Move move){
        moveExecutor.executeMove(move);
    }

    public void removePiece(Piece piece){
        for (Piece playerPiece : playerPieces){
            if (playerPiece.getPosition().equals(piece.getPosition())){
                playerPieces.remove(piece);
                break;
            }
        }
    }

    public void updatePosition(Piece piece, Position newPosition){
        for (Piece playerPiece : playerPieces){
            if (piece.getPosition().equals(playerPiece.getPosition())){
                playerPiece.setPosition(newPosition);
            }
        }
    }

    protected abstract void makeMove(List<Move> availableMoves);

    public enum PlayerType{
        White,
        Black
    }
}
