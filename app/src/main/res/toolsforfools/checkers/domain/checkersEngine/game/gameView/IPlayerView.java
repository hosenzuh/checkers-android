package com.toolsforfools.checkers.domain.checkersEngine.game.gameView;

import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

public interface IPlayerView {
    void startPlayerTurn(Player.PlayerType playerType);
    void displayPlayerWin();
}
