package com.toolsforfools.checkers.domain.checkersEngine.player;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.util.List;

public class BotPlayer extends Player{
    private Board board;

    public BotPlayer(MoveExecutor moveExecutor,
                     PlayerType playerType, List<Piece> playerPieces, Board board) {
        super(moveExecutor, playerType, playerPieces);
        this.board = board;
    }

    @Override
    protected void makeMove(List<Move> availableMoves) {

    }
}
