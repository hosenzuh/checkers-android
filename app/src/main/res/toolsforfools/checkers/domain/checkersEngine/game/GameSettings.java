package com.toolsforfools.checkers.domain.checkersEngine.game;

import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

public class GameSettings{
    private static int boardWidth = 8;
    private static int boardHeight = 8;
    private static int occupiedLines = 3;
    private static Player.PlayerType firstTurn = Player.PlayerType.White;
    private static Game.GameType gameType = Game.GameType.offlineMultiPlayer;

     GameSettings(){
    }

    private void setBoardWidth(int boardWidth) {
        GameSettings.boardWidth = boardWidth;
    }

    private void setBoardHeight(int boardHeight) {
        GameSettings.boardHeight = boardHeight;
    }

    private void setOccupiedLines(int occupiedLines) {
        GameSettings.occupiedLines = occupiedLines;
    }

    private void setFirstTurn(Player.PlayerType firstTurn) {
        GameSettings.firstTurn = firstTurn;
    }

    public void setGameType(Game.GameType gameType) {
        GameSettings.gameType = gameType;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public int getOccupiedLines() {
        return occupiedLines;
    }

    public Player.PlayerType getFirstTurn() {
        return firstTurn;
    }

    public static class Builder{

        private GameSettings gameSettings = new GameSettings();

        public Builder setBoardWidth(int width){
            gameSettings.setBoardWidth(width);
            return this;
        }

        public Builder setBoardHeight(int height){
            gameSettings.setBoardHeight(height);
            return this;
        }

        public Builder setOccupiedLines(int occupiedLines){
            gameSettings.setOccupiedLines(occupiedLines);
            return this;
        }

        public Builder setFirstTurnPlayer(Player.PlayerType player){
            gameSettings.setFirstTurn(player);
            return this;
        }

        public Builder gameType(Game.GameType gameType){
            gameSettings.setGameType(gameType);
            return this;
        }

        public GameSettings build(){
            return gameSettings;
        }
    }
}