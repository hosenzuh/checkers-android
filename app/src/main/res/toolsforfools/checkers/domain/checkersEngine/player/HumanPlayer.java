package com.toolsforfools.checkers.domain.checkersEngine.player;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;

import java.util.List;

public class HumanPlayer extends Player{
    public HumanPlayer(MoveExecutor moveExecutor, PlayerType playerType,
                       List<Piece> playerPieces) {
        super(moveExecutor, playerType, playerPieces);
    }

    @Override
    protected void makeMove(List<Move> availableMoves) {

    }
}
