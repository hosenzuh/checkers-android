package com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

public interface MoveExecutionListener {
    void onMoveExecuted(Move move);
    void removePieceFromPlayer(Player.PlayerType playerType, Piece piece);
    void onPieceUpgraded(Piece piece);
}
