package com.toolsforfools.checkers.domain.checkersEngine.move.movesFilter;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.util.ArrayList;
import java.util.List;

public class EatMovesFilter implements IMovesFilter {
    @Override
    public List<Move> filter(List<Move> moveList) {
        if (moveList == null) return null;
        List<Move> filteredList = new ArrayList<>();
        for (Move move : moveList){
            if (move instanceof EatMove){
                filteredList.add(move);
            }
        }
        return filteredList;
    }
}
