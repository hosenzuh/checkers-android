package com.toolsforfools.checkers.domain.checkersEngine.board;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.util.List;

public class Piece {
    private Position mPosition;
    private PieceType mType;
    private List<Move> moveList;

    public Piece(Position mPosition, PieceType mType) {
        this.mPosition = mPosition;
        this.mType = mType;
    }

    public Piece(Piece piece){
        this.mPosition = new Position(piece.getPosition());
        this.mType = piece.getType();
        this.moveList = piece.getMoveList();
    }

    public void setMoveList(List<Move> moveList) {
        this.moveList = moveList;
    }

    public PieceType getType() {
        return mType;
    }

    public Position getPosition() {
        return mPosition;
    }

    public void setPosition(Position position) {
        this.mPosition = position;
    }

    public void setType(PieceType type) {
        this.mType = type;
    }

    public List<Move> getMoveList() {
        return moveList;
    }
}
