package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.presention.model.CellType;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class MakeMoveUseCase extends ParamsUseCase<CellType[][], MakeMoveUseCase.MakeMoveParams> {

    @Inject
    public MakeMoveUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor) {
        super(uiExecutor, threadExecutor);
    }

    @Override
    protected Observable<CellType[][]> buildObservable(MakeMoveParams makeMoveParams) {
        ObservableOnSubscribe<CellType[][]> observableOnSubscribe = new ObservableOnSubscribe<CellType[][]>() {
            @Override
            public void subscribe(ObservableEmitter<CellType[][]> emitter) throws Exception {

            }
        };
        return Observable.create(observableOnSubscribe);
    }

    public static class MakeMoveParams {

        private int row;
        private int column;

        private MakeMoveParams(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public static MakeMoveParams create(int row,int column){
            return new MakeMoveParams(row, column);
        }
    }
}
