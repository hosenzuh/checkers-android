package com.toolsforfools.checkers.domain.interactor.usecase.base;

import android.util.Log;

import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

public abstract class UseCase<T> extends BaseUseCase {
    private static final String TAG = "UseCase";

    public UseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor) {
        super(uiExecutor, threadExecutor);
    }

    protected abstract Observable<T> buildObservable();

    public void execute(DisposableObserver<T> disposableObserver) {
        Log.d(TAG, "execute: UseCase");
        addDisposable(
                buildObservable()
                        .doOnError(Throwable::printStackTrace)
                        .subscribeOn(threadExecutor.getSchedulers())
                        .observeOn(uiExecutor.getSchedulers())
                        .subscribeWith(disposableObserver)
        );
    }
}
