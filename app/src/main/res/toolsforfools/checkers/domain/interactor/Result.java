package com.toolsforfools.checkers.domain.interactor;


import androidx.annotation.Nullable;

/**
 * Data Wrapper ,Can Handle null data
 *
 * @param <T>
 */
public class Result<T> {

    @Nullable
    private T data;

    private Exception exception;

    private Status status;

    public Result(@Nullable T data, Exception exception, Status status) {
        this.data = data;
        this.exception = exception;
        this.status = status;
    }

    public static <T> Result<T> success(T data) {
        return new Result<>(data, null, Status.SUCCESS);
    }

    public static <T> Result<T> failure(T data, Exception exception) {
        return new Result<>(data, exception, Status.FAILURE);
    }

    @Nullable
    public T getData() {
        return data;
    }

    public Exception getException() {
        return exception;
    }

    public Status getStatus() {
        return status;
    }

    public void setData(@Nullable T data) {
        this.data = data;
    }

    public enum Status {
        SUCCESS, FAILURE
    }
}
