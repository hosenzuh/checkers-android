package com.toolsforfools.checkers.data.local.sharedpref;


public interface ISharedPref {

    String getToken();

    void setUserStatus(int userStatus);
}
