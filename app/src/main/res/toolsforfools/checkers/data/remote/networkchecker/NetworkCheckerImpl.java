package com.toolsforfools.checkers.data.remote.networkchecker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.inject.Inject;

public class NetworkCheckerImpl implements INetworkChecker {

    private Context context;
    private ConnectivityManager connectivityManager;

    @Inject
    public NetworkCheckerImpl(Context context) {
        this.context = context;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    @Override
    public boolean isConnected() {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    private boolean isInternetAvailable() {
        try {
            InetAddress inetAddress = InetAddress.getByName("www.google.com");
            return !inetAddress.equals("");
        } catch (UnknownHostException e) {
            return false;
        }
    }

}
