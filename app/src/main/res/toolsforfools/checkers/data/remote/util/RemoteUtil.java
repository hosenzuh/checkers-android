package com.toolsforfools.checkers.data.remote.util;

import com.toolsforfools.checkers.data.entity.mapper.JsonMapper;
import com.toolsforfools.checkers.data.remote.api.model.base.BaseResponse;

public class RemoteUtil {

    private static JsonMapper jsonMapper = new JsonMapper();

    public static String getErrorMessage(String jsonError) {
        return jsonMapper.unmap(jsonError, BaseResponse.class).getMetaResponse().getMessage();
    }
}
