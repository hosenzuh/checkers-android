package com.toolsforfools.checkers.data.remote.api;


import com.toolsforfools.checkers.data.remote.api.model.base.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import static com.toolsforfools.checkers.data.remote.api.ApiEndPoints.BASE_URL;

public interface RetrofitService {

    @POST(BASE_URL)
    Call<BaseResponse<String>> signUp(@Body String request);
}
