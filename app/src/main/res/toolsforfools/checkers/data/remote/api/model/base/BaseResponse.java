package com.toolsforfools.checkers.data.remote.api.model.base;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<T> {

    @SerializedName("meta")
    protected MetaResponse metaResponse;
    @SerializedName("data")
    protected T body;


    public MetaResponse getMetaResponse() {
        return metaResponse;
    }

    public void setMetaResponse(MetaResponse metaResponse) {
        this.metaResponse = metaResponse;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
