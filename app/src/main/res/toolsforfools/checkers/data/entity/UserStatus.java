package com.toolsforfools.checkers.data.entity;

public final class UserStatus {


    public static final int LOGGED_IN = 1;
    public static final int GUEST = 0;
    public static final int NONE = -1; // == Not logged-in and not guest

    private UserStatus() {
    }

    public static int getUserStatus(int value) {
        switch (value) {
            case 1:
                return LOGGED_IN;
            case 0:
                return GUEST;
            case -1:
            default:
                return NONE;
        }
    }

}
