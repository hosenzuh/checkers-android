package com.toolsforfools.checkers.data.entity.mapper;

import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class JsonMapper {

    private Gson gson = new Gson();

    @Inject
    public JsonMapper() {
    }

    synchronized public String map(Object model) {
        if (model == null) {
            return null;
        }
        return gson.toJson(model);
    }

    public <K> K unmap(String json, Class<K> clazz) {
        if (json == null || json.isEmpty())
            return null;
        return gson.fromJson(json, clazz);
    }
}
