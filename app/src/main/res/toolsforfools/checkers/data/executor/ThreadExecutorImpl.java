package com.toolsforfools.checkers.data.executor;


import com.toolsforfools.checkers.domain.executor.ThreadExecutor;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class ThreadExecutorImpl implements ThreadExecutor {

    @Override
    public Scheduler getSchedulers() {
        return Schedulers.io();
    }
}
