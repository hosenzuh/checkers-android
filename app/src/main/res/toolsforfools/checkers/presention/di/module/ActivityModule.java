package com.toolsforfools.checkers.presention.di.module;

import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardModule;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.multi.MultiPlayerSettingsActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.multi.MultiPlayerSettingsModule;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.single.SinglePlayerSettingsActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.single.SinglePlayerSettingsModule;
import com.toolsforfools.checkers.presention.ui.game.startgame.StartGameActivity;
import com.toolsforfools.checkers.presention.ui.game.startgame.StartGameModule;
import com.toolsforfools.checkers.presention.ui.main.MainActivity;
import com.toolsforfools.checkers.presention.ui.main.MainModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity injectMainActivity();

    @ContributesAndroidInjector(modules = StartGameModule.class)
    abstract StartGameActivity injectStartGameActivity();

    @ContributesAndroidInjector(modules = SinglePlayerSettingsModule.class)
    abstract SinglePlayerSettingsActivity injectSinglePlayerSettingsActivity();

    @ContributesAndroidInjector(modules = OfflineBoardModule.class)
    abstract OfflineBoardActivity injectBoardActivity();

    @ContributesAndroidInjector(modules = MultiPlayerSettingsModule.class)
    abstract MultiPlayerSettingsActivity injectMultiPlayerSettingsActivity();
}
