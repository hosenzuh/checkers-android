package com.toolsforfools.checkers.presention.callback;

public interface OnItemClickListener<T> {
    void onClick(T item, int position);
}
