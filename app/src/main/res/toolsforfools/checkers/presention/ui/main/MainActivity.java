package com.toolsforfools.checkers.presention.ui.main;

import android.os.Bundle;


import com.toolsforfools.checkers.BR;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityMainBinding;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.startgame.StartGameActivity;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends MVVMActivity<MainViewModel, ActivityMainBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected MainViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(MainViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding.startGameButton.setOnClickListener(
                view -> NavigationUtils.goToActivity(this, StartGameActivity.class)
        );
    }
}
