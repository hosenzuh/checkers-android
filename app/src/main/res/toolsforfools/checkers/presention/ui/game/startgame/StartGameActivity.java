package com.toolsforfools.checkers.presention.ui.game.startgame;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityStartGameBinding;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.multi.MultiPlayerSettingsActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.single.SinglePlayerSettingsActivity;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

public class StartGameActivity extends MVVMActivity<StartGameViewModel, ActivityStartGameBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected StartGameViewModel provideViewModel() {
        return new ViewModelProvider(this,factory).get(StartGameViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_start_game;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding.singlePlayerButton.setOnClickListener(
                view -> NavigationUtils.goToActivity(this, SinglePlayerSettingsActivity.class)
                );
        mActivityBinding.multiPlayerButton.setOnClickListener(
                view -> NavigationUtils.goToActivity(this, MultiPlayerSettingsActivity.class)
        );
    }
}
