package com.toolsforfools.checkers.presention.ui.base.view.activity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.toolsforfools.checkers.presention.ui.base.IBaseView;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public abstract class BaseActivity extends AppCompatActivity implements IBaseView, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> androidInjector;

    private boolean isRunning;

    protected abstract int getLayoutId();

    protected void onFetchParams() {
    }

    ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        onFetchParams();
        onInject();
        super.onCreate(savedInstanceState);
        setView();
    }

    protected void setView() {
        setContentView(getLayoutId());
    }

    protected void onInject() {
        AndroidInjection.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected boolean isActivityRunning() {
        return isRunning;
    }

    // base methods
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int stringId) {
        showMessage(getString(stringId));
    }

    @Override
    public void showDialogMessage(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> dialog.dismiss())
                .setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.dismiss())
                .create().show();
    }

    @Override
    public void showDialogMessage(int title, int message) {
        showDialogMessage(getString(title), getString(message));
    }

    @Override
    public void showDialogMessage(String message) {
        showDialogMessage("", message);
    }

    @Override
    public void showDialogMessage(int message) {
        showDialogMessage(getString(message));
    }

    @Override
    public void hideKeyboard() {
        View currentFocus = getCurrentFocus();
        if (currentFocus == null)
            currentFocus = new View(this);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}
