package com.toolsforfools.checkers.presention.ui.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.presention.callback.OnCellClickListener;
import com.toolsforfools.checkers.presention.model.CellType;

import androidx.appcompat.widget.AppCompatButton;

public class Cell extends AppCompatButton implements View.OnClickListener {

    private boolean isBeige;
    private PieceType pieceType;
    private CellType cellType;
    private int row;
    private int column;
    private OnCellClickListener onCellClickListener;

    public Cell(Context context, int row, int column, boolean isBeige, OnCellClickListener onCellClickListener) {
        super(context);
        this.row = row;
        this.column = column;
        this.onCellClickListener = onCellClickListener;
        this.isBeige = isBeige;
        setParams();
        setOnClickListener(this::onClick);
    }

    public void setView(PieceType pieceType, CellType cellType) {
        this.pieceType = pieceType;
        this.cellType = cellType;
        setView();
    }


    public void setView() {

        if (pieceType == null) {
            setEmpty();
            return;
        }
        if (isBeige) {
            switch (pieceType) {
                case whitePiece:
                    setWhite();
                    break;
                case blackPiece:
                    setBlack();
                    break;
                case blackKing:
                    setBlackKing();
                    break;
                case whiteKing:
                    setWhiteKing();
                    break;
            }
        } else {
            setBrownBackground();
        }
    }

    @SuppressLint("ResourceAsColor")
    private void setBeigeBackground() {
        setBackground(getResources().getDrawable(R.drawable.ic_beige_cell));
    }

    @SuppressLint("ResourceAsColor")
    private void setBrownBackground() {
        setBackground(getResources().getDrawable(R.drawable.ic_brown_piece));
    }

    private void setBlack() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_piece));
    }

    private void setWhite() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_piece));
    }

    private void setBlackKing() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_king_piece));
    }

    private void setWhiteKing() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_king_piece));
    }

    private void setEmpty() {
        if (isBeige)
            setBrownBackground();
        else
            setBeigeBackground();
    }

    public void setAvailableMove() {
        cellType = CellType.AVAILABLE_MOVE;
        setBackground(getResources().getDrawable(R.drawable.ic_avilable_move));
    }

    public void removeAvailableMove() {
        cellType = CellType.NONE;
        setEmpty();
    }

    //view methods

    void setParams() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        setLayoutParams(layoutParams);
    }


    @Override
    public void onClick(View view) {
        onCellClickListener.onClick(row, column, pieceType, cellType);
    }
}
