package com.toolsforfools.checkers.presention.ui.game.gamesettings.multi;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityMultiPlayerSettingsBinding;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

public class MultiPlayerSettingsActivity extends MVVMActivity<MultiPlayerSettingsViewModel, ActivityMultiPlayerSettingsBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected MultiPlayerSettingsViewModel provideViewModel() {
        return new ViewModelProvider(this,factory).get(MultiPlayerSettingsViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_multi_player_settings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding.startGameButton.setOnClickListener(
                view -> NavigationUtils.goToActivity(this, OfflineBoardActivity.class)
        );
    }
}
