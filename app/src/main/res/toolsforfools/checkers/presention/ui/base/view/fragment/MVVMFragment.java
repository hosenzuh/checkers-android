package com.toolsforfools.checkers.presention.ui.base.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;
import com.toolsforfools.checkers.util.lifecycle.EventObserver;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import dagger.android.support.AndroidSupportInjection;

abstract public class MVVMFragment<VM extends BaseViewModel, DB extends ViewDataBinding> extends BaseFragment {

    protected VM viewModel;

    protected DB mFragmentBinding;

    protected abstract VM provideViewModel();

    protected abstract int getViewModelId();

    protected void onFetchParams() {

    }

    protected void onInject() {
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        onFetchParams();
        onInject();
        super.onCreate(savedInstanceState);
        viewModel = provideViewModel();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragmentBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        rootView = mFragmentBinding.getRoot();
        setupDataBinding();
        setupBaseObserver();
        return rootView;
    }

    private void setupDataBinding() {
        mFragmentBinding.setVariable(getViewModelId(), viewModel);
        mFragmentBinding.setLifecycleOwner(this);
        mFragmentBinding.executePendingBindings();
    }

    private void setupBaseObserver() {
        LifecycleOwner owner = getViewLifecycleOwner();
        viewModel.toastMessage.observe(owner, new EventObserver<String>(this::showMessage));
        viewModel.toastMessageResource.observe(owner, new EventObserver<>(this::showMessage));
        viewModel.hideKeyboard.observe(owner, new EventObserver<>(ignored -> hideKeyboard()));
    }
}
