package com.toolsforfools.checkers.presention.ui.game.board;

import com.toolsforfools.checkers.presention.di.ViewModelKey;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardViewModel;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class BoardModule {

    @ViewModelKey(OfflineBoardViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindBoardViewModel(OfflineBoardViewModel viewModel);
}
