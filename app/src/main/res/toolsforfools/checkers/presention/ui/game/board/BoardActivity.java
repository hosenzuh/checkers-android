package com.toolsforfools.checkers.presention.ui.game.board;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityBoardBinding;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.presention.callback.OnCellClickListener;
import com.toolsforfools.checkers.presention.model.CellType;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.customview.Cell;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardViewModel;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class BoardActivity extends MVVMActivity<OfflineBoardViewModel, ActivityBoardBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    public static final int CELLS_NUMBER = 9;
    private Cell[][] cells = new Cell[9][9];

    @Override
    protected OfflineBoardViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(OfflineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_offline_board;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        flipBoard();
        createBoard();
        setupObservers();
    }

    private void setupObservers() {

        for (int i = 1; i < CELLS_NUMBER; i++) {
            for (int j = 1; j < CELLS_NUMBER; j++) {
                int finalJ = j;
                int finalI = i;
                viewModel.cellsState[i][j].observe(this, cellType -> {
                    if (cellType.equals(CellType.AVAILABLE_MOVE))
                        cells[finalI][finalJ].setAvailableMove();
                    else if (cellType.equals(CellType.NONE))
                        cells[finalI][finalJ].removeAvailableMove();
                    else { //there is piece
                        PieceType pieceType = viewModel.piecesType[finalI][finalJ].getValue();
                            cells[finalI][finalJ].setView(pieceType,CellType.PIECE);
                    }
                });

                viewModel.piecesType[i][j].observe(this, pieceType -> {
                    cells[finalI][finalJ].setView(pieceType,viewModel.cellsState[finalI][finalJ].getValue());
                });

                // Winning observing
                viewModel.isWinner.observe(this,playerType -> {
                    Toast.makeText(this, playerType +" WON!!", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                });

                // change turn observing
                viewModel.playerTurn.observe(this,playerType -> {
                    if(playerType.equals(Player.PlayerType.Black)) {
                        mActivityBinding.blackPlayerBoard.setText("Your turn");
                        mActivityBinding.whitePlayerBoard.setText("Waiting black player");
                    }else{
                        mActivityBinding.whitePlayerBoard.setText("Your turn");
                        mActivityBinding.blackPlayerBoard.setText("Waiting white player");
                    }
                });

            }
        }
    }

    private void createBoard() {
        LinearLayout row;

        for (int i = 1; i < CELLS_NUMBER; i++) {

            row = new LinearLayout(this);

            for (int j = 1; j < CELLS_NUMBER; j++) {
                row.addView(createCell(i, j));
            }
            mActivityBinding.boardLayout.addView(row);
        }

    }

    boolean flipped = true;

    private Cell createCell(int row, int column) {
        OnCellClickListener onCellClickListener = (pieceRow, pieceColumn, pieceType, cellType) -> {
            if (cellType.equals(CellType.AVAILABLE_MOVE)) {
                viewModel.makeMove(pieceRow, pieceColumn);
//                flipBoard();
            }
            else if (cellType.equals(CellType.NONE)) {
                viewModel.clearAvailableMoves();
            } else {
                viewModel.checkMoves(pieceRow, pieceColumn);
            }
        };
        Cell cell = new Cell(this, row, column,isBeige(row,column) , onCellClickListener);
        cells[row][column] = cell;
        return cell;
    }

    private void flipBoard() {
        flipped = !flipped;
        if(!flipped)
            mActivityBinding.boardLayout.setScaleY(-1f);
        else
            mActivityBinding.boardLayout.setScaleY(1f);
    }

    private boolean isBeige(int row,int column) {
        return (row%2!=0&&column%2!=0)||(row%2==0&&column%2==0);
    }

}
