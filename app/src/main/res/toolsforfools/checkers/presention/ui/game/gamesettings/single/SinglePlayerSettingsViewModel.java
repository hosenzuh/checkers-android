package com.toolsforfools.checkers.presention.ui.game.gamesettings.single;

import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

public class SinglePlayerSettingsViewModel extends BaseViewModel {


    public MutableLiveData<Integer> difficultyLevel = new MutableLiveData<>(5);
    public MutableLiveData<Boolean> isBlack = new MutableLiveData<>(false);


    @Inject
    public SinglePlayerSettingsViewModel() {
    }

    public void setIsBlack(boolean isBlack){
        this.isBlack.postValue(isBlack);
    }
}
