package com.toolsforfools.checkers.presention.ui.game.board;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.Game;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.presention.model.CellType;
import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

import static com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity.CELLS_NUMBER;

public class BoardViewModel extends BaseViewModel implements IGameView {

    MutableLiveData<CellType>[][] cellsState = new MutableLiveData[20][20];
    MutableLiveData<PieceType>[][] piecesType = new MutableLiveData[20][20];
    MutableLiveData<Player.PlayerType> playerTurn = new MutableLiveData<>(Player.PlayerType.White);
    MutableLiveData<Player.PlayerType> isWinner = new MutableLiveData<>();
    private Game game;
    private Piece lastClickedPiece;
    String TAG = "VIEWMODEL";
    private List<Move> availableMoves;


    @Inject
    public BoardViewModel() {
        initBoardState();
        game = new Game(this);
        game.startGame();
    }

    private void initBoardState() {
        for (int row = 0; row < CELLS_NUMBER; row++) {
            for (int column = 0; column < CELLS_NUMBER; column++) {
                cellsState[row][column] = new MutableLiveData<>(CellType.NONE);
                piecesType[row][column] = new MutableLiveData<>(null);
            }
        }
    }

    void checkMoves(int row, int column) {
        Log.e(TAG, "checkMoveAvailable: ");
        clearAvailableMoves();
        if (!checkMoveAvailable(row, column))
            return;

        lastClickedPiece = new Piece(new Position(row, column), piecesType[row][column].getValue());
        availableMoves = game.getPieceMoves(new Position(row, column));
        Position position;
        for (int i = 0; i < availableMoves.size(); i++) {
            position = availableMoves.get(i).getDestination();
            cellsState[position.getX()][position.getY()].postValue(CellType.AVAILABLE_MOVE);
        }
    }

    private boolean checkMoveAvailable(int row, int column) {
        Player.PlayerType playerType = playerTurn.getValue();
        PieceType pieceType = piecesType[row][column].getValue();
        return playerType.equals(Player.PlayerType.Black) ? pieceType.equals(PieceType.blackKing) || pieceType.equals(PieceType.blackPiece)
                : pieceType.equals(PieceType.whiteKing) || pieceType.equals(PieceType.whitePiece);
    }

    void makeMove(int row, int column) {
        Log.e(TAG, "makeMove: ");

        for (int i = 0; i < availableMoves.size(); i++) {
            if (availableMoves.get(i).getDestination().getX() == row && availableMoves.get(i).getDestination().getY() == column) {
                game.selectMove(availableMoves.get(i));
                clearAvailableMoves();
                break;
            }
        }
    }

    void clearAvailableMoves() {
        Log.e(TAG, "clearAvailableMoves: ");
        lastClickedPiece = null;
        for (int i = 1; i < CELLS_NUMBER; i++) {
            for (int j = 1; j < CELLS_NUMBER; j++) {
                if (cellsState[i][j].getValue().equals(CellType.AVAILABLE_MOVE)) {
                    cellsState[i][j].postValue(CellType.NONE);
                }
            }
        }
    }

    @Override
    public void displayMove(Position oldPosition, Position newPosition) {
        Log.e(TAG, "displayMove: ");
        PieceType pieceType = piecesType[oldPosition.getX()][oldPosition.getY()].getValue();
        cellsState[oldPosition.getX()][oldPosition.getY()].postValue(CellType.NONE);
        piecesType[oldPosition.getX()][oldPosition.getY()].postValue(null);
        cellsState[newPosition.getX()][newPosition.getY()].postValue(CellType.PIECE);
        piecesType[newPosition.getX()][newPosition.getY()].postValue(pieceType);
    }

    @Override
    public void removePiece(Position position) {
        Log.e(TAG, "removePiece: ");
        cellsState[position.getX()][position.getY()].postValue(CellType.NONE);
        piecesType[position.getX()][position.getY()].postValue(null);
    }

    @Override
    public void upgradePiece(Position position) {
        Log.e(TAG, "upgradePiece: " + position.getX()+" "+position.getY()+ "  " +piecesType[position.getX()][position.getY()].getValue());
        PieceType pieceType = piecesType[position.getX()][position.getY()].getValue();
        if (pieceType.equals(PieceType.whitePiece))
            piecesType[position.getX()][position.getY()].postValue(PieceType.whiteKing);
        else
            piecesType[position.getX()][position.getY()].postValue(PieceType.blackKing);

    }

    @Override
    public void putPieceInPosition(PieceType pieceType, Position position) {
        Log.e(TAG, "putPieceInPosition: ");
        cellsState[position.getX()][position.getY()] = new MutableLiveData<>(CellType.PIECE);
        piecesType[position.getX()][position.getY()] = new MutableLiveData<>(pieceType);
    }

    @Override
    public void startPlayerTurn(Player.PlayerType playerType) {
        Log.e(TAG, "startPlayerTurn: ");
        playerTurn.postValue(playerType);
    }

    @Override
    public void displayPlayerWin() {
        Log.e(TAG, "displayPlayerWin: ");
        isWinner.postValue(playerTurn.getValue());
    }
}
