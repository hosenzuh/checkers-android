package com.toolsforfools.checkers.presention.ui.base.view.activity;

import android.os.Bundle;


import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;
import com.toolsforfools.checkers.util.lifecycle.EventObserver;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class MVVMActivity<VM extends BaseViewModel, DB extends ViewDataBinding> extends BaseActivity {

    protected VM viewModel;
    protected DB mActivityBinding;

    protected abstract VM provideViewModel();

    protected abstract int getViewModelId();

    @Override
    protected void setView() {
        mActivityBinding = DataBindingUtil.setContentView(this, getLayoutId());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupDataBinding();
        setupBaseObserver();
    }

    protected void setupBaseObserver() {
        viewModel.toastMessage.observe(this, new EventObserver<>(this::showMessage));
        viewModel.toastMessageResource.observe(this, new EventObserver<>(this::showMessage));
        viewModel.hideKeyboard.observe(this, new EventObserver<>(ignored -> hideKeyboard()));
    }

    private void setupDataBinding() {
        viewModel = provideViewModel();
        mActivityBinding.setVariable(getViewModelId(), viewModel);
        mActivityBinding.setLifecycleOwner(this);
        mActivityBinding.executePendingBindings();
    }


}
