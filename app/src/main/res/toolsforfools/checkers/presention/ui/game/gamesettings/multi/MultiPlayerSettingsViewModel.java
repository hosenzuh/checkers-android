package com.toolsforfools.checkers.presention.ui.game.gamesettings.multi;

import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

public class MultiPlayerSettingsViewModel extends BaseViewModel {

    public MutableLiveData<Boolean> isBlack = new MutableLiveData<>(false);

    @Inject
    public MultiPlayerSettingsViewModel() {
    }

    public void setIsBlack(boolean isBlack) {
        this.isBlack.postValue(isBlack);
    }
}
