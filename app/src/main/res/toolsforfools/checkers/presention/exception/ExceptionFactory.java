package com.toolsforfools.checkers.presention.exception;


import com.toolsforfools.checkers.R;

import androidx.annotation.StringRes;

public class ExceptionFactory {

    @StringRes
    public static int getString(Exception e) {
        return e instanceof Exception ? R.string.bottom_sheet_behavior
                : e instanceof Exception ? R.string.app_name :
                R.string.app_name;// This should be strings in resources
    }

    @StringRes
    public static int getString(Throwable throwable) {
        return getString((Exception) throwable);
    }
}
