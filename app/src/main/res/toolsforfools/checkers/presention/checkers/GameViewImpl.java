package com.toolsforfools.checkers.presention.checkers;

import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import io.reactivex.Observable;

public class GameViewImpl implements IGameView {


    @Override
    public void displayMove(Position oldPosition, Position newPosition) {

    }

    @Override
    public void removePiece(Position position) {

    }

    @Override
    public void upgradePiece(Position position) {

    }

    @Override
    public void putPieceInPosition(PieceType pieceType, Position position) {

    }

    @Override
    public void startPlayerTurn(Player.PlayerType playerType) {

    }

    @Override
    public void displayPlayerWin() {

    }
}
