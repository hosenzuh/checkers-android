package com.toolsforfools.checkers.presention.executor;

import com.toolsforfools.checkers.domain.executor.UIExecutor;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class UIExecutorImpl implements UIExecutor {

    @Inject
    public UIExecutorImpl() {
    }

    @Override
    public Scheduler getSchedulers() {
        return AndroidSchedulers.mainThread();
    }
}
