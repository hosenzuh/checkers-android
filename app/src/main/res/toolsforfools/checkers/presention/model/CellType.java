package com.toolsforfools.checkers.presention.model;

public enum CellType {
    PIECE, AVAILABLE_MOVE, NONE;


    public static CellType getTypeByTitle(String value) {
        switch (value.toLowerCase()) {
            case "piece":
                return PIECE;
            case "available_move":
                return AVAILABLE_MOVE;
            case "none":
            default:
                return NONE;
        }
    }
}
