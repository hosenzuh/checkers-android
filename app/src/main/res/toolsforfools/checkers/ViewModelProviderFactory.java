package com.toolsforfools.checkers;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ViewModelProviderFactory implements ViewModelProvider.Factory {

    private Map<Class<? extends ViewModel>, Provider<ViewModel>> viewModels;

    @Inject
    public ViewModelProviderFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> viewModels) {
        this.viewModels = viewModels;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Provider<ViewModel> viewModel = viewModels.get(modelClass);
        if (viewModel == null) {
            for (Map.Entry<Class<? extends ViewModel>, Provider<ViewModel>> entry :
                    viewModels.entrySet()) {
                if (entry.getKey().isAssignableFrom(modelClass)) {
                    viewModel = entry.getValue();
                    break;
                }
            }
        }

        if (viewModel == null)
            throw new RuntimeException(modelClass.getName() + " not found");

        try {
            return (T) viewModel.get();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
}
