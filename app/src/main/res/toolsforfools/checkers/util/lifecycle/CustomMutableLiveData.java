package com.toolsforfools.checkers.util.lifecycle;

import androidx.lifecycle.MutableLiveData;

public class CustomMutableLiveData<T> extends MutableLiveData<T> {

    public CustomMutableLiveData(T value) {
        super(value);
    }

    public CustomMutableLiveData() {
    }

    public T get() {
        return getValue();
    }
}
