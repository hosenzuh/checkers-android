package com.toolsforfools.checkers.util.lifecycle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Event<T> {

    @NonNull
    private T data;

    private boolean isHandled = false;

    public Event(@NonNull T data) {
        this.data = data;
    }

    @Nullable
    T getDataOrNull() {
        if (isHandled)
            return null;
        isHandled = true;
        return data;
    }
}
