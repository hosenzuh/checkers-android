package com.toolsforfools.checkers.util;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.presention.ui.base.adapter.BaseListAdapter;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class ViewsAdapter {

    @BindingAdapter(value = {"loadImage", "placeHolder"}, requireAll = false)
    public static void loadImage(ImageView imageView, String url, Drawable placeHolder) {
        ImageUtils.loadPictureAndCache(
                imageView
                , url
                , placeHolder != null ? placeHolder : imageView.getContext().getDrawable(R.drawable.ic_launcher_background));
    }

    @BindingAdapter("loadData")
    public static void loadData(RecyclerView recyclerView, List data) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter instanceof BaseListAdapter)
            ((BaseListAdapter) adapter).submitData(data);
    }
}
