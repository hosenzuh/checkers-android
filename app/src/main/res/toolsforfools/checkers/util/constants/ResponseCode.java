package com.toolsforfools.checkers.util.constants;

final public class ResponseCode {

    public static final int UNAUTHORISED_USER = 401;
    public static final int UNKNOWN_ERROR = 402;
    public static final int UNVERIFIED_ACCOUNT = 406;
}
