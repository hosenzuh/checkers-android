package com.toolsforfools.checkers.util.constants;

final public class SharedPrefKey {

    public static final String TOKEN = "token";

    private SharedPrefKey() {
    }
}
