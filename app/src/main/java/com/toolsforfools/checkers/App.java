package com.toolsforfools.checkers;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.util.Log;


import javax.inject.Inject;

import androidx.annotation.NonNull;

import com.toolsforfools.checkers.presention.di.component.DaggerAppComponent;
import com.toolsforfools.checkers.util.sounds.BackgroundMusicService;
import com.toolsforfools.checkers.util.sounds.SoundsManager;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import dagger.android.support.DaggerApplication;
import io.reactivex.plugins.RxJavaPlugins;


public class App extends DaggerApplication implements HasAndroidInjector, LifecycleEventObserver {

    @Inject
    DispatchingAndroidInjector<Object> androidInjector;

    private static final String TAG = "APP";

    private static App instance;
    private Intent svc;


    public static App getInstance() {
        return instance;
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent
                .builder()
                .app(this)
                .build();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        RxJavaPlugins.setErrorHandler(error -> {
        });
    }

    public void resumeMusic(){
        SoundsManager.playBackgroundMusic(this);
    }
    public void pauseMusic(){
        SoundsManager.pauseBackgroundMusic(this);
    }

    @Override
    public void onStateChanged(@NonNull LifecycleOwner source, @NonNull Lifecycle.Event event) {
        Log.e(TAG, "onStateChanged: "+event );
        if (event.equals(Lifecycle.Event.ON_PAUSE)) {
            pauseMusic();
        }else if (event.equals(Lifecycle.Event.ON_RESUME)){
            resumeMusic();
        }
    }
}
