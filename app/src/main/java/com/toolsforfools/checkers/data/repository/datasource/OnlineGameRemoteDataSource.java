package com.toolsforfools.checkers.data.repository.datasource;

import com.toolsforfools.checkers.data.remote.api.service.game.OnlineGameApiService;

import javax.inject.Inject;

import io.reactivex.Single;

public class OnlineGameRemoteDataSource implements IOnlineGameRemoteDataSource {

    OnlineGameApiService onlineGameApiService;
    @Inject
    public OnlineGameRemoteDataSource(OnlineGameApiService onlineGameApiService) {
        this.onlineGameApiService = onlineGameApiService;
    }

    @Override
    public Single<String> createRoom(String playerName) {
        return onlineGameApiService.createRoom(playerName);
    }
}
