package com.toolsforfools.checkers.data.remote.api;

final public class ApiEndPoints {

    public static final String BASE_URL = "https://hmedo-cheakers-server.herokuapp.com/api/";
    public static final String TOKEN_HEADER = "Authorization";
    public static final String LOGIN = "login";
    public static final String SIGN_UP = "users";
    // some End points
    public static final String END_POINT = BASE_URL + "SADsa";
}
