package com.toolsforfools.checkers.data.remote.api.model.base;

import com.google.gson.annotations.SerializedName;

public class MetaResponse {

    @SerializedName("status")
    private boolean success;
    @SerializedName("status_number")
    private int statusNumber;
    @SerializedName("message")
    private String message;

    public MetaResponse(boolean success, int statusNumber, String message) {
        this.success = success;
        this.statusNumber = statusNumber;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public int getStatusNumber() {
        return statusNumber;
    }

    public String getMessage() {
        return message;
    }
}
