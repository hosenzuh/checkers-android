package com.toolsforfools.checkers.data.remote.socket;

public final class SocketConstants {
    public static final String BASE_URL = "https://hmedo-cheakers-server.herokuapp.com";

    public static final String CREATE_ROOM = "create room";

    public static final String JOIN_ROOM = "join room";

    public static final String SEND_MOVE = "sendmove";
    public static final String RECEIVE_MOVE = "move";

    public static final String GAME_STARTED = "game started";
}
