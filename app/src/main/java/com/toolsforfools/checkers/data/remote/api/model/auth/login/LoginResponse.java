package com.toolsforfools.checkers.data.remote.api.model.auth.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;
import com.toolsforfools.checkers.data.remote.api.model.base.BaseResponse;

public class LoginResponse extends UserInfoResponse {

    public LoginResponse(String userName, int score, String token) {
        super(userName, score, token);
    }
}
