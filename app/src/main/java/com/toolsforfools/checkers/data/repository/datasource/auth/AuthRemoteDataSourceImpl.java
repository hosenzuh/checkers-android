package com.toolsforfools.checkers.data.repository.datasource.auth;

import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginResponse;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpResponse;
import com.toolsforfools.checkers.data.remote.api.service.auth.AuthApiService;
import com.toolsforfools.checkers.domain.interactor.Result;

import javax.inject.Inject;

import io.reactivex.Single;

public class AuthRemoteDataSourceImpl implements IAuthRemoteDataSource {
    private AuthApiService authApiService;

    @Inject
    public AuthRemoteDataSourceImpl(AuthApiService authApiService) {
        this.authApiService = authApiService;
    }

    @Override
    public Single<Result<LoginResponse>> login(LoginRequest request) {
        return authApiService.login(request);
    }

    @Override
    public Single<Result<SignUpResponse>> signUp(SignUpRequest request) {
        return authApiService.signUp(request);
    }
}
