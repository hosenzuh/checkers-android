package com.toolsforfools.checkers.data.repository;

import com.toolsforfools.checkers.data.entity.UserStatus;
import com.toolsforfools.checkers.data.entity.mapper.UserEntityMapper;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginResponse;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpRequest;
import com.toolsforfools.checkers.data.repository.datasource.auth.IAuthRemoteDataSource;
import com.toolsforfools.checkers.domain.entity.UserEntity;
import com.toolsforfools.checkers.domain.interactor.Result;
import com.toolsforfools.checkers.domain.repository.AuthRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class AuthRepositoryImpl implements AuthRepository {

    private ISharedPref iSharedPref;
    private IAuthRemoteDataSource remoteDataSource;
    private UserEntityMapper userEntityMapper;


    @Inject
    public AuthRepositoryImpl(ISharedPref iSharedPref, IAuthRemoteDataSource remoteDataSource, UserEntityMapper userEntityMapper) {
        this.iSharedPref = iSharedPref;
        this.remoteDataSource = remoteDataSource;
        this.userEntityMapper = userEntityMapper;
    }
    @Override
    public Single<Result<UserEntity>> signUp(String userName, String password, String confirmPassword, String email) {
        SignUpRequest request = new SignUpRequest(userName, password, confirmPassword, email);
        return remoteDataSource.signUp(request)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(result -> {
                    if (result.getData() != null)
                        iSharedPref.saveToke(result.getData().getToken());
                })
                .map(result -> new Result<>(
                        result.getData() == null ? null : userEntityMapper.map(result.getData()),
                        result.getException(),
                        result.getStatus()
                ));
    }


    @Override
    public Single<Result<UserEntity>> login(String userName, String password) {
        LoginRequest request = new LoginRequest(userName, password);
        return remoteDataSource.login(request)
                .subscribeOn(Schedulers.io()) // to make the second request run on another thread
                .doOnSuccess(result -> {
                    if (result.getStatus() == Result.Status.SUCCESS) { // logged in successfully
                        iSharedPref.logOut(); // clear old data
                        if (result.getData() != null)
                        saveUserLocally(result.getData(), result.getData().getToken());
                    }
                })
                .map(result -> new Result<>(
                        result.getData() == null ? null : userEntityMapper.map(result.getData()),
                        result.getException(),
                        result.getStatus()
                ));
    }


    private void saveUserLocally(UserInfoResponse user, String token) {
        iSharedPref.saveToke(token);
        iSharedPref.saveUser(user);
        iSharedPref.setUserStatus(UserStatus.LOGGED_IN);
    }
}
