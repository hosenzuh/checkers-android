package com.toolsforfools.checkers.data.remote.api.service.game;

import android.util.Log;

import com.google.gson.Gson;
import com.toolsforfools.checkers.data.remote.networkchecker.INetworkChecker;
import com.toolsforfools.checkers.data.remote.socket.SocketConstants;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.PublishSubject;
import io.socket.client.Ack;
import io.socket.client.Socket;

public class GameApiServiceImpl implements GameApiService {

    Socket socket;
    INetworkChecker networkChecker;
    private Gson gson;

    private static final String TAG = "GameApiServiceImpl";

    @Inject
    public GameApiServiceImpl(Socket socket, INetworkChecker networkChecker) {
        this.socket = socket;
        this.networkChecker = networkChecker;
        gson = new Gson();
    }

    @Override
    public DisposableObserver<Move> getSendMoveObserver() {
        return new DisposableObserver<Move>() {
            @Override
            public void onNext(Move move) {
                socket.emit(SocketConstants.SEND_MOVE, gson.toJson(move), socket.id());
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public Observable<Move> onMoveSelected() {
        return PublishSubject.create(
                emitter -> {
                    socket.on(SocketConstants.RECEIVE_MOVE, args -> {
                        EatMove eatMove = gson.fromJson((String) args[0], EatMove.class);
                        if (eatMove.getVictemPiece() == null) {
                            Move move = gson.fromJson((String) args[0], Move.class);
                            emitter.onNext(move);
                        }else {
                            emitter.onNext(eatMove);
                        }
                    });
                });
    }

    @Override
    public Observable<Boolean> onGameJoined() {
        return PublishSubject.create(
                emitter ->
                        socket.on(SocketConstants.GAME_STARTED,
                                args -> emitter.onNext(true)));

    }

    @Override
    public Observable<Boolean> onGameCreated() {
        Log.e(TAG, "onGameCreated: startObserving");
        return PublishSubject.create(emitter -> socket.on(SocketConstants.GAME_STARTED,
                args -> {
                    Log.e(TAG, "onGameCreated: ");
                    emitter.onNext(true);
                }));
    }

    @Override
    public Observable<String> createRoom() {
        return Observable.create(
                emitter -> {
                    Log.e(TAG, "createRoom: ");
                    socket.connect();
                    socket.emit(SocketConstants.CREATE_ROOM, (Ack) args -> {
                        emitter.onNext((String) args[0]);
                    });
                }
        );
    }

    @Override
    public void joinGame(String roomId) {
        socket.connect();
        socket.emit(SocketConstants.JOIN_ROOM, roomId);
    }


}
