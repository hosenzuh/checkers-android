package com.toolsforfools.checkers.data.repository.datasource;

import com.toolsforfools.checkers.data.remote.api.service.game.GameApiService;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

public class GameRemoteDataSource implements IGameRemoteDataSource {

    GameApiService gameApiService;
    @Inject
    public GameRemoteDataSource(GameApiService gameApiService) {
        this.gameApiService = gameApiService;
    }

    @Override
    public DisposableObserver<Move> getSendMoveObserver() {
        return gameApiService.getSendMoveObserver();
    }

    @Override
    public Observable<Move> onMoveSelected() {
        return gameApiService.onMoveSelected();
    }

    @Override
    public Observable<Boolean> onGameJoined() {
        return gameApiService.onGameJoined();
    }

    @Override
    public Observable<Boolean> onGameCreated() {
        return gameApiService.onGameCreated();
    }

    @Override
    public Observable<String> createRoom() {
        return gameApiService.createRoom();
    }

    @Override
    public void joinGame(String roomId) {
        gameApiService.joinGame(roomId);
    }
}
