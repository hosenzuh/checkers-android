package com.toolsforfools.checkers.data.local.sharedpref;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.reflect.TypeToken;
import com.toolsforfools.checkers.data.entity.mapper.JsonMapper;
import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.util.constants.SharedPrefKey;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import static com.toolsforfools.checkers.util.constants.SharedPrefKey.USER;
import static com.toolsforfools.checkers.util.constants.SharedPrefKey.USER_STATUS;

public class SharedPrefImpl implements ISharedPref {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private JsonMapper jsonMapper;

    @Inject
    public SharedPrefImpl(Context context, JsonMapper jsonMapper) {
        this.sharedPreferences = context.getSharedPreferences(SharedPrefKey.APP_SHARED_KEY, context.MODE_PRIVATE);
        this.jsonMapper = jsonMapper;
        editor = sharedPreferences.edit();
    }

    @Override
    public void saveToke(String token) {
        editor.putString(SharedPrefKey.TOKEN, token).apply();
    }

    @Override
    public String getToken() {
        return sharedPreferences.getString(SharedPrefKey.TOKEN,"");
    }

    @Override
    public void setUserStatus(int userStatus) {
        editor.putInt(USER_STATUS, userStatus).apply();
    }

    @Override
    public int getUserStatus() {
        return sharedPreferences.getInt(USER_STATUS, -1);
    }


    @Override
    public void saveGame(SaveModel saveModel) {
        List<SaveModel> saveModels = loadGames();
        saveModels.add(saveModel);
        editor.putString(SharedPrefKey.SAVE_GAME_KEY, jsonMapper.map(saveModels)).apply();
    }

    @Override
    public List<SaveModel> loadGames() {

        Type type = new TypeToken<List<SaveModel>>(){}.getType();

        List<SaveModel> saveModels =
                jsonMapper.unmap(sharedPreferences.getString(SharedPrefKey.SAVE_GAME_KEY,"[]"),type);
        return saveModels;
    }

    @Override
    public void logOut() {
        editor.clear().apply();
    }

    @Override
    public void saveUser(UserInfoResponse userInfoResponse) {
        editor.putString(USER, jsonMapper.map(userInfoResponse)).apply();
    }

    @Override
    public UserInfoResponse getSavedUser() {
        return jsonMapper.unmap(sharedPreferences.getString(USER, ""), UserInfoResponse.class);
    }
}
