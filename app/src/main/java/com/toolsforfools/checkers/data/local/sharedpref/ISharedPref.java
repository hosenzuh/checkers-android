package com.toolsforfools.checkers.data.local.sharedpref;


import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;

import java.util.List;

public interface ISharedPref {

    void saveToke(String token);
    String getToken();

    void setUserStatus(int userStatus);

    void saveGame(SaveModel saveModel);

    List<SaveModel> loadGames();
    void logOut();
    void saveUser(UserInfoResponse userInfoResponse);
    UserInfoResponse getSavedUser();
    int getUserStatus();
}
