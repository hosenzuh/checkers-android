package com.toolsforfools.checkers.data.remote.api.service.game;

import android.util.Log;

import com.toolsforfools.checkers.data.exception.CreateRoomException;
import com.toolsforfools.checkers.data.remote.RemoteSingleOnSubscribe;
import com.toolsforfools.checkers.data.remote.networkchecker.INetworkChecker;
import com.toolsforfools.checkers.data.remote.socket.SocketConstants;
import com.toolsforfools.checkers.util.constants.ResponseCode;

import javax.inject.Inject;

import io.reactivex.Single;
import io.socket.client.Ack;
import io.socket.client.Socket;

public class OnlineGameApiServiceImpl implements OnlineGameApiService {

    Socket socket;
    INetworkChecker networkChecker;

    @Inject
    public OnlineGameApiServiceImpl(Socket socket, INetworkChecker networkChecker) {
        this.socket = socket;
        this.networkChecker = networkChecker;
    }

    @Override
    public Single<String> createRoom(String playerName) {
        return Single.create(new RemoteSingleOnSubscribe<>(networkChecker, emitter -> {
            socket.emit(SocketConstants.CREATE_ROOM, playerName, (Ack) args -> {
                if (args == null)
                    emitter.onError(new CreateRoomException());
                else {
                    if ((int) args[0] == ResponseCode.SUCCESS)
                        emitter.onSuccess((String) args[1]);
                }
            });
            Log.e("TAG", "createRoom: " );
            socket.connect();

        }));
    }
}
