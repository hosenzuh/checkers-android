package com.toolsforfools.checkers.data.remote.networkchecker;

public interface INetworkChecker {
    boolean isConnected();
}
