package com.toolsforfools.checkers.data.remote.api.model.auth.singup;

import com.google.gson.annotations.SerializedName;
import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;

public class SignUpResponse extends UserInfoResponse {
    @SerializedName("id")
    private String id;

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;


    public SignUpResponse(String userName, int score, String token, String id, String email, String password) {
        super(userName, score, token);
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
