package com.toolsforfools.checkers.data.remote;


import com.toolsforfools.checkers.data.remote.networkchecker.INetworkChecker;

import io.reactivex.SingleEmitter;
import io.reactivex.SingleOnSubscribe;

public class RemoteSingleOnSubscribe<T> implements SingleOnSubscribe<T> {

    INetworkChecker networkChecker;
    OnConnected<T> onConnected;

    public RemoteSingleOnSubscribe(INetworkChecker networkChecker, OnConnected<T> onConnected) {
        this.networkChecker = networkChecker;
        this.onConnected = onConnected;
    }

    @Override
    public void subscribe(SingleEmitter<T> emitter) throws Exception {
        if (networkChecker.isConnected()) {
            try {
                onConnected.onConnected(emitter);
            } catch (Exception e) {
                e.printStackTrace();
                emitter.onError(new Exception());
            }
        } else {
            emitter.onError(new Exception());
        }
    }


    public interface OnConnected<T> {
        void onConnected(SingleEmitter<T> emitter) throws Exception;
    }
}
