package com.toolsforfools.checkers.data.remote.api.service.auth;

import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginResponse;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpResponse;
import com.toolsforfools.checkers.domain.interactor.Result;

import io.reactivex.Single;

public interface AuthApiService {
    Single<Result<LoginResponse>> login(LoginRequest loginRequest);
    Single<Result<SignUpResponse>> signUp(SignUpRequest signUpRequest);


}
