package com.toolsforfools.checkers.data.executor;


import com.toolsforfools.checkers.domain.executor.ThreadExecutor;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

public class ThreadExecutorImpl implements ThreadExecutor {

    @Inject
    public ThreadExecutorImpl() {
    }

    @Override
    public Scheduler getSchedulers() {
        return Schedulers.io();
    }
}
