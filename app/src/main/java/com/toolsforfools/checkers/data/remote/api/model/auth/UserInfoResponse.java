package com.toolsforfools.checkers.data.remote.api.model.auth;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.toolsforfools.checkers.data.remote.api.model.base.BaseResponse;

public class UserInfoResponse extends BaseResponse {
    @SerializedName("name")
    @Expose
    private String userName;

    @SerializedName("score")
    @Expose
    private int score;

    @SerializedName("token")
    @Expose
    private String token;

    public UserInfoResponse(String userName, int score, String token) {
        this.userName = userName;
        this.score = score;
        this.token = token;
    }

    public String getUserName() {
        return userName;
    }

    public int getScore() {
        return score;
    }

    public String getToken() {
        return token;
    }
}
