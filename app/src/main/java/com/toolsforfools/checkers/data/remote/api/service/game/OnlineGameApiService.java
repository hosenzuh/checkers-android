package com.toolsforfools.checkers.data.remote.api.service.game;

import io.reactivex.Single;

public interface OnlineGameApiService {

    Single<String> createRoom(String playerName);
}
