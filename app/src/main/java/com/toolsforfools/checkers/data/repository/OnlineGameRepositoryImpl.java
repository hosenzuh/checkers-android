package com.toolsforfools.checkers.data.repository;

import com.toolsforfools.checkers.data.repository.datasource.IOnlineGameRemoteDataSource;
import com.toolsforfools.checkers.domain.repository.OnlineGameRepository;

import javax.inject.Inject;

import io.reactivex.Single;

public class OnlineGameRepositoryImpl implements OnlineGameRepository {

    IOnlineGameRemoteDataSource remoteDataSource;

    @Inject
    public OnlineGameRepositoryImpl(IOnlineGameRemoteDataSource remoteDataSource) {
        this.remoteDataSource = remoteDataSource;
    }

    @Override
    public Single<String> createRoom(String playerName) {
        return remoteDataSource.createRoom(playerName);
    }
}
