package com.toolsforfools.checkers.data.entity.mapper;

import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;
import com.toolsforfools.checkers.domain.entity.UserEntity;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class UserEntityMapper extends Mapper<UserEntity, UserInfoResponse> {

    @Inject
    public UserEntityMapper() {

    }

    @Override
    public UserEntity map(UserInfoResponse model) {
        if (model == null)
            return null;
        return new UserEntity(
                model.getUserName(),
               model.getScore()
        );
    }

    @Override
    public UserInfoResponse unmap(UserEntity model) {
        if (model == null)
            return null;
        return new UserInfoResponse(
                model.getUserName(),
                model.getScore(),
                ""
        );
    }
}
