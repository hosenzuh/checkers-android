package com.toolsforfools.checkers.data.remote;


import androidx.annotation.Nullable;

import static com.toolsforfools.checkers.util.constants.ResponseCode.UNAUTHORISED_USER;
import static com.toolsforfools.checkers.util.constants.ResponseCode.UNKNOWN_ERROR;
import static com.toolsforfools.checkers.util.constants.ResponseCode.UNVERIFIED_ACCOUNT;

public final class RemoteExceptionFactory {

    private RemoteExceptionFactory() {
    }

    public static Exception fromCode(int code, @Nullable String message) {
        switch (code) {
            case UNAUTHORISED_USER:
                return new Exception(message);
            case UNVERIFIED_ACCOUNT:
                return new Exception(message);
            case UNKNOWN_ERROR:
                return new Exception(message);
            default:
                return new Exception(message);
        }
    }
}
