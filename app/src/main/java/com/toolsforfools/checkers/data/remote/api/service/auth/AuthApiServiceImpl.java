package com.toolsforfools.checkers.data.remote.api.service.auth;

import android.util.Log;

import com.toolsforfools.checkers.data.entity.mapper.JsonMapper;
import com.toolsforfools.checkers.data.remote.RemoteExceptionFactory;
import com.toolsforfools.checkers.data.remote.RemoteSingleOnSubscribe;
import com.toolsforfools.checkers.data.remote.api.RetrofitService;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginResponse;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpResponse;
import com.toolsforfools.checkers.data.remote.networkchecker.INetworkChecker;
import com.toolsforfools.checkers.domain.interactor.Result;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Response;

public class AuthApiServiceImpl implements AuthApiService {

    private RetrofitService retrofit;
    private INetworkChecker networkChecker;
    private JsonMapper jsonMapper;

    @Inject
    public AuthApiServiceImpl(RetrofitService retrofit, INetworkChecker networkChecker, JsonMapper jsonMapper) {
        this.retrofit = retrofit;
        this.networkChecker = networkChecker;
        this.jsonMapper = jsonMapper;
    }

    @Override
    public Single<Result<LoginResponse>> login(LoginRequest loginRequest) {
        return Single.create(new RemoteSingleOnSubscribe<>(networkChecker, emitter -> {
            Response<LoginResponse> response = retrofit.login(loginRequest).execute();
            if (response.isSuccessful() && response.body() != null) {
                emitter.onSuccess(Result.success((LoginResponse) response.body().getBody()));
            } else {
                LoginResponse errorResponse = jsonMapper.unmap(response.errorBody().string(), LoginResponse.class);
                int code = response.code();
                emitter.onError(
                        RemoteExceptionFactory.fromCode(code, errorResponse.getMessage())
                );
            }
        }));
    }

    @Override
    public Single<Result<SignUpResponse>> signUp(SignUpRequest signUpRequest) {
        return Single.create(new RemoteSingleOnSubscribe<>(networkChecker, emitter -> {
            Response<SignUpResponse> response = retrofit.signUp(signUpRequest).execute();
            Log.e("GGG", "signUp: " + response.message());
            if (response.isSuccessful() && response.body() != null) {
                emitter.onSuccess(Result.success((SignUpResponse) response.body().getBody()));
            } else {
                SignUpResponse errorResponse = jsonMapper.unmap(response.errorBody().string(), SignUpResponse.class);
                int code = response.code();
                emitter.onError(
                        RemoteExceptionFactory.fromCode(code, errorResponse.getMessage())
                );
            }
        }));
    }
}
