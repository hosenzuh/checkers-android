package com.toolsforfools.checkers.data.remote.api;


import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.login.LoginResponse;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpRequest;
import com.toolsforfools.checkers.data.remote.api.model.auth.singup.SignUpResponse;
import com.toolsforfools.checkers.data.remote.api.model.base.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import static com.toolsforfools.checkers.data.remote.api.ApiEndPoints.BASE_URL;
import static com.toolsforfools.checkers.data.remote.api.ApiEndPoints.LOGIN;
import static com.toolsforfools.checkers.data.remote.api.ApiEndPoints.SIGN_UP;

public interface RetrofitService {

    @POST(LOGIN)
    Call<LoginResponse> login(@Body LoginRequest request);

    @POST(SIGN_UP)
    Call<SignUpResponse> signUp(@Body SignUpRequest request);
}
