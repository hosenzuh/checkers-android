package com.toolsforfools.checkers.data.remote.api.model.leaderBoard;

import com.toolsforfools.checkers.data.remote.api.model.auth.UserInfoResponse;
import com.toolsforfools.checkers.data.remote.api.model.base.BaseResponse;

import java.util.List;

public class LeaderBoardResponse extends BaseResponse {
    private List<UserInfoResponse> usersList;

    public LeaderBoardResponse(List<UserInfoResponse> usersList) {
        this.usersList = usersList;
    }

    public List<UserInfoResponse> getUsersList() {
        return usersList;
    }
}
