package com.toolsforfools.checkers.data.repository.datasource;

import io.reactivex.Single;

public interface IOnlineGameRemoteDataSource {

    Single<String> createRoom(String playerName);
}
