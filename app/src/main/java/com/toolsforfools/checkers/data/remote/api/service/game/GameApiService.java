package com.toolsforfools.checkers.data.remote.api.service.game;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

public interface GameApiService {

    DisposableObserver<Move> getSendMoveObserver();

    Observable<Move> onMoveSelected();

    Observable<Boolean> onGameJoined();

    Observable<Boolean> onGameCreated();

    Observable<String> createRoom();

    void joinGame(String roomId);
}
