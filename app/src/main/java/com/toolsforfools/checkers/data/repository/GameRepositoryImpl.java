package com.toolsforfools.checkers.data.repository;

import com.toolsforfools.checkers.data.repository.datasource.IGameRemoteDataSource;
import com.toolsforfools.checkers.domain.checkersEngine.adpater.RxGameAdapter;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.entity.DisplayMoveEntity;
import com.toolsforfools.checkers.domain.entity.PutPieceInPositionEntity;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class GameRepositoryImpl implements GameRepository {

    private RxGameAdapter rxGameAdapter;
    private IGameRemoteDataSource remoteDataSource;

    @Inject
    public GameRepositoryImpl(RxGameAdapter rxGameAdapter,IGameRemoteDataSource gameRemoteDataSource) {
        this.rxGameAdapter = rxGameAdapter;
        this.remoteDataSource = gameRemoteDataSource;
    }

    @Override
    public void createSinglePlayerGame(Player.PlayerType localPlayer, int difficultyLevel) {
        rxGameAdapter.createSinglePlayerGame(localPlayer, difficultyLevel);
    }

    @Override
    public void createMultiPlayerGame() {
        rxGameAdapter.createMultiPlayerGame();
    }

    @Override
    public void loadGame(SaveModel saveModel) {
        rxGameAdapter.loadGame(saveModel);
    }

    @Override
    public Observable<String> createRoom() {
        //for the game object
        rxGameAdapter.createRoom();
        //for the ui
        return remoteDataSource.createRoom();
    }

    @Override
    public void joinRoom(String roomId) {
        rxGameAdapter.joinRoom();
        remoteDataSource.joinGame(roomId);
    }

    @Override
    public Observable<Boolean> onGameCreated() {
        //for the game
        remoteDataSource.onGameCreated()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(rxGameAdapter.onGameCreatedObserver());
        //for the ui
        return remoteDataSource.onGameCreated();
    }

    @Override
    public Observable<Boolean> onGameJoined() {
        return remoteDataSource.onGameJoined();
    }

    @Override
    public Observable<SaveModel> saveGame() {
        return rxGameAdapter.getSaveGameObservable();
    }

    @Override
    public void makeMove(Move move) {
        rxGameAdapter.makeMove(move);
    }

    @Override
    public Observable<List<Move>> getAvailableMoves(Position position) {
        return rxGameAdapter.getAvailableMoves(position);
    }

    @Override
    public PublishSubject<DisplayMoveEntity> displayMove() {
        return rxGameAdapter.getDisplayMoveEntityObservable();
    }

    @Override
    public PublishSubject<Position> removePiece() {
        return rxGameAdapter.getRemovePieceObservable();
    }

    @Override
    public PublishSubject<Position> upgradePiece() {
        return rxGameAdapter.getUpgradePieceObservable();
    }

    @Override
    public PublishSubject<PutPieceInPositionEntity> putPieceInPosition() {
        return rxGameAdapter.getPutPieceInPositionObservable();
    }

    @Override
    public PublishSubject<Player.PlayerType> startPlayerTurn() {
        return rxGameAdapter.getStartPlayerTurnObservable();
    }

    @Override
    public PublishSubject<Boolean> displayWin() {
        return rxGameAdapter.getDisplayWinObservable();
    }

    @Override
    public PublishSubject<Boolean> displayDraw() {
        return rxGameAdapter.getDisplayDrawObservable();
    }

    @Override
    public PublishSubject<List<Piece>> displayMovablePiece() {
        return rxGameAdapter.getDisplayMovablePieceObservable();
    }

    @Override
    public DisposableObserver<Move> getSendMoveObserver() {
        return remoteDataSource.getSendMoveObserver();
    }

    @Override
    public Observable<Move> sendMove() {
        rxGameAdapter.getSendMoveObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(getSendMoveObserver());
        return rxGameAdapter.getSendMoveObservable();
    }

    @Override
    public Observable<Move> onMoveSelected() {
        remoteDataSource.onMoveSelected()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(rxGameAdapter.onMoveSelectedObserver());
        return remoteDataSource.onMoveSelected();
    }

    @Override
    public Observable<Boolean> startGame() {
        return rxGameAdapter.startGame();
    }
}
