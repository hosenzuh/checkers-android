package com.toolsforfools.checkers.data.remote.interceptor;

import com.toolsforfools.checkers.data.entity.UserStatus;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.data.remote.api.ApiEndPoints;
import com.toolsforfools.checkers.presention.bus.Bus;
import com.toolsforfools.checkers.presention.bus.event.UnAuthorizedUserEvent;
import com.toolsforfools.checkers.util.constants.ResponseCode;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@Singleton
public class AuthInterceptor implements Interceptor {

    ISharedPref sharedPref;

    @Inject
    public AuthInterceptor(ISharedPref sharedPref) {
        this.sharedPref = sharedPref;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        String token = sharedPref.getToken();
        Request newRequest = request.newBuilder().header(ApiEndPoints.TOKEN_HEADER, token).build();
        Response response = chain.proceed(newRequest);
        if (response.code() == ResponseCode.UNAUTHORISED_USER) {
            sharedPref.setUserStatus(UserStatus.NONE);
            Bus.getInstance().publish(UnAuthorizedUserEvent.getInstance());
        }
        return response;
    }
}
