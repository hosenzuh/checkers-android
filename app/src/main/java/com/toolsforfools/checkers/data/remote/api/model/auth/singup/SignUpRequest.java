package com.toolsforfools.checkers.data.remote.api.model.auth.singup;

import com.google.gson.annotations.SerializedName;

public class SignUpRequest {
    @SerializedName("username")
    private String userName;
    @SerializedName("password")
    private String password;
    @SerializedName("repeat_password")
    private String repeatPassword;
    @SerializedName("email")
    private String email;

    public SignUpRequest(String userName, String password, String repeatPassword, String email) {
        this.userName = userName;
        this.password = password;
        this.repeatPassword = repeatPassword;
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public String getEmail() {
        return email;
    }
}
