package com.toolsforfools.checkers.domain.checkersEngine.player;
import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IPlayerView;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.FakeMoveHolder;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class BotPlayer extends Player {
    private static final String TAG = "BotPlayer";
    private Board board;
    private int botLevel;
    private PlayerType playerType;
    private List<Move> playerMoves;

    public BotPlayer(MoveExecutor moveExecutor,
                     PlayerType playerType, List<Piece> playerPieces, IPlayerView playerView, Board board,int botLevel)
    {
        super(moveExecutor, playerType, playerPieces, playerView);
        this.board = board;
        this.botLevel = botLevel;
        this.playerType = playerType;
    }

    @Override
    protected void makeMove(List<Move> availableMoves) {
        playerMoves = availableMoves;
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new BotExecutor());
    }



    private double evaluateBoard(Board b){

        double result = 0;
        double kingValue = 6.5;

        result = b.getNumOfPieces(PieceType.whiteKing) * kingValue + b.getNumOfPieces(PieceType.whitePiece)
                - ( b.getNumOfPieces(PieceType.blackKing) * kingValue + b.getNumOfPieces(PieceType.blackPiece) );

        return result;
    }

    private Move bestMove(Board b,int depth) {

        double alpha = Double.NEGATIVE_INFINITY;
        double beta = Double.POSITIVE_INFINITY;

        List<Move> possibleMoves = playerMoves;
        List<Double> movesValue = new ArrayList<>();

        for(Move move : possibleMoves) {
            // clone board and make move
            FakeMoveHolder moveHolder = moveExecutor.executeFakeMove(b,move);

            movesValue.add(minimax(depth-1,alpha,beta, moveHolder));
        }
        boolean maxi = false;
        if(playerType.equals(PlayerType.White)) {
            maxi = true;
        }
        double maxValue = Double.NEGATIVE_INFINITY;
        double minValue = Double.POSITIVE_INFINITY;

        for(Double value : movesValue) {
            if(maxi) {
                if(value >= maxValue) {
                    maxValue = value;
                }
            }
            else {
                if(value <= minValue) {
                    minValue = value;
                }
            }
        }

        for(int i = 0; i < movesValue.size(); i++) {
            if(maxi) {
                if(movesValue.get(i) < maxValue) {
                    movesValue.remove(i);
                    possibleMoves.remove(i);
                    i--;
                }
            }
            else {
                if(movesValue.get(i) > minValue) {
                    movesValue.remove(i);
                    possibleMoves.remove(i);
                    i--;
                }
            }
        }

        Random rand = new Random();

        return possibleMoves.get(rand.nextInt(possibleMoves.size()));
    }

    private double minimax(int depth, double alpha, double beta, FakeMoveHolder fakeMoveHolder) {

        Board b = fakeMoveHolder.fakeBoard;
        PlayerType currPlayer = fakeMoveHolder.playerTurnType;
        List<Move> possibleMoves = fakeMoveHolder.playerAvailableMoves;
        boolean isDraw = fakeMoveHolder.isDraw;
        boolean isGameEnded = fakeMoveHolder.isGameEnded;

        if (isDraw){
            return 0;
        }

        if (isGameEnded) {
            if(currPlayer == PlayerType.White)
                return 1000;
            else
                return  -1000;
        }

        if(depth == 0) {
            return evaluateBoard(b);
        }

        boolean maxiPlayer = false;
        if(currPlayer.equals(PlayerType.White))
            maxiPlayer = true;

        double init = 0;


        if(maxiPlayer) {
            init = Double.NEGATIVE_INFINITY;
            for(Move move : possibleMoves) {

                // clone board and make move
                FakeMoveHolder moveHolder = moveExecutor.executeFakeMove(b,move);

                double result = minimax(depth-1, alpha,beta, moveHolder);


                init = Math.max( init , result );
                alpha = Math.max( alpha , init );

                if(alpha >= beta)
                    break;
            }
        }
        else {
            init = Double.POSITIVE_INFINITY;
            for(Move move : possibleMoves) {

                // clone board and make move
                FakeMoveHolder moveHolder = moveExecutor.executeFakeMove(b,move);

                double result = minimax(depth-1, alpha,beta, moveHolder);

                init = Math.min( init , result );
                beta = Math.min( beta , init );

                if(alpha >= beta)
                    break;
            }
        }
        return init;
    }


    private class BotExecutor implements Runnable{

        @Override
        public void run() {
            Move move = bestMove(board,botLevel);
            chooseMove(move);
        }
    }


}
