package com.toolsforfools.checkers.domain.checkersEngine.online.interfaces;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

public interface INetworkOperations {
    void searchOpponent(ConnectionListener connectionListener);
    void sendMove(Move move);
}
