package com.toolsforfools.checkers.domain.interactor.usecase.base;

import android.util.Log;

import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

public abstract class ParamsUseCase<T, Params> extends BaseUseCase {
    private static final String TAG = "UseCase";

    public ParamsUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor) {
        super(uiExecutor, threadExecutor);
    }

    protected abstract Observable<T> buildObservable(Params params);

    public void execute(DisposableObserver<T> disposableObserver, Params params) {
        Log.d(TAG, "execute: UseCase");
        addDisposable(
                buildObservable(params)
                        .doOnError(Throwable::printStackTrace)
                        .subscribeOn(threadExecutor.getSchedulers())
                        .observeOn(uiExecutor.getSchedulers())
                        .subscribeWith(disposableObserver)
        );
    }
}
