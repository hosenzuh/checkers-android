package com.toolsforfools.checkers.domain.checkersEngine.game.repository;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.game.Game;
import com.toolsforfools.checkers.domain.checkersEngine.game.GameSettings;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.io.Serializable;

public class SaveModel implements Serializable {
    public Board gameBoard;
    public Player.PlayerType lasPlayerTurn;
    public GameSettings gameSettings;
    public String saveDate;
    public String gameName;
}
