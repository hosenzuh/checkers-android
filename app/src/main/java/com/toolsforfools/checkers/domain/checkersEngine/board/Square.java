package com.toolsforfools.checkers.domain.checkersEngine.board;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;

import java.io.Serializable;

public class Square implements Serializable {
    private Position mPosition;
    private Piece mPiece;

    public Square(Position mPosition, Piece mPiece) {
        this.mPosition = mPosition;
        this.mPiece = mPiece;
    }

    public Square(Position mPosition) {
        this.mPosition = mPosition;
    }

    public Position getmPosition() {
        return mPosition;
    }

    public Piece getPiece() {
        return mPiece;
    }

    public void setPiece(Piece piece) {
        this.mPiece = piece;
    }

    @Override
    public String toString() {
        return "Square{" +
                "mPosition=" + mPosition +
                ", mPiece=" + mPiece.toString() +
                '}';
    }
}
