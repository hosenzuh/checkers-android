package com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.util.List;

public interface FakeMoveExecutionListener {
    void onNewFakeTurn(Player.PlayerType playerType, Board fakeBoard, List<Move> newFakeMoves);
    void onGameEnd();
    void onDraw();
}
