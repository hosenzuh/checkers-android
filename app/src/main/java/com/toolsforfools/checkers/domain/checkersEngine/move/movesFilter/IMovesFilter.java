package com.toolsforfools.checkers.domain.checkersEngine.move.movesFilter;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.util.List;

public interface IMovesFilter {
    List<Move> filter(List<Move> moveList);
}
