package com.toolsforfools.checkers.domain.checkersEngine.online;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.Game;
import com.toolsforfools.checkers.domain.checkersEngine.game.GameSettings;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.online.interfaces.ConnectionListener;
import com.toolsforfools.checkers.domain.checkersEngine.online.interfaces.INetworkOperations;

import java.util.List;

public class GameSessionManger {
    private Game game;
    private IGameView gameView;
    private INetworkOperations networkOperations;
    private GameSettings gameSettings;

    public GameSessionManger(IGameView gameView,
                             INetworkOperations networkOperations) {
        this.gameView = gameView;
        this.networkOperations = networkOperations;
    }

    public GameSessionManger(IGameView gameView, INetworkOperations networkOperations, GameSettings gameSettings) {
        this.gameView = gameView;
        this.networkOperations = networkOperations;
        this.gameSettings = gameSettings;
    }

    public void searchOpponent() {
        networkOperations.searchOpponent(new ConnectionListener() {
            @Override
            public void onGameMatched() {
                if (gameSettings != null)
                    game = new Game(gameSettings,gameView);
                else
                    game = new Game(gameView);
                game.startGame();
            }

            @Override
            public void onMoveSelected(Move move) {
                game.selectMove(move);
            }
        });
    }

    public List<Move> getCurrentPieceMoves(Position position) {
        if (game == null) {
            throw new IllegalStateException("opponent didnt match yet");
        }
        return game.getPieceMoves(position);
    }

    public void selectMove(Move move) {
        Log.e("MOVE", "selectMove: "+ move.getSourcePiece().getPosition().getX()+" " + move.getSourcePiece().getPosition().getY()+" " + move.getDestination().getX() +move.getDestination().getY() );
        if (game == null) {
            throw new IllegalStateException("opponent didnt match yet");
        }
        networkOperations.sendMove(move);
        Log.e("MOVE", "selectMove: "+ move.getSourcePiece().getPosition().getX()+" " + move.getSourcePiece().getPosition().getY()+" " + move.getDestination().getX() +move.getDestination().getY() );
        game.selectMove(move);
    }


}
