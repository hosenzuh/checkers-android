package com.toolsforfools.checkers.domain.repository;

import com.toolsforfools.checkers.domain.entity.UserEntity;
import com.toolsforfools.checkers.domain.interactor.Result;

import io.reactivex.Single;

public interface AuthRepository {

    Single<Result<UserEntity>> signUp(String userName, String password, String confirmPassword, String email);

    Single<Result<UserEntity>> login(String userName, String password);
}
