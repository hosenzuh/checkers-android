package com.toolsforfools.checkers.domain.interactor.usecase;

import android.util.Log;

import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;
import com.toolsforfools.checkers.domain.repository.OnlineGameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CreateRoomUseCase extends UseCase<String> {

    GameRepository gameRepository;

    @Inject
    public CreateRoomUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<String> buildObservable() {
        return gameRepository.createRoom();
    }

}
