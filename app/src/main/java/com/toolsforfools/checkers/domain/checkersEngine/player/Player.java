package com.toolsforfools.checkers.domain.checkersEngine.player;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IPlayerView;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Player {
    public MoveExecutor moveExecutor;
    private PlayerType playerType;
    private List<Piece> playerPieces;
    private IPlayerView playerView;

    public Player(MoveExecutor moveExecutor, PlayerType playerType,
                  List<Piece> playerPieces, IPlayerView playerView) {
        this.moveExecutor = moveExecutor;
        this.playerType = playerType;
        this.playerPieces = playerPieces;
        this.playerView = playerView;
    }

    public List<Piece> getMovablePieces(){
        List<Piece> movablePieces = new ArrayList<>();
        for (Piece piece : playerPieces){
            if (piece.getMoveList().size() > 0){
                movablePieces.add(piece);
            }
        }
        return movablePieces;
    }

    public void showMoves(List<Move> availableMoves){
        Log.e("TAG", "showMoves: " );
        setMovesToPieces(availableMoves);
        playerView.displayMovablePieces(getMovablePieces());
        makeMove(availableMoves);
    }

    private void setMovesToPieces(List<Move> availableMoves){
        Log.e("TAG", "setMovesToPieces: " +availableMoves);
        if (availableMoves == null) return;
        for (Piece piece : playerPieces){
            List<Move> pieceMoves = new ArrayList<>();
            for (Move move : availableMoves){
                if (piece.getPosition().equals(move.getSourcePiece().getPosition())){
                    pieceMoves.add(move);
                }
            }
            piece.setMoveList(pieceMoves);
        }
    }

    public void upgradePiece(Piece piece) {
        for (Piece playerPiece : playerPieces) {
            if (playerPiece.getPosition().equals(piece.getPosition())) {
                if (playerPiece.getType() == PieceType.whitePiece){
                    playerPiece.setType(PieceType.whiteKing);
                }
                if (playerPiece.getType() == PieceType.blackPiece){
                    playerPiece.setType(PieceType.blackKing);
                }

                break;
            }
        }
    }

    public List<Piece> getPlayerPieces() {
        return playerPieces;
    }

    public PlayerType getPlayerType() {
        return playerType;
    }

    public void chooseMove(Move move){
        moveExecutor.executeMove(move);
    }

    public void removePiece(Piece piece){
        for (int i = 0; i < playerPieces.size(); i++){
            Piece playerPiece = playerPieces.get(i);
            if (playerPiece.getPosition().equals(piece.getPosition())){
                playerPieces.remove(i);
                break;
            }
        }
    }

    public void updatePosition(Piece piece, Position newPosition){
        for (Piece playerPiece : playerPieces){
            if (piece.getPosition().equals(playerPiece.getPosition())){
                playerPiece.setPosition(newPosition);
            }
        }
    }

    protected abstract void makeMove(List<Move> availableMoves);

    public void setPlayerPieces(List<Piece> playerPieces) {
        this.playerPieces = playerPieces;
    }

    private String getPiecesString(){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{ ");
        for (Piece piece : playerPieces){
            stringBuilder.append(piece.toString()).append(", ").append(piece.getPosition().toString()).append("\n");
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerType=" + playerType +
                ", playerPieces=" + getPiecesString() +
                '}';
    }

    public enum PlayerType implements Serializable {
        White,
        Black
    }
}
