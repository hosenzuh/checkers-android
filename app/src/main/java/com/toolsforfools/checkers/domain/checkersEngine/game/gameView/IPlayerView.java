package com.toolsforfools.checkers.domain.checkersEngine.game.gameView;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.util.List;

public interface IPlayerView {
    void startPlayerTurn(Player.PlayerType playerType);
    void displayPlayerWin();
    void displayDraw();
    void displayMovablePieces(List<Piece> movablePieces);

}
