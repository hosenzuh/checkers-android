package com.toolsforfools.checkers.domain.entity;

import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

public class DisplayMoveEntity {
    private Position oldPosition;
    private Position newPosition;
    private Move move;

    public DisplayMoveEntity(Position oldPosition, Position newPosition, Move move) {
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
        this.move = move;
    }

    public Position getOldPosition() {
        return oldPosition;
    }

    public Position getNewPosition() {
        return newPosition;
    }

    public Move getMove() {
        return move;
    }
}
