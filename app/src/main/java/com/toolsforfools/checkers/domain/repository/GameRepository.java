package com.toolsforfools.checkers.domain.repository;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.entity.DisplayMoveEntity;
import com.toolsforfools.checkers.domain.entity.PutPieceInPositionEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.PublishSubject;

public interface GameRepository {

    //create game
    void createSinglePlayerGame(Player.PlayerType localPlayer, int difficultyLevel);

    void createMultiPlayerGame();

    void loadGame(SaveModel gamePocket);

    Observable<String> createRoom();

    void joinRoom(String roomId);

    Observable<Boolean> onGameCreated();

    Observable<Boolean> onGameJoined();

    //load game
    Observable<SaveModel> saveGame();

    //Moves

    void makeMove(Move move);

    Observable<List<Move>> getAvailableMoves(Position position);

    //CallBacks
    PublishSubject<DisplayMoveEntity> displayMove();

    PublishSubject<Position> removePiece();

    PublishSubject<Position> upgradePiece();

    PublishSubject<PutPieceInPositionEntity> putPieceInPosition();

    PublishSubject<Player.PlayerType> startPlayerTurn();

    PublishSubject<Boolean> displayWin();

    PublishSubject<Boolean> displayDraw();

    PublishSubject<List<Piece>> displayMovablePiece();

    // online

    DisposableObserver<Move> getSendMoveObserver();

    Observable<Move> sendMove();

    Observable<Move> onMoveSelected();

    Observable<Boolean> startGame();
}
