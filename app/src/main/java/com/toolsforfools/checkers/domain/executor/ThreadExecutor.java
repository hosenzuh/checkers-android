package com.toolsforfools.checkers.domain.executor;

import io.reactivex.Scheduler;

public interface ThreadExecutor {
    Scheduler getSchedulers();
}
