package com.toolsforfools.checkers.domain.checkersEngine.board;

import java.io.Serializable;

public enum PieceType implements Serializable {
    blackPiece,
    blackKing,
    whitePiece,
    whiteKing,
    NULL;

    public String getType() {
        return this.name();
    }

    public boolean isKing() {
        return this.equals(blackKing) || this.equals(whiteKing);
    }
}
