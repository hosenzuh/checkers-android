package com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.FakeMoveHolder;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

public class OfflineMoveExecutor implements MoveExecutor {
    private MoveExecutionListener moveExecutionListener;
    private Board board;

    public OfflineMoveExecutor(MoveExecutionListener moveExecutionListener, Board board) {
        this.moveExecutionListener = moveExecutionListener;
        this.board = board;
    }

    @Override
    public void executeMove(Move move) {
        Move moveCopy;
        if (move instanceof EatMove){
            moveCopy = new EatMove((EatMove) move);
        }else moveCopy = new Move(move);

        exexuteMove(move, board);
        boolean isUpgraded = upgradePiece(move, board);

        if (move instanceof EatMove){
           executeEateMove((EatMove) move, board);
            moveExecutionListener.removePieceFromPlayer((((EatMove) move).getVictemPiece().getType() == PieceType.whiteKing || ((EatMove) move).getVictemPiece().getType() == PieceType.whitePiece ) ?
                            Player.PlayerType.White : Player.PlayerType.Black
                    , ((EatMove) move).getVictemPiece());
        }

        moveExecutionListener.onMoveExecuted(moveCopy, isUpgraded);
    }

    private void exexuteMove(Move move, Board board){
        Piece movePiece = board.getPiece(move.getSourcePiece().getPosition());
        board.removePiece(movePiece.getPosition());
        board.addPiece(movePiece, move.getDestination());
    }

    private void executeEateMove(EatMove eatMove, Board board){
        board.removePiece(eatMove.getVictemPiece().getPosition());
    }

    private boolean upgradePiece(Move move, Board board){
        boolean isUpgraded = false;
        if (board.isReachedTheEnd(board.getPiece(move.getDestination()))){
            switch (move.getSourcePiece().getType()){
                case whitePiece:{
                    board.getPiece(move.getDestination()).setType(PieceType.whiteKing);
                    isUpgraded = true;
                    break;
                }

                case blackPiece:{
                    board.getPiece(move.getDestination()).setType(PieceType.blackKing);
                    isUpgraded = true;
                    break;
                }
            }
        }
        return isUpgraded;
    }

    @Override
    public FakeMoveHolder executeFakeMove(Board board, Move move) {
        Board fakeBoard = new Board(board);
        final Move moveCopy;
        if (move instanceof EatMove){
            moveCopy = new EatMove((EatMove) move);
        }else {
            moveCopy = new Move(move);
        }

        exexuteMove(moveCopy, fakeBoard);
        upgradePiece(moveCopy, fakeBoard);
        if (move instanceof EatMove){
            executeEateMove((EatMove) moveCopy, fakeBoard);
        }

       return moveExecutionListener.onFakeMoveExecuted(moveCopy, fakeBoard);
    }
}
