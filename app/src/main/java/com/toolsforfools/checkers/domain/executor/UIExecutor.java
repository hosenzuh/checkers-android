package com.toolsforfools.checkers.domain.executor;

import io.reactivex.Scheduler;

public interface UIExecutor {
    Scheduler getSchedulers();
}
