package com.toolsforfools.checkers.domain.interactor.usecase;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class StartTurnUseCase extends UseCase<Player.PlayerType> {

    private GameRepository gameRepository;

    @Inject
    public StartTurnUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Player.PlayerType> buildObservable() {
        Log.e("TAG", "buildObservable: asdasd" );
        return gameRepository.startPlayerTurn();
    }

}
