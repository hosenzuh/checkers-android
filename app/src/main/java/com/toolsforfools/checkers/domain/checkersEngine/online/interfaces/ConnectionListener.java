package com.toolsforfools.checkers.domain.checkersEngine.online.interfaces;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

public interface ConnectionListener {
    void onGameMatched();
    void onMoveSelected(Move move);
}
