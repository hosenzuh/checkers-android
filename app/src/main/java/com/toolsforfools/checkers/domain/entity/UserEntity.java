package com.toolsforfools.checkers.domain.entity;

public class UserEntity {
    private String userName;
    private int score;

    public UserEntity( String userName, int score) {
        this.userName = userName;
        this.score = score;
    }

    public String getUserName() {
        return userName;
    }

    public int getScore() {
        return score;
    }
}
