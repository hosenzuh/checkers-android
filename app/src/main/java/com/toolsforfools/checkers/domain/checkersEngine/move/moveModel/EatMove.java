package com.toolsforfools.checkers.domain.checkersEngine.move.moveModel;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;

public class EatMove extends Move {
    private Piece victemPiece;

    public EatMove(Piece sourcePiece,Piece victemPiece , Position destination) {
        super(sourcePiece, destination);
        this.victemPiece = victemPiece;
    }

    public EatMove(EatMove eatMove){
        super(eatMove);
        this.victemPiece = new Piece(eatMove.getVictemPiece());
    }

    public Piece getVictemPiece() {
        return victemPiece;
    }
}
