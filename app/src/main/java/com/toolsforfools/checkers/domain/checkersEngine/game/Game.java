package com.toolsforfools.checkers.domain.checkersEngine.game;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.GamePocket;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutionListener;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.MoveExecutor;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution.OfflineMoveExecutor;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.FakeMoveHolder;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFactory.MovesFactory;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFactory.StandardCheckersMoveFactory;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFilter.EatMovesFilter;
import com.toolsforfools.checkers.domain.checkersEngine.move.movesFilter.IMovesFilter;
import com.toolsforfools.checkers.domain.checkersEngine.player.BotPlayer;
import com.toolsforfools.checkers.domain.checkersEngine.player.HumanPlayer;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.io.Serializable;
import java.util.List;

public class Game {
    private static final String TAG = "Game";

    private Board board;
    private Player whitePlayer;
    private Player blackPlayer;
    private Player currentPlayer;
    private GameSettings gameSettings;

    private TurnManger turnManger;

    private IGameView gameView;
    private SaveModel savedGame;


    public Game(GameSettings gameSettings, IGameView gameView) {
        this.gameSettings = gameSettings;
        this.gameView = gameView;
    }

    public Game(IGameView gameView) {
        this.gameView = gameView;
        gameSettings = new GameSettings();
    }

    public Game(GamePocket gamePocket, IGameView gameView)  throws IllegalStateException{
        SaveModel saveModel = gamePocket.load();
        if (saveModel != null){
            gameSettings = saveModel.gameSettings;
            this.gameView = gameView;
            savedGame = saveModel;
        }else throw new IllegalStateException("couldnt find any saved games");

    }

    public void startGame() {
        turnManger = new OfflineTurnManger();
        if (savedGame == null) {
            turnManger.start();
        }else {
            turnManger.resume();
        }
    }

    public void save(GamePocket gamePocket){
        SaveModel saveModel = new SaveModel();
        saveModel.gameBoard = board;
        saveModel.gameSettings = gameSettings;
        saveModel.lasPlayerTurn = (currentPlayer.getPlayerType());
        gamePocket.save(saveModel);
    }

    public List<Move> getPieceMoves(Position position) {
        Log.d(TAG, "getPieceMoves: fanction called");
        for (Piece piece : currentPlayer.getPlayerPieces()) {
            if (piece.getPosition().equals(position)) {
                if (piece.getMoveList().size() == 0) {
                    Log.d(TAG, "getPieceMoves: ");
                }
                Log.d(TAG, "getPieceMoves: " + piece.getMoveList().toString());
                return piece.getMoveList();
            }
        }

        return null;
    }


    public void selectMove(Move selectedMove) throws IllegalStateException {
        if (gameSettings.getGameType() == GameType.offlineSinglePlayer &&
        currentPlayer.getPlayerType() != gameSettings.getLocalPLayerType()){
            throw new IllegalStateException("the bot player must select automatically");
        }
        currentPlayer.chooseMove(selectedMove);
    }

    public static abstract class TurnManger implements MoveExecutionListener {
        protected MoveExecutor moveExecutor;

        public TurnManger() {

        }

        protected abstract void setUpGame(Board board);

        protected abstract List<Move> getCurrentPlayerMoves();

        protected abstract void startNewTurn();

        protected abstract boolean checkWin();

        protected abstract void start();
        protected abstract void resume();

    }


    public class OfflineTurnManger extends TurnManger {
        private MovesFactory movesFactory;
        private IMovesFilter movesFilter;
        private boolean noMoves = false;

        private Board setUpBoard(){
            board = new Board(gameSettings.getBoardHeight(), gameSettings.getBoardWidth(),
                    gameSettings.getOccupiedLines(), gameView);
            return board;
        }
        @Override
        protected void setUpGame(Board board) {
            if (board == null) board = setUpBoard();
            moveExecutor = new OfflineMoveExecutor(this, board);
            movesFactory = new StandardCheckersMoveFactory(board);
            movesFilter = new EatMovesFilter();
            setUpPlayers();

        }

        private void setUpPlayers(){
            if (gameSettings.getGameType().equals(GameType.offlineSinglePlayer)){
                if (gameSettings.getLocalPLayerType() == Player.PlayerType.White){
                    whitePlayer = new HumanPlayer(moveExecutor, Player.PlayerType.White, board.getPlayerPieces(Player.PlayerType.White), gameView);
                    blackPlayer = new BotPlayer(moveExecutor, Player.PlayerType.Black, board.getPlayerPieces(Player.PlayerType.Black), gameView, board, gameSettings.getBotLevel());

                }else {
                    blackPlayer = new HumanPlayer(moveExecutor, Player.PlayerType.Black, board.getPlayerPieces(Player.PlayerType.Black), gameView);
                    whitePlayer = new BotPlayer(moveExecutor, Player.PlayerType.White, board.getPlayerPieces(Player.PlayerType.White), gameView, board, gameSettings.getBotLevel());
                }
            }
            else {
                blackPlayer = new HumanPlayer(moveExecutor, Player.PlayerType.Black, board.getPlayerPieces(Player.PlayerType.Black), gameView);
                whitePlayer = new HumanPlayer(moveExecutor, Player.PlayerType.White, board.getPlayerPieces(Player.PlayerType.White), gameView);
            }

        }

        @Override
        public void onMoveExecuted(Move executedMove, boolean isPieceUpgraded) {
            whitePlayer.setPlayerPieces(board.getPlayerPieces(Player.PlayerType.White));
            blackPlayer.setPlayerPieces(board.getPlayerPieces(Player.PlayerType.Black));
            gameView.displayMove(executedMove.getSourcePiece().getPosition(), executedMove.getDestination(), executedMove);
            if (isPieceUpgraded) {
                onPieceUpgraded(board.getPiece(executedMove.getDestination()));
            }
            if (executedMove instanceof EatMove) {
                List<Move> moveList = movesFilter.filter(movesFactory.getAvailableMoves(board.getPiece(executedMove.getDestination())));
                if (moveList != null && moveList.size() > 0) {
                    Log.d(TAG, "startNewTurn: " + board.toString());
                    Log.d(TAG, "startNewTurn: " + currentPlayer.toString());
                    currentPlayer.showMoves(moveList);
                } else {
                    Log.e(TAG, "onMoveExecuted: " + currentPlayer.getPlayerType());
                    startNewTurn();
                }
            } else {
                Log.e(TAG, "onMoveExecuted: " + currentPlayer.getPlayerType());
                startNewTurn();
            }

        }

        @Override
        public FakeMoveHolder onFakeMoveExecuted(Move move, Board board) {
            MovesFactory fakeMovesFactory = new StandardCheckersMoveFactory(board);
            FakeMoveHolder fakeMoveHolder = new FakeMoveHolder();
            PieceType pieceType = move.getSourcePiece().getType();
            Player.PlayerType currentPlayer = pieceType == PieceType.whitePiece || pieceType == PieceType.whiteKing ? Player.PlayerType.White : Player.PlayerType.Black;

            if (move instanceof EatMove) {
                List<Move> moveList = movesFilter.filter(fakeMovesFactory.getAvailableMoves(board.getPiece(move.getDestination())));
                if (moveList != null && moveList.size() > 0) {
                    fakeMoveHolder.fakeBoard = board;
                    fakeMoveHolder.playerAvailableMoves = moveList;
                    fakeMoveHolder.playerTurnType = currentPlayer;

                } else {
                    fakeMoveHolder = startNewFakeTurn(board, currentPlayer, fakeMovesFactory);
                }
            } else {
                fakeMoveHolder = startNewFakeTurn(board, currentPlayer, fakeMovesFactory);
            }
            return fakeMoveHolder;
        }

        private FakeMoveHolder startNewFakeTurn(Board board, Player.PlayerType currentPlayer, MovesFactory fakeMovesFactory) {
            FakeMoveHolder fakeMoveHolder = new FakeMoveHolder();

            Player.PlayerType nextPlayerTurn = (currentPlayer == Player.PlayerType.Black) ?
                    Player.PlayerType.White : Player.PlayerType.Black;

            if (checkWin(currentPlayer, board)) {
                fakeMoveHolder.isGameEnded = true;
                fakeMoveHolder.playerTurnType = currentPlayer;
            } else {
                List<Move> availableMoves = getCurrentPlayerMoves(fakeMovesFactory, nextPlayerTurn, board);
                if (availableMoves == null || availableMoves.size() == 0) {
                    List<Move> anotherPlaerAvailableMoves = getCurrentPlayerMoves(fakeMovesFactory, currentPlayer, board);
                    if (anotherPlaerAvailableMoves == null || anotherPlaerAvailableMoves.size() == 0) {
                        fakeMoveHolder.isDraw = true;
                    } else {
                        fakeMoveHolder.playerTurnType = currentPlayer;
                        fakeMoveHolder.fakeBoard = board;
                        fakeMoveHolder.playerAvailableMoves = anotherPlaerAvailableMoves;
                    }
                } else {
                    fakeMoveHolder.playerAvailableMoves = availableMoves;
                    fakeMoveHolder.playerTurnType = nextPlayerTurn;
                    fakeMoveHolder.fakeBoard = board;
                }
            }
            return fakeMoveHolder;
        }

        @Override
        public void removePieceFromPlayer(Player.PlayerType playerType, Piece piece) {
            gameView.removePiece(piece.getPosition());

        }

        @Override
        public void onPieceUpgraded(Piece piece) {
            gameView.upgradePiece(piece.getPosition());
        }

        @Override
        protected List<Move> getCurrentPlayerMoves() {
            List<Move> movesList = movesFactory.getAvailableMoves(currentPlayer.getPlayerPieces());
            List<Move> filteredList = movesFilter.filter(movesList);
            Log.e(TAG, "getCurrentPlayerMoves: "+filteredList.size() );
            Log.e(TAG, "getCurrentPlayerMoves: "+movesList.size() );
            if (filteredList != null && filteredList.size() > 0) return filteredList;
            return movesList;
        }

        private List<Move> getCurrentPlayerMoves(MovesFactory movesFactory, Player.PlayerType playerType, Board board) {
            List<Move> movesList = movesFactory.getAvailableMoves(board.getPlayerPieces(playerType));
            List<Move> filteredList = movesFilter.filter(movesList);
            if (filteredList != null && filteredList.size() > 0) return filteredList;
            return movesList;
        }

        @Override
        protected void startNewTurn() {
            Log.e(TAG, "startNewTurn: "+currentPlayer );
            if (checkWin()) {
                gameView.displayPlayerWin();
                return;
            }
            if (currentPlayer.getPlayerType() == Player.PlayerType.White) {
                currentPlayer = blackPlayer;
            } else {
                currentPlayer = whitePlayer;
            }
            currentPlayer.setPlayerPieces(board.getPlayerPieces(currentPlayer.getPlayerType()));
            List<Move> availableMoves = getCurrentPlayerMoves();
            if (availableMoves == null || availableMoves.size() == 0) {
                if (noMoves) {
                    gameView.displayDraw();
                } else {
                    noMoves = true;
                    startNewTurn();
                }
            } else {
                noMoves = false;
                currentPlayer.showMoves(availableMoves);
                Log.d(TAG, "startNewTurn: " + board.toString());
                Log.d(TAG, "startNewTurn: " + currentPlayer.toString());
                gameView.startPlayerTurn(currentPlayer.getPlayerType());
            }
        }

        @Override
        protected void start() {
            setUpGame(null);
            switch (gameSettings.getFirstTurn()) {
                case Black: {
                    currentPlayer = blackPlayer;
                    break;
                }

                case White: {
                    currentPlayer = whitePlayer;
                    break;
                }
            }
            Log.e(TAG, "start: " );
            currentPlayer.showMoves(getCurrentPlayerMoves());
            Log.e(TAG, "start: " );
            gameView.startPlayerTurn(currentPlayer.getPlayerType());

        }

        @Override
        protected void resume() {
            board = savedGame.gameBoard;
            board.setBoardView(gameView);
            board.render();
           setUpGame(board);
            if (savedGame.lasPlayerTurn == Player.PlayerType.White){
                currentPlayer = whitePlayer;
            }else {
                currentPlayer = blackPlayer;
            }

            currentPlayer.showMoves(getCurrentPlayerMoves());
            gameView.startPlayerTurn(currentPlayer.getPlayerType());

        }

        @Override
        protected boolean checkWin() {
            if (currentPlayer.getPlayerType() == Player.PlayerType.White) {
                return blackPlayer.getPlayerPieces().size() == 0;
            } else {
                return whitePlayer.getPlayerPieces().size() == 0;
            }
        }

        private boolean checkWin(Player.PlayerType playerType, Board board) {
            switch (playerType) {
                case White: {
                    return !board.hasPieces(Player.PlayerType.Black);
                }

                case Black: {
                    return !board.hasPieces(Player.PlayerType.White);
                }
            }
            throw new IllegalArgumentException("Player type undefined: " + playerType.toString());
        }
    }


    public enum GameType implements Serializable {
        offlineSinglePlayer,
        offlineMultiPlayer,
        onlineMultiPlayer
    }

}
