package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class MakeMoveUseCase extends ParamsUseCase<Boolean, MakeMoveUseCase.MakeMoveParams> {

    private GameRepository gameRepository;

    @Inject
    public MakeMoveUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Boolean> buildObservable(MakeMoveParams makeMoveParams) {
        gameRepository.makeMove(makeMoveParams.move);
        return Observable.just(true);
    }

    public static class MakeMoveParams {

        private Move move;

        private MakeMoveParams(Move move) {
            this.move = move;
        }

        public static MakeMoveParams create(Move move) {
            return new MakeMoveParams(move);
        }
    }
}
