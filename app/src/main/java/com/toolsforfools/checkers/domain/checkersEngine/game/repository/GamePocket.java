package com.toolsforfools.checkers.domain.checkersEngine.game.repository;

public interface GamePocket {

    void save(SaveModel saveModel);
    SaveModel load();
}

