package com.toolsforfools.checkers.domain.interactor.usecase.auth;

import com.toolsforfools.checkers.domain.entity.UserEntity;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.Result;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.AuthRepository;

import javax.inject.Inject;

import io.reactivex.Observable;


public class SignUpUseCase extends ParamsUseCase<Result<UserEntity>, SignUpUseCase.SignUpParams> {

    private AuthRepository authRepository;

    @Inject
    public SignUpUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, AuthRepository authRepository) {
        super(uiExecutor, threadExecutor);
        this.authRepository = authRepository;
    }

    @Override
    protected Observable<Result<UserEntity>> buildObservable(SignUpParams signUpParams) {
        return authRepository.signUp(signUpParams.userName, signUpParams.password, signUpParams.confirmPassword, signUpParams.email)
                .toObservable();
    }

    public static final class SignUpParams {
        private String userName;
        private String email;
        private String password;
        private String confirmPassword;

        public SignUpParams(String userName, String email, String password, String confirmPassword) {
            this.userName = userName;
            this.email = email;
            this.password = password;
            this.confirmPassword = confirmPassword;
        }

        public static SignUpParams create(String userName, String email, String password, String confirmPassword) {
            return new SignUpParams(userName, email, password, confirmPassword);
        }
    }
}
