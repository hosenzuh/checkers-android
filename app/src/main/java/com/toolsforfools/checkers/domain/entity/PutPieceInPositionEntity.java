package com.toolsforfools.checkers.domain.entity;

import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;

public class PutPieceInPositionEntity {
    private PieceType pieceType;
    private Position position;

    public PutPieceInPositionEntity(PieceType pieceType, Position position) {
        this.pieceType = pieceType;
        this.position = position;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public Position getPosition() {
        return position;
    }
}
