package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class OnMoveSelectedUseCase extends UseCase<Move> {

    GameRepository gameRepository;

    @Inject
    public OnMoveSelectedUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Move> buildObservable() {
        return gameRepository.onMoveSelected();
    }
}
