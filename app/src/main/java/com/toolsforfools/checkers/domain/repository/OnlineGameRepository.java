package com.toolsforfools.checkers.domain.repository;

import io.reactivex.Single;

public interface OnlineGameRepository {

    Single<String> createRoom(String playerName);
}
