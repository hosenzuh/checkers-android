package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CreateSinglePlayerGameUseCase extends ParamsUseCase<Boolean, CreateSinglePlayerGameUseCase.CreateSinglePlayerGameParams> {

    private GameRepository gameRepository;

    @Inject
    public CreateSinglePlayerGameUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Boolean> buildObservable(CreateSinglePlayerGameParams createSinglePlayerGameParams) {
        gameRepository.createSinglePlayerGame(createSinglePlayerGameParams.localPlayer, createSinglePlayerGameParams.difficultyLevel);
        return Observable.just(true);
    }

    public static class CreateSinglePlayerGameParams {
        private Player.PlayerType localPlayer;
        private int difficultyLevel;

        private CreateSinglePlayerGameParams(Player.PlayerType localPlayer, int difficultyLevel) {
            this.localPlayer = localPlayer;
            this.difficultyLevel = difficultyLevel;
        }

        public static CreateSinglePlayerGameParams create(Player.PlayerType localPlayer, int difficultyLevel) {
            return new CreateSinglePlayerGameParams(localPlayer, difficultyLevel);
        }
    }
}
