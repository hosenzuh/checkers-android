package com.toolsforfools.checkers.domain.interactor.usecase.auth;


import com.toolsforfools.checkers.domain.entity.UserEntity;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.Result;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.AuthRepository;

import javax.inject.Inject;

import io.reactivex.Observable;


public class LoginUseCase extends ParamsUseCase<Result<UserEntity>, LoginUseCase.LoginParams> {

    private AuthRepository authRepository;

    @Inject
    public LoginUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, AuthRepository authRepository) {
        super(uiExecutor, threadExecutor);
        this.authRepository = authRepository;
    }

    @Override
    protected Observable<Result<UserEntity>> buildObservable(LoginParams loginParams) {
        return authRepository.login(loginParams.userName, loginParams.password).toObservable();
    }


    public static final class LoginParams {
        private String userName;
        private String password;

        public LoginParams(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }

        public static LoginParams create(String userName, String password) {
            return new LoginParams(userName, password);
        }
    }
}