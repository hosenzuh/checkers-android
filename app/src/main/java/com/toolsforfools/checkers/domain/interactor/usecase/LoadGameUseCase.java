package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class LoadGameUseCase extends ParamsUseCase<Boolean, LoadGameUseCase.LoadGameParams> {

    GameRepository gameRepository;

    @Inject
    public LoadGameUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Boolean> buildObservable(LoadGameParams loadGameParams) {
        gameRepository.loadGame(loadGameParams.saveModel);
        return Observable.just(true);
    }

    public static class LoadGameParams {
        private SaveModel saveModel;

        private LoadGameParams(SaveModel saveModel) {
            this.saveModel = saveModel;
        }

        public static LoadGameParams create(SaveModel saveModel) {
            return new LoadGameParams(saveModel);
        }
    }
}
