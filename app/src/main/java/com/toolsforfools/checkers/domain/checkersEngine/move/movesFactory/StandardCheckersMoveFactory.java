package com.toolsforfools.checkers.domain.checkersEngine.move.movesFactory;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class StandardCheckersMoveFactory implements MovesFactory {
    private Board board;

    public StandardCheckersMoveFactory(Board board) {
        this.board = board;
    }


    @Override
    public List<Move> getAvailableMoves(List<Piece> pieceList) {
        List<Move> availableMoves = new ArrayList<>();
        Log.e("TAG", "getAvailableMoves: ");
        for (Piece piece : pieceList) {
            List<Move> pieceMoves = checkPieceMoves(piece);
            if (pieceMoves != null) {
                availableMoves.addAll(pieceMoves);
            }
        }
        Log.e("TAG", "getAvailableMoves: " + availableMoves.size());
        return availableMoves.size() == 0 ? null : availableMoves;
    }

    @Override
    public List<Move> getAvailableMoves(Piece piece) {
        List<Move> availableMoves = new ArrayList<>();
        List<Move> pieceMoves = checkPieceMoves(piece);
        if (pieceMoves != null) {
            availableMoves.addAll(pieceMoves);
        }
        return availableMoves.size() == 0 ? null : availableMoves;
    }

    private List<Move> checkPieceMoves(Piece piece) {
        Log.e("TAG", "checkPieceMoves: " + piece.getPosition());
        List<Move> pieceMoves = new ArrayList<>();

        List<Move> walkMoves = checkWalkMoves(piece);
        List<Move> eatMoves = checkEatMoves(piece);

        if (eatMoves != null) {
            pieceMoves.addAll(eatMoves);
        }

        if (walkMoves != null) {
            pieceMoves.addAll(walkMoves);
        }

        Log.e("TAG", "checkPieceMoves: " + pieceMoves.size());
        return pieceMoves.size() == 0 ? null : pieceMoves;
    }


    private List<Move> checkEatMoves(Piece piece) {
        List<Move> moveList = new ArrayList<>();

        EatMove upLeftEatMove = canEat(piece, MoveDirection.upLeft);
        if (upLeftEatMove != null) moveList.add(upLeftEatMove);

        EatMove upRightEatMove = canEat(piece, MoveDirection.upRight);
        if (upRightEatMove != null) moveList.add(upRightEatMove);

        if (piece.getType() == PieceType.whiteKing || piece.getType() == PieceType.blackKing) {
            EatMove downLeftEat = canEat(piece, MoveDirection.downLeft);
            if (downLeftEat != null) moveList.add(downLeftEat);

            EatMove downRightEat = canEat(piece, MoveDirection.downRight);
            if (downRightEat != null) moveList.add(downRightEat);
        }

        return moveList.size() == 0 ? null : moveList;
    }


    private List<Move> checkWalkMoves(Piece piece) {
        List<Move> moveList = new ArrayList<>();
        Log.e("TAG", "checkWalkMoves: ");
        Position upLeftPos = canMove(piece.getPosition(), piece.getType(), MoveDirection.upLeft);
        Log.e("TAG", "checkWalkMoves: ");
        if (upLeftPos != null) {
            moveList.add(new Move(new Piece(piece), upLeftPos));
        }

        Position upRightPos = canMove(piece.getPosition(), piece.getType(), MoveDirection.upRight);
        if (upRightPos != null) {
            moveList.add(new Move(new Piece(piece), upRightPos));
        }

        if (piece.getType() == PieceType.whiteKing || piece.getType() == PieceType.blackKing) {
            Position downLeftPos = canMove(piece.getPosition(), piece.getType(), MoveDirection.downLeft);
            if (downLeftPos != null) {
                moveList.add(new Move(new Piece(piece), downLeftPos));
            }

            Position downRightPos = canMove(piece.getPosition(), piece.getType(), MoveDirection.downRight);
            if (downRightPos != null) {
                moveList.add(new Move(new Piece(piece), downRightPos));
            }
        }
        Log.e("TAG", "checkWalkMoves: " + moveList.size());
        return moveList.size() == 0 ? null : moveList;
    }


    private EatMove canEat(Piece piece, MoveDirection moveDirection) {
        EatMove eatMove = null;
        switch (piece.getType()) {
            case whitePiece:
            case blackPiece: {
                switch (moveDirection) {
                    case upLeft: {
                        eatMove = canEatUpLeft(piece);
                        break;
                    }

                    case upRight: {
                        eatMove = canEatUpRight(piece);
                        break;
                    }
                }
                break;
            }

            case blackKing:
            case whiteKing: {
                switch (moveDirection) {
                    case upLeft: {
                        eatMove = canEatUpLeft(piece);
                        break;
                    }

                    case upRight: {
                        eatMove = canEatUpRight(piece);
                        break;
                    }

                    case downRight: {
                        eatMove = canEatDownRight(piece);
                        break;
                    }

                    case downLeft: {
                        eatMove = canEatDownLeft(piece);
                    }
                }
                break;
            }
        }
        return eatMove;
    }

    private Position canMove(Position position, PieceType pieceType, MoveDirection moveDirection) {
        Position newPosition = null;
        Log.e("TAG", "canMove: ");
        switch (pieceType) {

            case blackPiece:
            case whitePiece: {
                switch (moveDirection) {
                    case upLeft: {
                        Log.e("TAG", "canMove: " + newPosition);
                        newPosition = canMoveUpLeft(position, pieceType);
                        Log.e("TAG", "canMove: " + newPosition);
                        break;
                    }

                    case upRight: {
                        newPosition = canMoveUpRight(position, pieceType);
                        Log.e("TAG", "canMove: " + newPosition);
                        break;
                    }
                }
                break;
            }

            case blackKing:
            case whiteKing: {
                switch (moveDirection) {
                    case upRight: {
                        newPosition = canMoveUpRight(position, pieceType);
                        Log.e("TAG", "canMove: " + newPosition);
                        break;
                    }

                    case upLeft: {
                        newPosition = canMoveUpLeft(position, pieceType);
                        Log.e("TAG", "canMove: " + newPosition);
                        break;
                    }

                    case downLeft: {
                        newPosition = canMoveDownLeft(position, pieceType);
                        Log.e("TAG", "canMove: " + newPosition);
                        break;
                    }

                    case downRight: {
                        newPosition = canMoveDownRight(position, pieceType);
                        Log.e("TAG", "canMove: " + newPosition);
                        break;
                    }
                }
                break;
            }
        }
                        Log.e("TAG", "canMove: " + newPosition);

        return newPosition;
    }

    private Position canMoveUpLeft(Position position, @NotNull PieceType pieceType) {
        Position newPosition = null;

        switch (pieceType) {
            case whiteKing:
            case whitePiece: {
                Log.e("TAG", "canMoveUpLeft: "+position.getX()+" " + position.getY() );
                newPosition = new Position(position.getX() + 1, position.getY() - 1);
                Log.e("TAG", "canMoveUpLeft: "+board.canMove(newPosition) );
                if (!board.canMove(newPosition)) newPosition = null;
                Log.e("TAG", "canMoveUpLeft: "+newPosition );
                break;
            }

            case blackKing:
            case blackPiece: {
                newPosition = new Position(position.getX() - 1, position.getY() - 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }
        }

        return newPosition;
    }

    private Position canMoveUpRight(Position position, PieceType pieceType) {
        Position newPosition = null;

        switch (pieceType) {
            case whiteKing:
            case whitePiece: {
                newPosition = new Position(position.getX() + 1, position.getY() + 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }

            case blackKing:
            case blackPiece: {
                newPosition = new Position(position.getX() - 1, position.getY() + 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }
        }

        return newPosition;
    }


    private Position canMoveDownLeft(Position position, PieceType pieceType) {
        Position newPosition = null;

        switch (pieceType) {
            case whiteKing:
            case whitePiece: {
                newPosition = new Position(position.getX() - 1, position.getY() - 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }

            case blackKing:
            case blackPiece: {
                newPosition = new Position(position.getX() + 1, position.getY() - 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }
        }

        return newPosition;
    }

    private Position canMoveDownRight(Position position, PieceType pieceType) {
        Position newPosition = null;

        switch (pieceType) {
            case whiteKing:
            case whitePiece: {
                newPosition = new Position(position.getX() - 1, position.getY() + 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }

            case blackKing:
            case blackPiece: {
                newPosition = new Position(position.getX() + 1, position.getY() + 1);
                if (!board.canMove(newPosition)) newPosition = null;
                break;
            }
        }

        return newPosition;
    }


    private EatMove canEatUpLeft(Piece piece) {
        EatMove eatMove = null;
        Position victimPosition;

        PieceType pieceType = piece.getType();
        Position position = piece.getPosition();

        switch (pieceType) {

            case whiteKing:
            case whitePiece: {

                victimPosition = new Position(position.getX() + 1, position.getY() - 1);
                Piece victim = board.getBlackPiece(victimPosition);
                Position newPosition = canMoveUpLeft(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }
                break;
            }

            case blackKing:
            case blackPiece: {
                victimPosition = new Position(position.getX() - 1, position.getY() - 1);
                Piece victim = board.getWhitePiece(victimPosition);
                Position newPosition = canMoveUpLeft(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }

                break;
            }
        }
        return eatMove;
    }

    private EatMove canEatUpRight(Piece piece) {
        EatMove eatMove = null;
        Position victimPosition;

        PieceType pieceType = piece.getType();
        Position position = piece.getPosition();

        switch (pieceType) {

            case whiteKing:
            case whitePiece: {

                victimPosition = new Position(position.getX() + 1, position.getY() + 1);
                Piece victim = board.getBlackPiece(victimPosition);
                Position newPosition = canMoveUpRight(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }
                break;
            }

            case blackKing:
            case blackPiece: {
                victimPosition = new Position(position.getX() - 1, position.getY() + 1);
                Piece victim = board.getWhitePiece(victimPosition);
                Position newPosition = canMoveUpRight(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }

                break;
            }
        }
        return eatMove;
    }


    private EatMove canEatDownLeft(Piece piece) {
        EatMove eatMove = null;
        Position victimPosition;

        PieceType pieceType = piece.getType();
        Position position = piece.getPosition();

        switch (pieceType) {

            case whiteKing:
            case whitePiece: {

                victimPosition = new Position(position.getX() - 1, position.getY() - 1);
                Piece victim = board.getBlackPiece(victimPosition);
                Position newPosition = canMoveDownLeft(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }
                break;
            }

            case blackKing:
            case blackPiece: {
                victimPosition = new Position(position.getX() + 1, position.getY() - 1);
                Piece victim = board.getWhitePiece(victimPosition);
                Position newPosition = canMoveDownLeft(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }

                break;
            }
        }
        return eatMove;
    }

    private EatMove canEatDownRight(Piece piece) {
        EatMove eatMove = null;
        Position victimPosition;

        PieceType pieceType = piece.getType();
        Position position = piece.getPosition();

        switch (pieceType) {

            case whiteKing:
            case whitePiece: {

                victimPosition = new Position(position.getX() - 1, position.getY() + 1);
                Piece victim = board.getBlackPiece(victimPosition);
                Position newPosition = canMoveDownRight(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }
                break;
            }

            case blackKing:
            case blackPiece: {
                victimPosition = new Position(position.getX() + 1, position.getY() + 1);
                Piece victim = board.getWhitePiece(victimPosition);
                Position newPosition = canMoveDownRight(victimPosition, pieceType);
                if (victim != null && newPosition != null) {
                    eatMove = new EatMove(new Piece(piece), victim, newPosition);
                }

                break;
            }
        }
        return eatMove;
    }


    enum MoveDirection {
        upLeft,
        upRight,
        downLeft,
        downRight
    }
}
