package com.toolsforfools.checkers.domain.checkersEngine.board;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

import java.io.Serializable;
import java.util.List;

public class Piece implements Serializable {
    @SerializedName("position")
    private Position mPosition;
    @SerializedName("piece_type")
    private PieceType mType;
    private transient List<Move> moveList;

    public Piece(Position mPosition, PieceType mType) {
        this.mPosition = mPosition;
        this.mType = mType;
    }

    public Piece(Piece piece){
        this.mPosition = new Position(piece.getPosition());
        this.mType = piece.getType();
        this.moveList = piece.getMoveList();
    }

    public void setMoveList(List<Move> moveList) {
        this.moveList = moveList;
    }

    public PieceType getType() {
        return mType;
    }

    public Position getPosition() {
        return mPosition;
    }

    public void setPosition(Position position) {
        this.mPosition = position;
    }

    public void setType(PieceType type) {
        this.mType = type;
    }

    public List<Move> getMoveList() {
        return moveList;
    }

    @Override
    public String toString() {
        return "Piece{" +
                "mType=" + mType +
                '}';
    }
}
