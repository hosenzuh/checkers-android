package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.entity.DisplayMoveEntity;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DisplayMoveUseCase extends UseCase<DisplayMoveEntity> {

    private GameRepository gameRepository;

    @Inject
    public DisplayMoveUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<DisplayMoveEntity> buildObservable() {
        return gameRepository.displayMove();
    }


}
