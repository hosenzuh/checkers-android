package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.game.Game;
import com.toolsforfools.checkers.domain.checkersEngine.game.GameSettings;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import okhttp3.internal.http.HttpHeaders;

public class SaveGameUseCase extends UseCase<SaveModel> {

    GameRepository gameRepository;

    @Inject
    public SaveGameUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor,GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<SaveModel> buildObservable() {
        return gameRepository.saveGame();
    }

}
