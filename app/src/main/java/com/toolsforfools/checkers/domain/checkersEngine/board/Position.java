package com.toolsforfools.checkers.domain.checkersEngine.board;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.annotation.Nullable;

public class Position implements Serializable {
    @SerializedName("x")
    private int x;
    @SerializedName("y")
    private int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(Position position){
        this.x = position.x;
        this.y = position.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (!(obj instanceof Position)) throw new IllegalStateException("operation equals not allowed");
        return ((Position) obj).getX() == x && ((Position) obj).getY() == y;
    }

    @Override
    public String
    toString() {
        return "Position{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
