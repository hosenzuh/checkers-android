package com.toolsforfools.checkers.domain.checkersEngine.move.moveModel;

import com.google.gson.annotations.SerializedName;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;

public class Move {

    @SerializedName("source_piece")
    private Piece sourcePiece;
    @SerializedName("destination")
    private Position destination;

    public Move(Piece sourcePiece, Position destination) {
        this.sourcePiece = sourcePiece;
        this.destination = destination;
    }

    public Move (Move move){
        this.sourcePiece = new Piece(move.getSourcePiece());
        this.destination = new Position(move.getDestination());
    }
    public Piece getSourcePiece() {
        return sourcePiece;
    }

    public Position getDestination() {
        return destination;
    }

    @Override
    public String toString() {
        return "Move{" +
                "sourcePiece=" + sourcePiece.toString() +
                ", position=" + sourcePiece.getPosition().toString() +
                ", destination=" + destination.toString() +
                '}';
    }
}
