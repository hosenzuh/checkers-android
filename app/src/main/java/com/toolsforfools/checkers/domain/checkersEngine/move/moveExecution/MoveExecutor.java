package com.toolsforfools.checkers.domain.checkersEngine.move.moveExecution;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.FakeMoveHolder;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;

public interface MoveExecutor {
     void executeMove(Move move);
     FakeMoveHolder executeFakeMove(Board board, Move move);
}
