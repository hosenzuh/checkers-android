package com.toolsforfools.checkers.domain.checkersEngine.game;

import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.io.Serializable;

public class GameSettings implements Serializable {
    private int boardWidth = 8;
    private int boardHeight = 8;
    private int occupiedLines = 3;
    private Player.PlayerType firstTurn = Player.PlayerType.White;
    private Player.PlayerType localPLayerType = Player.PlayerType.White;
    private Game.GameType gameType = Game.GameType.offlineMultiPlayer;
    private int botLevel = 10;

     GameSettings(){
    }

    private void setBoardWidth(int boardWidth) {
        this.boardWidth = boardWidth;
    }

    private void setBoardHeight(int boardHeight) {
        this.boardHeight = boardHeight;
    }

    private void setOccupiedLines(int occupiedLines) {
        this.occupiedLines = occupiedLines;
    }

    private void setFirstTurn(Player.PlayerType firstTurn) {
        this.firstTurn = firstTurn;
    }

    private void setGameType(Game.GameType gameType) {
        this.gameType = gameType;
    }

    private void setLocalPLayerType(Player.PlayerType localPLayerType) {
        this.localPLayerType = localPLayerType;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public int getOccupiedLines() {
        return occupiedLines;
    }

    private void setBotLevel(int botLevel) {
        this.botLevel = botLevel;
    }

    public Player.PlayerType getLocalPLayerType() {
        return localPLayerType;
    }

    public int getBotLevel() {
        return botLevel;
    }

    public Player.PlayerType getFirstTurn() {
        return firstTurn;
    }

    public Game.GameType getGameType() {
        return gameType;
    }

    public static class Builder{

        private GameSettings gameSettings = new GameSettings();

        public Builder setBoardWidth(int width){
            gameSettings.setBoardWidth(width);
            return this;
        }

        public Builder setBoardHeight(int height){
            gameSettings.setBoardHeight(height);
            return this;
        }

        public Builder setOccupiedLines(int occupiedLines){
            gameSettings.setOccupiedLines(occupiedLines);
            return this;
        }

        public Builder setFirstTurnPlayer(Player.PlayerType player){
            gameSettings.setFirstTurn(player);
            return this;
        }

        public Builder gameType(Game.GameType gameType){
            gameSettings.setGameType(gameType);
            return this;
        }

        public Builder setBotLevel(int botLevel){
            gameSettings.setBotLevel(botLevel);
            return this;
        }

        public Builder setLocalPlayerType(Player.PlayerType playerType){
            gameSettings.setLocalPLayerType(playerType);
            return this;
        }
        public GameSettings build(){
            return gameSettings;
        }
    }
}