package com.toolsforfools.checkers.domain.checkersEngine.adpater;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.Game;
import com.toolsforfools.checkers.domain.checkersEngine.game.GameSettings;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.GamePocket;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.online.GameSessionManger;
import com.toolsforfools.checkers.domain.checkersEngine.online.interfaces.ConnectionListener;
import com.toolsforfools.checkers.domain.checkersEngine.online.interfaces.INetworkOperations;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.entity.DisplayMoveEntity;
import com.toolsforfools.checkers.domain.entity.PutPieceInPositionEntity;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.PublishSubject;

public class RxGameAdapter implements IGameView, GamePocket, INetworkOperations {

    private Game game;
    private GameSessionManger gameSessionManger;
    private GameSettings gameSettings;
    private SaveModel saveModel;
    private Move moveToSend;
    private boolean isOnline = false;
    private PublishSubject<DisplayMoveEntity> displayMoveEntityObservable = PublishSubject.create();
    private PublishSubject<Position> removePieceObservable = PublishSubject.create();
    private PublishSubject<Position> upgradePieceObservable = PublishSubject.create();
    private PublishSubject<PutPieceInPositionEntity> putPieceInPositionObservable = PublishSubject.create();
    private PublishSubject<Player.PlayerType> startPlayerTurnObservable = PublishSubject.create();
    private PublishSubject<Boolean> displayWinObservable = PublishSubject.create();
    private PublishSubject<Boolean> displayDrawObservable = PublishSubject.create();
    private PublishSubject<List<Piece>> displayMovablePieceObservable = PublishSubject.create();
    private PublishSubject<SaveModel> saveGameObservable = PublishSubject.create();
    private PublishSubject<Move> sendMoveObservable = PublishSubject.create();
    private ConnectionListener connectionListener;


    @Inject
    public RxGameAdapter() {
    }

    public void createSinglePlayerGame(Player.PlayerType localPlayer, int difficultyLevel) {
        GameSettings gameSettings = new GameSettings.Builder()
                .gameType(Game.GameType.offlineSinglePlayer)
                .setLocalPlayerType(localPlayer)
                .setBotLevel(difficultyLevel)
                .build();
        game = new Game(gameSettings, this);
        game.startGame();
    }

    public void createMultiPlayerGame() {
        GameSettings gameSettings = new GameSettings.Builder()
                .gameType(Game.GameType.offlineMultiPlayer)
                .build();
        game = new Game(gameSettings, this);
        game.startGame();
    }

    public void createRoom() {
        isOnline = true;
        gameSettings = new GameSettings.Builder()
                .gameType(Game.GameType.onlineMultiPlayer)
                .setLocalPlayerType(Player.PlayerType.White)
                .build();
        gameSessionManger = new GameSessionManger(this, this, gameSettings);
        gameSessionManger.searchOpponent();
    }

    public void joinRoom() {
        isOnline = true;
        gameSettings = new GameSettings.Builder()
                .gameType(Game.GameType.onlineMultiPlayer)
                .setLocalPlayerType(Player.PlayerType.Black)
                .build();
        gameSessionManger = new GameSessionManger(this, this, gameSettings);
        gameSessionManger.searchOpponent();
    }

    public void loadGame(SaveModel saveModel) {
        Log.e("TAG", "loadGame: " + saveModel.lasPlayerTurn);
        this.saveModel = saveModel;
        game = new Game(this, this);
        game.startGame();
    }

    public void makeMove(Move move) {
        if (isOnline)
            gameSessionManger.selectMove(move);
        else
            game.selectMove(move);
    }

    public Observable<List<Move>> getAvailableMoves(Position position) {
        if (isOnline)
            return Observable.just(gameSessionManger.getCurrentPieceMoves(position));
        else
            return Observable.just(game.getPieceMoves(position));
    }

    // online
    public DisposableObserver<Boolean> onGameCreatedObserver() {
        return new DisposableObserver<Boolean>() {
            @Override
            public void onNext(Boolean roomId) {
//                connectionListener.onGameMatched();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    public DisposableObserver<Move> onMoveSelectedObserver() {
        return new DisposableObserver<Move>() {
            @Override
            public void onNext(Move move) {
                connectionListener.onMoveSelected(move);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }


    @Override
    public void displayMove(Position oldPosition, Position newPosition, Move move) {
        displayMoveEntityObservable.onNext(new DisplayMoveEntity(oldPosition, newPosition, move));
    }

    @Override
    public void removePiece(Position position) {
        removePieceObservable.onNext(position);
    }

    @Override
    public void upgradePiece(Position position) {
        upgradePieceObservable.onNext(position);
    }

    @Override
    public void putPieceInPosition(PieceType pieceType, Position position) {
        putPieceInPositionObservable.onNext(new PutPieceInPositionEntity(pieceType, position));
    }

    @Override
    public void startPlayerTurn(Player.PlayerType playerType) {
        Log.e("TAG", "startPlayerTurn: " );
        startPlayerTurnObservable.onNext(playerType);
    }

    @Override
    public void displayPlayerWin() {
        displayWinObservable.onNext(true);
    }

    @Override
    public void displayDraw() {
        displayDrawObservable.onNext(true);
    }

    @Override
    public void displayMovablePieces(List<Piece> movablePieces) {
        displayMovablePieceObservable.onNext(movablePieces);
    }

    @Override
    public void searchOpponent(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    @Override
    public void sendMove(Move move) {
        sendMoveObservable.onNext(move);
    }

    @Override
    public void save(SaveModel saveModel) {
        this.saveModel = saveModel;
    }

    @Override
    public SaveModel load() {
        return this.saveModel;
    }

    public PublishSubject<DisplayMoveEntity> getDisplayMoveEntityObservable() {
        return displayMoveEntityObservable;
    }

    public PublishSubject<Position> getRemovePieceObservable() {
        return removePieceObservable;
    }

    public PublishSubject<Position> getUpgradePieceObservable() {
        return upgradePieceObservable;
    }

    public PublishSubject<PutPieceInPositionEntity> getPutPieceInPositionObservable() {
        return putPieceInPositionObservable;
    }

    public PublishSubject<Player.PlayerType> getStartPlayerTurnObservable() {
        return startPlayerTurnObservable;
    }

    public PublishSubject<Boolean> getDisplayWinObservable() {
        return displayWinObservable;
    }

    public PublishSubject<Boolean> getDisplayDrawObservable() {
        return displayDrawObservable;
    }

    public PublishSubject<List<Piece>> getDisplayMovablePieceObservable() {
        return displayMovablePieceObservable;
    }

    public Observable<SaveModel> getSaveGameObservable() {
        game.save(this);
        return Observable.just(saveModel);
    }

    public PublishSubject<Move> getSendMoveObservable() {
        return sendMoveObservable;
    }


    public Observable<Boolean> startGame() {
        Log.e("TAG", "startGame: ");
        connectionListener.onGameMatched();
        return Observable.just(true);
    }
}
