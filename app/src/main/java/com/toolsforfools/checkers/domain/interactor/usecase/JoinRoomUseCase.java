package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class JoinRoomUseCase extends ParamsUseCase<Boolean, JoinRoomUseCase.JoinRoomParams> {

    GameRepository gameRepository;

    @Inject
    public JoinRoomUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Boolean> buildObservable(JoinRoomParams joinRoomParams) {
        gameRepository.joinRoom(joinRoomParams.roomId);
        return Observable.just(true);
    }

    public static class JoinRoomParams{

        String roomId;

        private JoinRoomParams(String roomId) {
            this.roomId = roomId;
        }

        public static JoinRoomParams create(String roomId){
            return new JoinRoomParams(roomId);
        }
    }
}
