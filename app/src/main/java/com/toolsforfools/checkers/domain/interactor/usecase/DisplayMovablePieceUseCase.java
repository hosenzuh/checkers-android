package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DisplayMovablePieceUseCase extends UseCase<List<Piece>> {

    private GameRepository gameRepository;

    @Inject
    public DisplayMovablePieceUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<List<Piece>> buildObservable() {
        return gameRepository.displayMovablePiece();
    }

}
