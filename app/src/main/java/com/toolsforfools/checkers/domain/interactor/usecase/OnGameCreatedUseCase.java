package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.UseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class OnGameCreatedUseCase extends UseCase<Boolean> {

    GameRepository gameRepository;

    @Inject
    public OnGameCreatedUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<Boolean> buildObservable() {
        return gameRepository.onGameCreated();
    }

}
