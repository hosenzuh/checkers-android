package com.toolsforfools.checkers.domain.checkersEngine.move.moveModel;

import com.toolsforfools.checkers.domain.checkersEngine.board.Board;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.util.List;

public class FakeMoveHolder {
    public Board fakeBoard;
    public Player.PlayerType playerTurnType;
    public List<Move> playerAvailableMoves;
    public boolean isDraw;
    public boolean isGameEnded;
}
