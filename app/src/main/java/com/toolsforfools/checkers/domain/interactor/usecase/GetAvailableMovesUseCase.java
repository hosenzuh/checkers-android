package com.toolsforfools.checkers.domain.interactor.usecase;

import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.domain.interactor.usecase.base.ParamsUseCase;
import com.toolsforfools.checkers.domain.repository.GameRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetAvailableMovesUseCase extends ParamsUseCase<List<Move>, GetAvailableMovesUseCase.GetAvailableMovesParams> {

    private GameRepository gameRepository;

    @Inject
    public GetAvailableMovesUseCase(UIExecutor uiExecutor, ThreadExecutor threadExecutor, GameRepository gameRepository) {
        super(uiExecutor, threadExecutor);
        this.gameRepository = gameRepository;
    }

    @Override
    protected Observable<List<Move>> buildObservable(GetAvailableMovesParams getAvailableMovesParams) {
        return gameRepository.getAvailableMoves(getAvailableMovesParams.position);
    }


    public static class GetAvailableMovesParams {
        private Position position;

        public GetAvailableMovesParams(Position position) {
            this.position = position;
        }

        public static GetAvailableMovesParams create(Position position) {
            return new GetAvailableMovesParams(position);
        }
    }
}
