package com.toolsforfools.checkers.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.presention.callback.OnAnimationFinished;

import androidx.annotation.AnimatorRes;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.SpringAnimation;
import androidx.dynamicanimation.animation.SpringForce;

public class AnimationUtil {

    private static void animate(View view, @AnimatorRes int animatorId, Context context, OnAnimationFinished onAnimationFinished) {
        @SuppressLint("ResourceType") Animation animation = AnimationUtils.loadAnimation(context, animatorId);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (onAnimationFinished != null)
                    onAnimationFinished.onFinished();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animation);
    }

    @SuppressLint("ResourceType")
    public static void slideUp(View view, Context context, OnAnimationFinished onAnimationFinished) {
        animate(view, R.anim.slide_up, context, onAnimationFinished);
    }

    @SuppressLint("ResourceType")
    public static void slideUp(View view, Context context) {
        animate(view, R.anim.slide_up, context, null);
    }

    @SuppressLint("ResourceType")
    public static void dropDown(View view, Context context, OnAnimationFinished onAnimationFinished) {
        animate(view, R.anim.drop_down, context, onAnimationFinished);
    }

    @SuppressLint("ResourceType")
    public static void dropDown(View view, Context context) {
        animate(view, R.anim.drop_down, context, null);
    }

    public static void dropDownSpring(View view, OnAnimationFinished onAnimationFinished) {
        view.setTranslationY(-100);
        SpringAnimation springAnimator = new SpringAnimation(view, DynamicAnimation.TRANSLATION_Y);

        SpringForce spring = new SpringForce()
                .setFinalPosition(0f)
                .setStiffness(SpringForce.STIFFNESS_VERY_LOW)
                .setDampingRatio(SpringForce.DAMPING_RATIO_HIGH_BOUNCY);
        springAnimator.setSpring(spring)
                .addEndListener(new DynamicAnimation.OnAnimationEndListener() {
                    @Override
                    public void onAnimationEnd(DynamicAnimation animation, boolean canceled, float value, float velocity) {
                        if (onAnimationFinished != null)
                            onAnimationFinished.onFinished();
                    }
                });

        springAnimator.start();
    }

    public static void dropDownSpring(View view){
        dropDownSpring(view,null);
    }


    public static void slideUpSpring(View view, OnAnimationFinished onAnimationFinished) {
        SpringAnimation springAnimator = new SpringAnimation(view, DynamicAnimation.TRANSLATION_Y);

        SpringForce spring = new SpringForce()
                .setFinalPosition(-200f)
                .setStiffness(SpringForce.STIFFNESS_LOW)
                .setDampingRatio(SpringForce.DAMPING_RATIO_MEDIUM_BOUNCY);
        springAnimator.setSpring(spring)
                .addEndListener(new DynamicAnimation.OnAnimationEndListener() {
                    @Override
                    public void onAnimationEnd(DynamicAnimation animation, boolean canceled, float value, float velocity) {
                        if (onAnimationFinished != null)
                            onAnimationFinished.onFinished();
                    }
                });

        springAnimator.start();
    }

    public static void slideUpSpring(View view){
        slideUpSpring(view,null);
    }

}
