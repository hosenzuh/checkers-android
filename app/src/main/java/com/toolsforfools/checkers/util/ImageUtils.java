package com.toolsforfools.checkers.util;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import androidx.annotation.Nullable;

public class ImageUtils {

    private static String PICTURES_FOLDER = "images";

    private ImageUtils() {
    }

    public interface OnLoadBitmap {
        void onSuccess(@Nullable Bitmap bitmap);
    }

    public static void loadImage(ImageView imageView, String url, Drawable placeHolder, boolean cache) {
        if (url == null || url.isEmpty()) {
            if (placeHolder == null)
                imageView.setImageDrawable(null);
            return;
        }
        if (imageView == null)
            throw new RuntimeException("Null ImageView");

        RequestCreator creator = Picasso.get().load(url);
        if (placeHolder != null)
            creator = creator.fit().centerCrop().placeholder(placeHolder);
        if (!cache)
            creator = creator.memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE);
        creator.fit().into(imageView);
    }

    public static void loadPictureAndCache(ImageView imageView, String url, Drawable placeHolder) {
        loadImage(imageView, url, placeHolder, true);
    }

}
