package com.toolsforfools.checkers.util;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

public class TextUtils {


    public static void copyToClipboard(Context context,String text){
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label",text);
        clipboardManager.setPrimaryClip(clipData);
    }
}
