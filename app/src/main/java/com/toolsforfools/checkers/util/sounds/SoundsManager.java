package com.toolsforfools.checkers.util.sounds;

import android.content.Context;
import android.media.MediaPlayer;

import com.toolsforfools.checkers.R;

public class SoundsManager {

    private SoundsManager() {
    }

    private static MediaPlayer mediaPlayer;
    private static MediaPlayer backgroundMusicPlayer;
    private static int currentPosition;

    public static void playMoveSound(Context context) {
        mediaPlayer = MediaPlayer.create(context, R.raw.sound_move);
        mediaPlayer.start();
    }

    public static void playEatSound(Context context) {
        mediaPlayer = MediaPlayer.create(context, R.raw.sound_eat);
        mediaPlayer.start();
        mediaPlayer.setOnSeekCompleteListener(mediaPlayer1 -> mediaPlayer1.release());
    }

    public static void playKingSound(Context context) {
        mediaPlayer = MediaPlayer.create(context, R.raw.sound_king);
        mediaPlayer.start();
        mediaPlayer.setOnSeekCompleteListener(mediaPlayer1 -> mediaPlayer1.release());
    }

    public static void playBackgroundMusic(Context context) {
        if (backgroundMusicPlayer == null) {
            backgroundMusicPlayer = MediaPlayer.create(context, R.raw.music_background);
            backgroundMusicPlayer.setLooping(true);
        }else {
            backgroundMusicPlayer.seekTo(currentPosition);
        }
        if(backgroundMusicPlayer.isPlaying())
            return;

        backgroundMusicPlayer.start();
    }

    public static void pauseBackgroundMusic(Context context) {
        backgroundMusicPlayer.pause();
        currentPosition = backgroundMusicPlayer.getCurrentPosition();
    }

}
