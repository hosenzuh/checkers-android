package com.toolsforfools.checkers.util.constants;

final public class SharedPrefKey {

    public static final String TOKEN = "token";
    public static final String APP_SHARED_KEY = "app shared";
    public static final String SAVE_GAME_KEY = "save game";
    public static final String USER_STATUS = "userStatus";
    public static final String USER = "user";

    private SharedPrefKey() {
    }
}
