package com.toolsforfools.checkers.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class NavigationUtils {

    private NavigationUtils() {
    }

    public static void goToActivity(Context context,
                                    Intent intent,
                                    boolean isFinished,
                                    Bundle bundle) {
        if (bundle != null)
            intent = intent.putExtras(bundle);
        context.startActivity(intent);
        if (isFinished)
            ((Activity) context).finish();
    }

    public static void goToActivity(Context context,
                                    Class<?> target,
                                    boolean isFinished,
                                    Bundle bundle) {
        goToActivity(context, new Intent(context, target), isFinished, bundle);
    }

    public static void goToActivity(Context context,
                                    Intent intent) {
        goToActivity(context, intent, false, null);
    }

    public static void goToActivity(Context context,
                                    Class<?> target) {
        goToActivity(context, target, false, null);
    }


    public static void goToActivity(Context context,
                                    Intent intent,
                                    Bundle bundle) {
        goToActivity(context, intent, false, bundle);
    }

    public static void goToActivity(Context context,
                                    Intent intent,
                                    boolean isFinished) {
        goToActivity(context, intent, isFinished, null);
    }

    public static void goToActivity(Context context,
                                    Class<?> target,
                                    Bundle bundle) {
        goToActivity(context, new Intent(context, target), false, bundle);
    }

    public static void goToActivity(Context context,
                                    Class<?> target,
                                    boolean isFinished) {
        goToActivity(context, new Intent(context, target), isFinished, null);
    }

    // Fragments

    public static void addFragment(FragmentManager fragmentManager
            , int containerId
            , Fragment fragment
            , boolean addToBackStack
            , boolean isReplace) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction = isReplace ? transaction.replace(containerId, fragment) : transaction.add(containerId, fragment);
        if (addToBackStack)
            transaction = transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public static void addFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, boolean addToBackStack) {
        addFragment(fragmentManager, containerId, fragment, addToBackStack, false);
    }

    public static void replaceFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, boolean addToBackStack) {
        addFragment(fragmentManager, containerId, fragment, addToBackStack, true);
    }

    public static void addFragment(FragmentManager fragmentManager, int containerId, Fragment fragment) {
        addFragment(fragmentManager, containerId, fragment, false);
    }

    public static void replaceFragment(FragmentManager fragmentManager, int containerId, Fragment fragment) {
        replaceFragment(fragmentManager, containerId, fragment, false);
    }

}
