package com.toolsforfools.checkers.util.sounds;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class BackgroundMusicService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
    }
    @SuppressLint("WrongConstant")
    public int onStartCommand(Intent intent, int flags, int startId) {
        SoundsManager.playBackgroundMusic(getApplicationContext());
        return 1;
    }

    @Override
    public void onDestroy() {
        SoundsManager.pauseBackgroundMusic(getApplicationContext());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
