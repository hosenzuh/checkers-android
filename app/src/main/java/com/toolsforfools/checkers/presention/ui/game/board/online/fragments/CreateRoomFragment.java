package com.toolsforfools.checkers.presention.ui.game.board.online.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.FragmentCreateRoomBinding;
import com.toolsforfools.checkers.databinding.FragmentRoomControlBinding;
import com.toolsforfools.checkers.presention.ui.base.view.fragment.MVVMFragment;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardViewModel;
import com.toolsforfools.checkers.util.NavigationUtils;
import com.toolsforfools.checkers.util.TextUtils;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class CreateRoomFragment extends MVVMFragment<OnlineBoardViewModel, FragmentCreateRoomBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected OnlineBoardViewModel provideViewModel() {
        return new ViewModelProvider(requireActivity(),factory).get(OnlineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_create_room;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        viewModel.createRoom();
        mFragmentBinding.exitButton.setOnClickListener(view1 -> {
            getActivity().getSupportFragmentManager().popBackStack();
            viewModel.roomId.setValue("");
        });
        mFragmentBinding.codeTv.setOnClickListener(view1 -> {
            TextUtils.copyToClipboard(activity,viewModel.roomId.getValue());
            showMessage(R.string.copied_to_clipboard);
        });
    }
}
