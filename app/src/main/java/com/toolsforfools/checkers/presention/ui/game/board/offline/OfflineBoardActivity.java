package com.toolsforfools.checkers.presention.ui.game.board.offline;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.toolsforfools.checkers.App;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.databinding.ActivityOfflineBoardBinding;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.presention.callback.OnCellClickListener;
import com.toolsforfools.checkers.presention.model.CellType;
import com.toolsforfools.checkers.presention.model.CellTypeModel;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.customview.Cell;
import com.toolsforfools.checkers.presention.ui.game.board.dialog.SaveGameDialog;
import com.toolsforfools.checkers.presention.ui.game.board.dialog.WinningDialog;
import com.toolsforfools.checkers.util.sounds.SoundsManager;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class OfflineBoardActivity extends MVVMActivity<OfflineBoardViewModel, ActivityOfflineBoardBinding> {

    private static final String IS_LOADING = "isLoading";
    @Inject
    ViewModelProvider.Factory factory;
    @Inject
    ISharedPref sharedPref;

    private static final String TAG = "ACTIVITY";
    private static final String IS_SINGLE = "isSingle";
    private static final String DIFFICULTY_LEVEL = "difficultyLevel";
    private static final String PLAYER_TYPE = "playerType";
    private static final String SAVE_MODEL = "saveModel";

    public static final int CELLS_NUMBER = 9;
    private Cell[][] cells = new Cell[9][9];
    //TODO : REMOVE THIS AFTER PRESENT
    private Position lastPiece = new Position(1,1);
    private Position lastCell = new Position(1,1);

    private boolean isSingle;
    private boolean isLoading;
    private int difficultyLevel;
    private SaveModel saveModel;
    private Player.PlayerType playerType = Player.PlayerType.White;

    public static Intent getOfflineBoardForLoadGame(Context context, SaveModel saveModel) {
        Intent intent = new Intent(context, OfflineBoardActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_LOADING, true);
        bundle.putSerializable(SAVE_MODEL, saveModel);
        intent.putExtras(bundle);
        return intent;
    }

    public static Intent getOfflineBoardForSingle(Context context, int difficultyLevel, Player.PlayerType playerType) {
        Intent intent = new Intent(context, OfflineBoardActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_SINGLE, true);
        bundle.putInt(DIFFICULTY_LEVEL, difficultyLevel);
        bundle.putSerializable(PLAYER_TYPE, playerType);
        intent.putExtras(bundle);
        return intent;
    }


    public static Intent getOfflineBoardForMulti(Context context) {
        Intent intent = new Intent(context, OfflineBoardActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_SINGLE, false);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onFetchParams() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            isSingle = bundle.getBoolean(IS_SINGLE);
            isLoading = bundle.getBoolean(IS_LOADING, false);
            if (isSingle) {
                difficultyLevel = bundle.getInt(DIFFICULTY_LEVEL);
                playerType = (Player.PlayerType) bundle.getSerializable(PLAYER_TYPE);
            } else if (isLoading) {
                saveModel = (SaveModel) bundle.getSerializable(SAVE_MODEL);
            }
        }
    }

    @Override
    protected OfflineBoardViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(OfflineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_offline_board;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.getInstance().pauseMusic();
        createBoard();
        setupObservers();
        initBoard();
        viewModel.createGame(isSingle, difficultyLevel, playerType, isLoading, saveModel);
        mActivityBinding.saveButton.setOnClickListener(view -> {
            new SaveGameDialog().setOnClickListener(view1 -> viewModel.saveGame()).show(getSupportFragmentManager(), "");
        });
    }

    private void initBoard() {
        for (int i = 1; i < CELLS_NUMBER; i++) {
            for (int j = 1; j < CELLS_NUMBER; j++) {
                cells[i][j].removeAvailableMove();
                cells[i][j].setView(PieceType.NULL, CellType.NONE);
            }
        }
    }


    private void setupObservers() {
        viewModel.cellTypeMutable.observe(this, cellTypeModel -> {
            CellType cellType = cellTypeModel.getCellType();
            int row = cellTypeModel.getX();
            int column = cellTypeModel.getY();
            if (cellType.equals(CellType.AVAILABLE_MOVE))
                cells[row][column].setAvailableMove();
            else if (cellType.equals(CellType.NONE))
                cells[row][column].removeAvailableMove();
            else { //there is piece
                PieceType pieceType = viewModel.pieceTypes[row][column];
                Log.e(TAG, "setupObservers: " + pieceType);
                cells[row][column].setView(pieceType, CellType.PIECE);
            }
        });

        viewModel.pieceTypeMutable.observe(this, pieceTypeModel -> {
            PieceType pieceType = pieceTypeModel.getPieceType();
            int row = pieceTypeModel.getX();
            int column = pieceTypeModel.getY();
            if (pieceType.isKing())
                SoundsManager.playKingSound(this);
            cells[row][column].setView(pieceType, viewModel.cellTypes[row][column]);
        });

        // move type observing (for sound)
        viewModel.moveMutableLiveData.observe(this, move -> {

            Log.e(TAG, "setupObservers: " + (move instanceof EatMove));
            if (move instanceof EatMove) {
                SoundsManager.playEatSound(this);
            } else {
                SoundsManager.playMoveSound(this);
            }
        });

        // movable pieces observing
        viewModel.movablePieces.observe(this, movablePieces -> {
            Position position;
            for (int k = 0; k < movablePieces.getPieces().size(); k++) {
                position = movablePieces.getPieces().get(k).getPosition();
                if (movablePieces.isRemove())
                    cells[position.getX()][position.getY()].removePieceAvailable();
                else
                    cells[position.getX()][position.getY()].setPieceAvailable();
            }
        });

        // Last clicked Piece observing
        final boolean[] isInit = {false};
        viewModel.lastClickedPiece.observe(this, piece -> {
            if (piece.getType().equals(PieceType.NULL) && isInit[0])
                cells[piece.getPosition().getX()][piece.getPosition().getY()].removePieceClicked();
            else if (isInit[0])
                cells[piece.getPosition().getX()][piece.getPosition().getY()].setPieceClicked();
            isInit[0] = true;
        });

        // Winning observing
        viewModel.isWinner.observe(this, playerType -> {
            new WinningDialog().setWinner(playerType.name()).show(getSupportFragmentManager(), "win dialog");
        });

        // change turn observing
        viewModel.playerTurn.observe(this, playerType -> {
            if (playerType.equals(Player.PlayerType.Black)) {
                mActivityBinding.blackPlayerBoard.setText("Your turn");
                mActivityBinding.whitePlayerBoard.setText("Waiting black player");
            } else {
                mActivityBinding.whitePlayerBoard.setText("Your turn");
                mActivityBinding.blackPlayerBoard.setText("Waiting white player");
            }
        });

        // last cell selected observing
        viewModel.lastPieceMovedMutable.observe(this,lastPieceMoved->{
            cells[lastPiece.getX()][lastPiece.getY()].removePieceLastMoved();
            lastPiece = lastPieceMoved;
            cells[lastPieceMoved.getX()][lastPieceMoved.getY()].setPieceLastMoved();
        });

        viewModel.lastSelectedCellMutable.observe(this,lastSelectedCell->{
            Log.e(TAG, "setupObservers: "+lastSelectedCell.getX()+ " " +lastSelectedCell.getY() );
            cells[lastCell.getX()][lastCell.getY()].removeEmptyCellLastSelected();
            lastCell = lastSelectedCell;
            cells[lastSelectedCell.getX()][lastSelectedCell.getY()].setEmptyCellLastSelected();
        });

        Log.e(TAG, "setupObservers: SETUPED");
    }

    private void createBoard() {
        LinearLayout row;

        for (int i = 1; i < CELLS_NUMBER; i++) {

            row = new LinearLayout(this);

            for (int j = 1; j < CELLS_NUMBER; j++) {
                row.addView(createCell(i, j));
            }
            mActivityBinding.boardLayout.addView(row);
        }

        if (playerType.equals(Player.PlayerType.White)) {
            flipBoard();
//            List<View> viewList = new ArrayList<>();
//            for (int i = 0; i < mActivityBinding.activityLayout.getChildCount(); i++) {
//                viewList.add(mActivityBinding.activityLayout.getChildAt(i));
//            }
//            mActivityBinding.activityLayout.removeAllViews();
//            for (int i = 2; i >= 0; i--) {
//                mActivityBinding.activityLayout.addView(viewList.get(i));
//            }
//            for (int i = 3; i < viewList.size(); i++) {
//                mActivityBinding.activityLayout.addView(viewList.get(i));
//            }
        }

    }

    boolean flipped = true;

    private Cell createCell(int row, int column) {
        OnCellClickListener onCellClickListener = (pieceRow, pieceColumn, pieceType, cellType) -> {
            if (cellType.equals(CellType.AVAILABLE_MOVE)) {
                viewModel.makeMove(pieceRow, pieceColumn);
//                flipBoard();
            } else if (cellType.equals(CellType.NONE)) {
                viewModel.clearAvailableMoves();
            } else {
                viewModel.checkMoves(pieceRow, pieceColumn);
            }
        };
        Cell cell = new Cell(this, row, column, isBeige(row, column), onCellClickListener);
        cells[row][column] = cell;
        return cell;
    }

    private void flipBoard() {
        flipped = !flipped;
        if (!flipped) {
            mActivityBinding.playersLayout.setRotation(180);
            mActivityBinding.blackPlayerLayout.setRotation(180);
            mActivityBinding.whitePlayerLayout.setRotation(180);
        } else
            mActivityBinding.boardLayout.setRotation(0);
    }

    private boolean isBeige(int row, int column) {
        return (row % 2 != 0 && column % 2 != 0) || (row % 2 == 0 && column % 2 == 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        App.getInstance().resumeMusic();
    }
}
