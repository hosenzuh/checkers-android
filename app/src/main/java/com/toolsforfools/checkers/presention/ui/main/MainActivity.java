package com.toolsforfools.checkers.presention.ui.main;

import android.os.Bundle;

import com.toolsforfools.checkers.BR;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.databinding.ActivityMainBinding;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.presention.callback.OnAnimationFinished;
import com.toolsforfools.checkers.presention.callback.OnItemClickListener;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.board.dialog.LoadGameDialog;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity;
import com.toolsforfools.checkers.presention.ui.game.startgame.StartGameActivity;
import com.toolsforfools.checkers.util.AnimationUtil;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

import androidx.lifecycle.ViewModelProvider;

public class MainActivity extends MVVMActivity<MainViewModel, ActivityMainBinding> {

    @Inject
    ViewModelProvider.Factory factory;
    @Inject
    ISharedPref sharedPref;

    private LoadGameDialog loadGameDialog;


    @Override
    protected MainViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(MainViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerAllForSlideDown();
        slideDown();
        registerAllForSlideUp();
        mActivityBinding.startGameButton.setOnClickListener(
                view -> {
                    slidUpWithCheck(onStartGameAnimationFinished);
                }
        );
        AnimationUtil.dropDown(mActivityBinding.optionsLayout, this);
        mActivityBinding.loadButton.setOnClickListener(view -> {
            loadGameDialog = new LoadGameDialog().setSharedPref(sharedPref).setOnItemClickListener(onItemClickListener);
            loadGameDialog.show(getSupportFragmentManager(),"");
        });
    }

    private OnItemClickListener<SaveModel> onItemClickListener = (((item, position) -> {
        NavigationUtils.goToActivity(this, OfflineBoardActivity.getOfflineBoardForLoadGame(this,item));
        loadGameDialog.dismiss();
    }));

    OnAnimationFinished onStartGameAnimationFinished = () -> NavigationUtils.goToActivity(this, StartGameActivity.class);
    OnAnimationFinished onSettingsAnimationFinished = () -> NavigationUtils.goToActivity(this, StartGameActivity.class);

    @Override
    protected void onResume() {
        super.onResume();
        slideDown();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
