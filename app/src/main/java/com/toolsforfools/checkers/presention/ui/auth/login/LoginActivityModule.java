package com.toolsforfools.checkers.presention.ui.auth.login;

import androidx.lifecycle.ViewModel;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;


@Module
public abstract class LoginActivityModule {
    @ViewModelKey(LoginViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindLoginViewModel(LoginViewModel viewModel);
}
