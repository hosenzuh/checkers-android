package com.toolsforfools.checkers.presention.ui.game.board.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.presention.ui.base.view.fragment.BaseFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.DialogFragment;

public class SaveGameDialog extends DialogFragment {

    View.OnClickListener onClickListener;

    public SaveGameDialog setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_save_game,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        AppCompatButton yesButton = view.findViewById(R.id.yes_button);
        yesButton.setOnClickListener(view1 -> {
            onClickListener.onClick(getView());
            dismiss();
        });
        AppCompatButton noButton = view.findViewById(R.id.no_button);
        noButton.setOnClickListener(view1 -> {
            dismiss();
        });
    }
}
