package com.toolsforfools.checkers.presention.ui.base;

import com.toolsforfools.checkers.domain.interactor.usecase.base.BaseUseCase;
import com.toolsforfools.checkers.presention.exception.ExceptionFactory;
import com.toolsforfools.checkers.util.lifecycle.CustomMutableLiveData;
import com.toolsforfools.checkers.util.lifecycle.Event;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel extends ViewModel {

    public CustomMutableLiveData<Boolean> isLoading = new CustomMutableLiveData<>(false);

    private List<BaseUseCase> useCases = new ArrayList<>();

    private MutableLiveData<Event<String>> _toastMessage = new MutableLiveData<>();

    public LiveData<Event<String>> toastMessage = _toastMessage;

    private MutableLiveData<Event<Integer>> _toastMessageResource = new MutableLiveData<>();
    public LiveData<Event<Integer>> toastMessageResource = _toastMessageResource;

    private MutableLiveData<Event<Boolean>> _hideKeyboard = new MutableLiveData<>();
    public LiveData<Event<Boolean>> hideKeyboard = _hideKeyboard;


    protected void showMessage(String message) {
        _toastMessage.setValue(new Event<>(message));
    }

    protected void showMessage(int stringId) {
        _toastMessageResource.setValue(new Event<>(stringId));
    }

    protected void showMessage(Throwable error) {
        if (error.getMessage() != null)
            showMessage(error.getMessage());
        else
            showMessage(ExceptionFactory.getString(error));
    }

    protected void hideKeyboard() {
        _hideKeyboard.postValue(new Event<>(true));
    }

    protected void startLoading() {
        isLoading.postValue(true);
    }

    protected void stopLoading() {
        isLoading.postValue(false);
    }

    protected void registerUseCase(BaseUseCase... useCases) {
        this.useCases.addAll(Arrays.asList(useCases));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        for (BaseUseCase useCase : this.useCases) {
            useCase.dispose();
        }
    }
}
