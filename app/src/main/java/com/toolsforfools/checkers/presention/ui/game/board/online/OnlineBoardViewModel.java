package com.toolsforfools.checkers.presention.ui.game.board.online;

import android.util.Log;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.entity.DisplayMoveEntity;
import com.toolsforfools.checkers.domain.entity.PutPieceInPositionEntity;
import com.toolsforfools.checkers.domain.interactor.BaseObserver;
import com.toolsforfools.checkers.domain.interactor.usecase.CreateRoomUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayDrawUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayMovablePieceUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayMoveUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayWinUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.GetAvailableMovesUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.JoinRoomUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.MakeMoveUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.OnGameCreatedUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.OnMoveSelectedUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.PutPieceInPositionUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.RemovePieceUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.SendMoveUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.StartGameUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.StartTurnUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.UpgradePieceUseCase;
import com.toolsforfools.checkers.presention.model.CellType;
import com.toolsforfools.checkers.presention.model.CellTypeModel;
import com.toolsforfools.checkers.presention.model.MovablePieces;
import com.toolsforfools.checkers.presention.model.PieceTypeModel;
import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

import static com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity.CELLS_NUMBER;

public class OnlineBoardViewModel extends BaseViewModel {

    public MutableLiveData<CellTypeModel> cellTypeMutable = new MutableLiveData(new CellTypeModel(CellType.NONE, 1, 1));
    public MutableLiveData<PieceTypeModel> pieceTypeMutable = new MutableLiveData(new PieceTypeModel(PieceType.NULL, 1, 1));
    public CellType[][] cellTypes = new CellType[20][20];
    public PieceType[][] pieceTypes = new PieceType[20][20];

    public MutableLiveData<Player.PlayerType> playerTurn = new MutableLiveData<>(Player.PlayerType.White);
    public MutableLiveData<Player.PlayerType> isWinner = new MutableLiveData<>();
    public MutableLiveData<Boolean> flipBoard = new MutableLiveData<>(false);
    public MutableLiveData<Move> moveMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Boolean> gameStarted = new MutableLiveData<>(false);
    private Player.PlayerType localPlayer;
    public MutableLiveData<Piece> lastClickedPiece = new MutableLiveData<>(new Piece(new Position(1, 1), PieceType.NULL));
    public MutableLiveData<MovablePieces> movablePieces = new MutableLiveData<>(new MovablePieces(Collections.emptyList(), true));
    public MutableLiveData<Boolean> isEatMoveDisplayed = new MutableLiveData<>();
    String TAG = "VIEWMODEL";
    private List<Move> availableMoves;
    public MutableLiveData<String> roomId = new MutableLiveData<>("");

    // UseCases
    private MakeMoveUseCase makeMoveUseCase;
    private GetAvailableMovesUseCase getAvailableMovesUseCase;
    private PutPieceInPositionUseCase putPieceInPositionUseCase;
    private DisplayDrawUseCase displayDrawUseCase;
    private DisplayMovablePieceUseCase displayMovablePieceUseCase;
    private DisplayMoveUseCase displayMoveUseCase;
    private DisplayWinUseCase displayWinUseCase;
    private StartTurnUseCase startTurnUseCase;
    private RemovePieceUseCase removePieceUseCase;
    private UpgradePieceUseCase upgradePieceUseCase;
    private CreateRoomUseCase createRoomUseCase;
    private JoinRoomUseCase joinRoomUseCase;
    private OnGameCreatedUseCase onGameCreatedUseCase;
    private SendMoveUseCase sendMoveUseCase;
    private OnMoveSelectedUseCase onMoveSelectedUseCase;
    private StartGameUseCase startGameUseCase;

    @Inject
    public OnlineBoardViewModel(MakeMoveUseCase makeMoveUseCase, GetAvailableMovesUseCase getAvailableMovesUseCase, PutPieceInPositionUseCase putPieceInPositionUseCase, DisplayDrawUseCase displayDrawUseCase, DisplayMovablePieceUseCase displayMovablePieceUseCase, DisplayMoveUseCase displayMoveUseCase, DisplayWinUseCase displayWinUseCase, StartTurnUseCase startTurnUseCase, RemovePieceUseCase removePieceUseCase, UpgradePieceUseCase upgradePieceUseCase, CreateRoomUseCase createRoomUseCase, JoinRoomUseCase joinRoomUseCase, OnGameCreatedUseCase onGameCreatedUseCase, SendMoveUseCase sendMoveUseCase, OnMoveSelectedUseCase onMoveSelectedUseCase,StartGameUseCase startGameUseCase) {
        this.makeMoveUseCase = makeMoveUseCase;
        this.getAvailableMovesUseCase = getAvailableMovesUseCase;
        this.putPieceInPositionUseCase = putPieceInPositionUseCase;
        this.displayDrawUseCase = displayDrawUseCase;
        this.displayMovablePieceUseCase = displayMovablePieceUseCase;
        this.displayMoveUseCase = displayMoveUseCase;
        this.displayWinUseCase = displayWinUseCase;
        this.startTurnUseCase = startTurnUseCase;
        this.removePieceUseCase = removePieceUseCase;
        this.upgradePieceUseCase = upgradePieceUseCase;
        this.createRoomUseCase = createRoomUseCase;
        this.joinRoomUseCase = joinRoomUseCase;
        this.onGameCreatedUseCase = onGameCreatedUseCase;
        this.sendMoveUseCase = sendMoveUseCase;
        this.onMoveSelectedUseCase = onMoveSelectedUseCase;
        this.startGameUseCase = startGameUseCase;
        registerUseCase(makeMoveUseCase,
                getAvailableMovesUseCase,
                putPieceInPositionUseCase,
                displayDrawUseCase,
                displayMovablePieceUseCase,
                displayMoveUseCase,
                displayWinUseCase,
                startTurnUseCase,
                removePieceUseCase,
                upgradePieceUseCase,
                createRoomUseCase,
                joinRoomUseCase,
                onGameCreatedUseCase,
                sendMoveUseCase,
                onMoveSelectedUseCase,
                startGameUseCase);
        setupObservers();
        initBoardState();
    }

    private void initBoardState() {
        for (int row = 1; row < CELLS_NUMBER; row++) {
            for (int column = 1; column < CELLS_NUMBER; column++) {
                Log.e(TAG, "initBoardState: " + row + " " + column);
                cellTypes[row][column] = CellType.NONE;
                pieceTypes[row][column] = PieceType.NULL;
            }
        }
    }

    private void setupObservers() {
        putPieceInPositionUseCase.execute(new PutPieceInPositionObserver());
        displayDrawUseCase.execute(new DisplayDrawObserver());
        displayMovablePieceUseCase.execute(new DisplayMovablePieceObserver());
        displayMoveUseCase.execute(new DisplayMoveObserver());
        displayWinUseCase.execute(new DisplayWinObserver());
        startTurnUseCase.execute(new StartTurnObserver());
        removePieceUseCase.execute(new RemovePieceObserver());
        upgradePieceUseCase.execute(new UpgradePieceObserver());
        onGameCreatedUseCase.execute(new OnGameCreatedObserver());
        onMoveSelectedUseCase.execute(new DoNothingObserver());
        sendMoveUseCase.execute(new DoNothingObserver());
    }


    public void createRoom() {
        localPlayer = Player.PlayerType.White;
        Log.e(TAG, "createRoom: "+localPlayer );
        createRoomUseCase.execute(new CreateRoomObserver());
    }

    public void startGame(){
        startGameUseCase.execute(new StartGameObserver());
    }

    public void joinRoom() {
        localPlayer = Player.PlayerType.Black;
        Log.e(TAG, "joinRoom: "+localPlayer );
        joinRoomUseCase.execute(new JoinRoomObserver(),
                JoinRoomUseCase.JoinRoomParams.create(roomId.getValue()));
    }

    public void checkMoves(int row, int column) {
        Log.e(TAG, "checkMoveAvailable: ");
        clearAvailableMoves();
        if (!checkMoveAvailable(row, column))
            return;

        lastClickedPiece.setValue(new Piece(new Position(row, column), PieceType.blackPiece));

        Position piecePosition = new Position(row, column);
        getAvailableMovesUseCase.execute(new GetAvailableMovesObserver(),
                GetAvailableMovesUseCase.GetAvailableMovesParams.create(piecePosition));

    }

    private boolean checkMoveAvailable(int row, int column) {
        Log.e(TAG, "checkMoveAvailable: "+localPlayer+" " +playerTurn.getValue() );
        if (!localPlayer.equals(playerTurn.getValue()))
            return false;

        Player.PlayerType playerType = playerTurn.getValue();
        PieceType pieceType = pieceTypes[row][column];
        Log.e(TAG, "checkMoveAvailable: " + pieceType + "  " + playerType);
        return playerType.equals(Player.PlayerType.Black) ? pieceType.equals(PieceType.blackKing) || pieceType.equals(PieceType.blackPiece)
                : pieceType.equals(PieceType.whiteKing) || pieceType.equals(PieceType.whitePiece);
    }

    public void makeMove(int row, int column) {
        Log.e(TAG, "makeMove: ");

        for (int i = 0; i < availableMoves.size(); i++) {
            if (availableMoves.get(i).getDestination().getX() == row && availableMoves.get(i).getDestination().getY() == column) {
                Move move = availableMoves.get(i);
                clearAvailableMoves();
                makeMoveUseCase.execute(new MakeMoveObserver(),
                        MakeMoveUseCase.MakeMoveParams.create(move));
                break;
            }
        }
    }

    public void clearAvailableMoves() {
        Log.e(TAG, "clearAvailableMoves: ");
        Piece piece = lastClickedPiece.getValue();
        piece.setType(PieceType.NULL);
        lastClickedPiece.setValue(piece);
        for (int i = 1; i < CELLS_NUMBER; i++) {
            for (int j = 1; j < CELLS_NUMBER; j++) {
                if (cellTypes[i][j].equals(CellType.AVAILABLE_MOVE)) {
                    cellTypes[i][j] = CellType.NONE;
                    cellTypeMutable.setValue(new CellTypeModel(CellType.NONE, i, j));
                }
            }
        }
    }

    // UseCase Observers
    private class CreateGameObserver extends BaseObserver<Boolean> {
        @Override
        public void onNext(Boolean created) {
        }
    }

    private class DisplayDrawObserver extends BaseObserver<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {

        }
    }

    private class DisplayWinObserver extends BaseObserver<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            isWinner.setValue(playerTurn.getValue());
        }
    }

    private class DisplayMovablePieceObserver extends BaseObserver<List<Piece>> {
        @Override
        public void onNext(List<Piece> pieces) {
            List<Piece> oldPieces = movablePieces.getValue().getPieces();
            movablePieces.setValue(new MovablePieces(oldPieces, true));
            movablePieces.setValue(new MovablePieces(pieces, false));
        }
    }

    private class DisplayMoveObserver extends BaseObserver<DisplayMoveEntity> {
        @Override
        public void onNext(DisplayMoveEntity displayMoveEntity) {
            Log.e(TAG, "onNext: " + displayMoveEntity.getMove());
            int oldX = displayMoveEntity.getOldPosition().getX();
            int oldY = displayMoveEntity.getOldPosition().getY();
            int newX = displayMoveEntity.getNewPosition().getX();
            int newY = displayMoveEntity.getNewPosition().getY();
            PieceType pieceType = pieceTypes[oldX][oldY];
            cellTypes[oldX][oldY] = CellType.NONE;
            pieceTypes[oldX][oldY] = PieceType.NULL;
            cellTypes[newX][newY] = CellType.PIECE;
            pieceTypes[newX][newY] = pieceType;
            cellTypeMutable.setValue(new CellTypeModel(CellType.NONE, oldX, oldY));
            pieceTypeMutable.setValue(new PieceTypeModel(PieceType.NULL, oldX, oldY));
            cellTypeMutable.setValue(new CellTypeModel(CellType.PIECE, newX, newY));
            pieceTypeMutable.setValue(new PieceTypeModel(pieceType, newX, newY));
            moveMutableLiveData.setValue(displayMoveEntity.getMove());
        }
    }

    private class MakeMoveObserver extends BaseObserver<Boolean> {
        @Override
        public void onNext(Boolean made) {
        }
    }

    private class GetAvailableMovesObserver extends BaseObserver<List<Move>> {
        @Override
        public void onNext(List<Move> moves) {
            Log.e(TAG, "onNext: " + moves.size());
            availableMoves = moves;
            Position position;
            for (int i = 0; i < moves.size(); i++) {
                position = moves.get(i).getDestination();
                cellTypes[position.getX()][position.getY()] = CellType.AVAILABLE_MOVE;
                cellTypeMutable.setValue(new CellTypeModel(CellType.AVAILABLE_MOVE, position.getX(), position.getY()));
            }
        }
    }

    private class PutPieceInPositionObserver extends BaseObserver<PutPieceInPositionEntity> {

        @Override
        public void onNext(PutPieceInPositionEntity putPieceInPositionEntity) {
            Log.e(TAG, "onNext: " + putPieceInPositionEntity.getPieceType() + " " + putPieceInPositionEntity.getPosition().getX() + putPieceInPositionEntity.getPosition().getY());
            Position position = putPieceInPositionEntity.getPosition();
            cellTypes[position.getX()][position.getY()] = CellType.PIECE;
            pieceTypes[position.getX()][position.getY()] = putPieceInPositionEntity.getPieceType();
            cellTypeMutable.setValue(new CellTypeModel(CellType.PIECE, position.getX(), position.getY()));
            pieceTypeMutable.setValue(new PieceTypeModel(putPieceInPositionEntity.getPieceType(), position.getX(), position.getY()));
        }
    }

    private class StartTurnObserver extends BaseObserver<Player.PlayerType> {
        @Override
        public void onNext(Player.PlayerType playerType) {
            playerTurn.setValue(playerType);
        }
    }

    private class RemovePieceObserver extends BaseObserver<Position> {
        @Override
        public void onNext(Position position) {
            cellTypes[position.getX()][position.getY()] = CellType.NONE;
            pieceTypes[position.getX()][position.getY()] = PieceType.NULL;
            cellTypeMutable.setValue(new CellTypeModel(CellType.NONE, position.getX(), position.getY()));
            pieceTypeMutable.setValue(new PieceTypeModel(PieceType.NULL, position.getX(), position.getY()));
        }
    }

    private class UpgradePieceObserver extends BaseObserver<Position> {
        @Override
        public void onNext(Position position) {
            PieceType pieceType1 = pieceTypes[position.getX()][position.getY()];
            if (pieceType1.equals(PieceType.whitePiece)) {
                pieceTypes[position.getX()][position.getY()] = PieceType.whiteKing;
                pieceTypeMutable.setValue(new PieceTypeModel(PieceType.whiteKing, position.getX(), position.getY()));
            } else {
                pieceTypes[position.getX()][position.getY()] = PieceType.blackKing;
                pieceTypeMutable.setValue(new PieceTypeModel(PieceType.blackKing, position.getX(), position.getY()));
            }
        }
    }

    private class OnGameCreatedObserver extends BaseObserver<Boolean> {
        @Override
        public void onNext(Boolean aBoolean) {
            Log.e(TAG, "onNext: " );
            gameStarted.setValue(true);
        }
    }

    private class CreateRoomObserver extends BaseObserver<String> {
        @Override
        public void onNext(String s) {
            flipBoard.setValue(true);
            roomId.setValue(s);
        }
    }

    private class JoinRoomObserver extends BaseObserver<Boolean> {
        @Override
        public void onNext(Boolean aBoolean) {
            Log.e(TAG, "onNext: "+flipBoard.getValue() );
        }
    }

    private class DoNothingObserver extends BaseObserver<Move> {

    }

    private class StartGameObserver extends BaseObserver<Boolean>{

    }


}
