package com.toolsforfools.checkers.presention.ui.game.board.online;

import android.os.Bundle;

import com.toolsforfools.checkers.App;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityOnlineBoardBinding;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.board.online.fragments.OnlineBoardFragment;
import com.toolsforfools.checkers.presention.ui.game.board.online.fragments.RoomControlFragment;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class OnlineBoardActivity extends MVVMActivity<OnlineBoardViewModel, ActivityOnlineBoardBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected OnlineBoardViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(OnlineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_online_board;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        NavigationUtils.addFragment(getSupportFragmentManager(),R.id.container,new RoomControlFragment());
        OnlineBoardFragment onlineBoardFragment = new OnlineBoardFragment();
        viewModel.gameStarted.observe(this,gameStarted->{
            if(gameStarted){
                NavigationUtils.addFragment(getSupportFragmentManager(),R.id.container,onlineBoardFragment);
            }
        });
    }
}
