package com.toolsforfools.checkers.presention.ui.game.board.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.presention.callback.OnItemClickListener;
import com.toolsforfools.checkers.presention.ui.game.board.adapter.LoadedGameAdapter;
import com.toolsforfools.checkers.util.constants.SharedPrefKey;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

public class LoadGameDialog extends DialogFragment {

    private ISharedPref sharedPref;
    private OnItemClickListener<SaveModel> onItemClickListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_load_game, container, false);
    }

    public LoadGameDialog setSharedPref(ISharedPref sharedPref) {
        this.sharedPref = sharedPref;
        return this;
    }

    public LoadGameDialog setOnItemClickListener(OnItemClickListener<SaveModel> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        return this;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        List<SaveModel> loadedGames = sharedPref.loadGames();
        RecyclerView recyclerView = view.findViewById(R.id.loaded_games_list);
        LoadedGameAdapter adapter = new LoadedGameAdapter(getContext(),onItemClickListener);
        recyclerView.setAdapter(adapter);
        adapter.submitData(loadedGames);
    }
}
