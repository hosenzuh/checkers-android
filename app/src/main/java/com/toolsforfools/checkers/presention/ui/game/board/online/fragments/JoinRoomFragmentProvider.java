package com.toolsforfools.checkers.presention.ui.game.board.online.fragments;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class JoinRoomFragmentProvider {

    @ContributesAndroidInjector()
    abstract JoinRoomFragment injectRoomControlFragment();
}
