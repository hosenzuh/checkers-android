package com.toolsforfools.checkers.presention.checkers;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.gameView.IGameView;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;

import java.util.List;

public class GameViewImpl implements IGameView {


    @Override
    public void displayMove(Position oldPosition, Position newPosition, Move move) {

    }

    @Override
    public void removePiece(Position position) {

    }

    @Override
    public void upgradePiece(Position position) {

    }

    @Override
    public void putPieceInPosition(PieceType pieceType, Position position) {

    }

    @Override
    public void startPlayerTurn(Player.PlayerType playerType) {

    }

    @Override
    public void displayPlayerWin() {

    }

    @Override
    public void displayDraw() {

    }

    @Override
    public void displayMovablePieces(List<Piece> movablePieces) {

    }
}
