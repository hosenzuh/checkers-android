package com.toolsforfools.checkers.presention.ui.base.adapter;

import android.content.Context;
import android.graphics.Point;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseListAdapter<T, VH extends BaseViewHolder<T, ?>> extends RecyclerView.Adapter<VH> {

    private static int width = -1;
    protected Context context;
    private List<T> data;

    public BaseListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public void onBindViewHolder(@NonNull VH vh, int position) {
        T item = data != null && data.size() > position ? data.get(position) : null;
        vh.onBind(item, position);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public List<T> getData() {
        return data;
    }

    // Clear old data , then add the new data
    public void submitData(List<T> data) {
        if (data == null)
            return;
        if (this.data == null) {
            this.data = new ArrayList<>();
        }
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    //insert data to the existed one
    public void insertData(List<T> data) {
        if (data == null)
            return;
        if (this.data == null) {
            this.data = new ArrayList<>();
        }
        int insertIndex = this.data.size();
        this.data.addAll(data);
        notifyItemRangeInserted(insertIndex, data.size());
    }

    public void insertItem(T item) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.add(item);
        notifyItemInserted(data.size() - 1);
    }

    public void removeItem(T item) {
        if (item == null || data == null) return;

        int index = data.indexOf(item);

        if (index == -1) return;

        data.remove(index);
        notifyItemRemoved(index);
        notifyItemRangeRemoved(index, data.size() - 1);
    }

    public boolean isEmptyData() {
        return data == null || data.isEmpty();
    }

    protected int getScreenWidth() {
        if (width == -1) {
            WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);
            width = size.x;
        }
        return width;
    }

}