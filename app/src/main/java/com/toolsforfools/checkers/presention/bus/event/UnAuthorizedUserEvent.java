package com.toolsforfools.checkers.presention.bus.event;

public class UnAuthorizedUserEvent {

    private static final UnAuthorizedUserEvent INSTANCE = new UnAuthorizedUserEvent();

    public static final UnAuthorizedUserEvent getInstance() {
        return INSTANCE;
    }

    private UnAuthorizedUserEvent() {

    }
}
