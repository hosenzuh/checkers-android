package com.toolsforfools.checkers.presention.di.module;

import com.toolsforfools.checkers.data.remote.api.ApiEndPoints;
import com.toolsforfools.checkers.data.remote.api.RetrofitService;
import com.toolsforfools.checkers.data.remote.api.service.auth.AuthApiService;
import com.toolsforfools.checkers.data.remote.api.service.auth.AuthApiServiceImpl;
import com.toolsforfools.checkers.data.remote.api.service.game.GameApiService;
import com.toolsforfools.checkers.data.remote.api.service.game.GameApiServiceImpl;
import com.toolsforfools.checkers.data.remote.api.service.game.OnlineGameApiService;
import com.toolsforfools.checkers.data.remote.api.service.game.OnlineGameApiServiceImpl;
import com.toolsforfools.checkers.data.remote.interceptor.AuthInterceptor;
import com.toolsforfools.checkers.data.remote.socket.SocketConstants;

import java.net.URISyntaxException;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.socket.client.IO;
import io.socket.client.Socket;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    Retrofit provideRetrofit(@Named("base_url") String baseUrl, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


    @Provides
    @Singleton
    OkHttpClient provideHttpClient(AuthInterceptor authInterceptor) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(authInterceptor)
                .build();
    }

    @Provides
    @Singleton
    RetrofitService provideRetrofitService(Retrofit retrofit) {
        return retrofit.create(RetrofitService.class);
    }

    @Provides
    @Named("base_url")
    @Singleton
    String provideBaseUrl() {
        return ApiEndPoints.BASE_URL;
    }

    @Provides
    OnlineGameApiService provideOnlineGameApiService(OnlineGameApiServiceImpl onlineGameApiService) {
        return onlineGameApiService;
    }

    @Provides
    GameApiService provideGameApiService(GameApiServiceImpl gameApiService){
        return gameApiService;
    }

    @Provides
    @Named("socket_base_url")
    String provideSocketBaseUrl(){
        return SocketConstants.BASE_URL;
    }

    @Provides
    Socket provideSocketClient(@Named("socket_base_url")String baseUrl) {
        try {
            IO.Options options = new IO.Options();
            options.forceNew = true;
            options.query = "auth_token=";
            return IO.socket(baseUrl);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Provides
    @Singleton
    AuthApiService provideAuthService(AuthApiServiceImpl authApiService) {
        return authApiService;
    }


}
