package com.toolsforfools.checkers.presention.ui.game.board.online.fragments;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.FragmentRoomControlBinding;
import com.toolsforfools.checkers.presention.ui.base.view.fragment.MVVMFragment;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardViewModel;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class RoomControlFragment extends MVVMFragment<OnlineBoardViewModel, FragmentRoomControlBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected OnlineBoardViewModel provideViewModel() {
        return new ViewModelProvider(requireActivity(),factory).get(OnlineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_room_control;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        int containerId = ((ViewGroup)getView().getParent()).getId();
        mFragmentBinding.createRoomButton.setOnClickListener(view1 -> {
            NavigationUtils.addFragment(activity.getSupportFragmentManager(),containerId,new CreateRoomFragment(),true);
        });
        mFragmentBinding.joinRoomButton.setOnClickListener(view1 -> {
            NavigationUtils.addFragment(activity.getSupportFragmentManager(),containerId,new JoinRoomFragment(),true);
        });
    }
}
