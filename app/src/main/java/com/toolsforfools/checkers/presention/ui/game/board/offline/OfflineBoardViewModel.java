package com.toolsforfools.checkers.presention.ui.game.board.offline;

import android.util.Log;

import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.Move;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.domain.entity.DisplayMoveEntity;
import com.toolsforfools.checkers.domain.entity.PutPieceInPositionEntity;
import com.toolsforfools.checkers.domain.interactor.BaseObserver;
import com.toolsforfools.checkers.domain.interactor.usecase.CreateMultiPlayerGameUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.CreateSinglePlayerGameUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayDrawUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayMovablePieceUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayMoveUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.DisplayWinUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.GetAvailableMovesUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.LoadGameUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.MakeMoveUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.PutPieceInPositionUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.RemovePieceUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.SaveGameUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.StartTurnUseCase;
import com.toolsforfools.checkers.domain.interactor.usecase.UpgradePieceUseCase;
import com.toolsforfools.checkers.presention.model.CellType;
import com.toolsforfools.checkers.presention.model.CellTypeModel;
import com.toolsforfools.checkers.presention.model.MovablePieces;
import com.toolsforfools.checkers.presention.model.PieceTypeModel;
import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

import static com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity.CELLS_NUMBER;

public class OfflineBoardViewModel extends BaseViewModel {

    MutableLiveData<CellTypeModel> cellTypeMutable = new MutableLiveData(new CellTypeModel(CellType.NONE, 1, 1));
    MutableLiveData<PieceTypeModel> pieceTypeMutable = new MutableLiveData(new PieceTypeModel(PieceType.NULL, 1, 1));
    CellType[][] cellTypes = new CellType[20][20];
    PieceType[][] pieceTypes = new PieceType[20][20];

    MutableLiveData<Player.PlayerType> playerTurn = new MutableLiveData<>(Player.PlayerType.White);
    MutableLiveData<Player.PlayerType> isWinner = new MutableLiveData<>();
    MutableLiveData<Move> moveMutableLiveData = new MutableLiveData<>();
    private Player.PlayerType localPlayer;
    public MutableLiveData<Piece> lastClickedPiece = new MutableLiveData<>(new Piece(new Position(1, 1), PieceType.NULL));
    public MutableLiveData<MovablePieces> movablePieces = new MutableLiveData<>(new MovablePieces(Collections.emptyList(), true));
    public MutableLiveData<Boolean> isEatMoveDisplayed = new MutableLiveData<>();
    String TAG = "VIEWMODEL";
    private List<Move> availableMoves;
    private SaveModel saveModel;
    private ISharedPref sharedPref;
    private Position lastSelectedCell;
    private Position lastPieceMoved = null;
    MutableLiveData<Position> lastSelectedCellMutable = new MutableLiveData<>();
    MutableLiveData<Position> lastPieceMovedMutable = new MutableLiveData<>();

    // UseCases
    private CreateSinglePlayerGameUseCase createSinglePlayerGameUseCase;
    private CreateMultiPlayerGameUseCase createMultiPlayerGameUseCase;
    private LoadGameUseCase loadGameUseCase;
    private SaveGameUseCase saveGameUseCase;
    private MakeMoveUseCase makeMoveUseCase;
    private GetAvailableMovesUseCase getAvailableMovesUseCase;
    private PutPieceInPositionUseCase putPieceInPositionUseCase;
    private DisplayDrawUseCase displayDrawUseCase;
    private DisplayMovablePieceUseCase displayMovablePieceUseCase;
    private DisplayMoveUseCase displayMoveUseCase;
    private DisplayWinUseCase displayWinUseCase;
    private StartTurnUseCase startTurnUseCase;
    private RemovePieceUseCase removePieceUseCase;
    private UpgradePieceUseCase upgradePieceUseCase;

    @Inject
    public OfflineBoardViewModel(ISharedPref sharedPref, CreateSinglePlayerGameUseCase createSinglePlayerGameUseCase, CreateMultiPlayerGameUseCase createMultiPlayerGameUseCase, LoadGameUseCase loadGameUseCase, MakeMoveUseCase makeMoveUseCase, GetAvailableMovesUseCase getAvailableMovesUseCase, PutPieceInPositionUseCase putPieceInPositionUseCase, DisplayDrawUseCase displayDrawUseCase, DisplayMovablePieceUseCase displayMovablePieceUseCase, DisplayMoveUseCase displayMoveUseCase, DisplayWinUseCase displayWinUseCase, StartTurnUseCase startTurnUseCase, RemovePieceUseCase removePieceUseCase, UpgradePieceUseCase upgradePieceUseCase, SaveGameUseCase saveGameUseCase) {
        this.sharedPref = sharedPref;
        this.createSinglePlayerGameUseCase = createSinglePlayerGameUseCase;
        this.createMultiPlayerGameUseCase = createMultiPlayerGameUseCase;
        this.loadGameUseCase = loadGameUseCase;
        this.makeMoveUseCase = makeMoveUseCase;
        this.getAvailableMovesUseCase = getAvailableMovesUseCase;
        this.putPieceInPositionUseCase = putPieceInPositionUseCase;
        this.displayDrawUseCase = displayDrawUseCase;
        this.displayMovablePieceUseCase = displayMovablePieceUseCase;
        this.displayMoveUseCase = displayMoveUseCase;
        this.displayWinUseCase = displayWinUseCase;
        this.startTurnUseCase = startTurnUseCase;
        this.removePieceUseCase = removePieceUseCase;
        this.upgradePieceUseCase = upgradePieceUseCase;
        this.saveGameUseCase = saveGameUseCase;
        registerUseCase(createSinglePlayerGameUseCase,
                createMultiPlayerGameUseCase,
                loadGameUseCase,
                makeMoveUseCase,
                getAvailableMovesUseCase,
                putPieceInPositionUseCase,
                displayDrawUseCase,
                displayMovablePieceUseCase,
                displayMoveUseCase,
                displayWinUseCase,
                startTurnUseCase,
                removePieceUseCase,
                upgradePieceUseCase);
        setupObservers();
    }

    private void initBoardState() {
        for (int row = 1; row < CELLS_NUMBER; row++) {
            for (int column = 1; column < CELLS_NUMBER; column++) {
                Log.e(TAG, "initBoardState: " + row + " " + column);
                cellTypes[row][column] = CellType.NONE;
                pieceTypes[row][column] = PieceType.NULL;
            }
        }
    }

    private void setupObservers() {
        putPieceInPositionUseCase.execute(new PutPieceInPositionObserver());
        displayDrawUseCase.execute(new DisplayDrawObserver());
        displayMovablePieceUseCase.execute(new DisplayMovablePieceObserver());
        displayMoveUseCase.execute(new DisplayMoveObserver());
        displayWinUseCase.execute(new DisplayWinObserver());

        removePieceUseCase.execute(new RemovePieceObserver());
        upgradePieceUseCase.execute(new UpgradePieceObserver());
    }


    public void createGame(boolean isSingle, int difficultyLevel, Player.PlayerType playerType, boolean isLoading, SaveModel saveModel) {
        initBoardState();
        if (isLoading) {
            loadGameUseCase.execute(new CreateGameObserver(), LoadGameUseCase.LoadGameParams.create(saveModel));
        } else if (isSingle) {
            createSinglePlayerGameUseCase.execute(new CreateGameObserver(),
                    CreateSinglePlayerGameUseCase.CreateSinglePlayerGameParams.create(playerType, difficultyLevel));
            this.localPlayer = playerType;
        } else {
            createMultiPlayerGameUseCase.execute(new CreateGameObserver());
        }
        startTurnUseCase.execute(new StartTurnObserver());

    }


    void checkMoves(int row, int column) {
        Log.e(TAG, "checkMoveAvailable: ");
        clearAvailableMoves();
        if (!checkMoveAvailable(row, column))
            return;

        lastClickedPiece.setValue(new Piece(new Position(row, column), PieceType.blackPiece));

        Position piecePosition = new Position(row, column);
        getAvailableMovesUseCase.execute(new GetAvailableMovesObserver(),
                GetAvailableMovesUseCase.GetAvailableMovesParams.create(piecePosition));

    }

    private boolean checkMoveAvailable(int row, int column) {
        if (localPlayer != null && localPlayer != playerTurn.getValue())
            return false;
        Player.PlayerType playerType = playerTurn.getValue();
        PieceType pieceType = pieceTypes[row][column];
        Log.e(TAG, "checkMoveAvailable: " + pieceType + "  " + playerType);
        return playerType.equals(Player.PlayerType.Black) ? pieceType.equals(PieceType.blackKing) || pieceType.equals(PieceType.blackPiece)
                : pieceType.equals(PieceType.whiteKing) || pieceType.equals(PieceType.whitePiece);
    }

    void makeMove(int row, int column) {
        Log.e(TAG, "makeMove: ");

        for (int i = 0; i < availableMoves.size(); i++) {
            if (availableMoves.get(i).getDestination().getX() == row && availableMoves.get(i).getDestination().getY() == column) {
                Move move = availableMoves.get(i);
                clearAvailableMoves();
                makeMoveUseCase.execute(new MakeMoveObserver(),
                        MakeMoveUseCase.MakeMoveParams.create(move));
                break;
            }
        }
    }

    public void saveGame() {
        saveGameUseCase.execute(new SaveGameObserver());
    }


    void clearAvailableMoves() {
        Log.e(TAG, "clearAvailableMoves: ");
        Piece piece = lastClickedPiece.getValue();
        piece.setType(PieceType.NULL);
        lastClickedPiece.setValue(piece);
        for (int i = 1; i < CELLS_NUMBER; i++) {
            for (int j = 1; j < CELLS_NUMBER; j++) {
                if (cellTypes[i][j].equals(CellType.AVAILABLE_MOVE)) {
                    cellTypes[i][j] = CellType.NONE;
                    cellTypeMutable.setValue(new CellTypeModel(CellType.NONE, i, j));
                }
            }
        }
    }

    // UseCase Observers
    private class CreateGameObserver extends BaseObserver<Boolean> {
        @Override
        public void onNext(Boolean created) {
        }
    }

    private class DisplayDrawObserver extends BaseObserver<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {

        }
    }

    private class DisplayWinObserver extends BaseObserver<Boolean> {

        @Override
        public void onNext(Boolean aBoolean) {
            isWinner.setValue(playerTurn.getValue());
        }
    }

    private class DisplayMovablePieceObserver extends BaseObserver<List<Piece>> {
        @Override
        public void onNext(List<Piece> pieces) {
            List<Piece> oldPieces = movablePieces.getValue().getPieces();
            movablePieces.setValue(new MovablePieces(oldPieces, true));
            movablePieces.setValue(new MovablePieces(pieces, false));
        }
    }

    private class DisplayMoveObserver extends BaseObserver<DisplayMoveEntity> {
        @Override
        public void onNext(DisplayMoveEntity displayMoveEntity) {
            Log.e(TAG, "onNext: " + displayMoveEntity.getMove());
            int oldX = displayMoveEntity.getOldPosition().getX();
            int oldY = displayMoveEntity.getOldPosition().getY();
            int newX = displayMoveEntity.getNewPosition().getX();
            int newY = displayMoveEntity.getNewPosition().getY();
            if (lastSelectedCell == null)
                lastSelectedCell = new Position(oldX, oldY);
            lastPieceMoved = new Position(newX,newY);
            PieceType pieceType = pieceTypes[oldX][oldY];
            cellTypes[oldX][oldY] = CellType.NONE;
            pieceTypes[oldX][oldY] = PieceType.NULL;
            cellTypes[newX][newY] = CellType.PIECE;
            pieceTypes[newX][newY] = pieceType;
            cellTypeMutable.setValue(new CellTypeModel(CellType.NONE, oldX, oldY));
            pieceTypeMutable.setValue(new PieceTypeModel(PieceType.NULL, oldX, oldY));
            cellTypeMutable.setValue(new CellTypeModel(CellType.PIECE, newX, newY));
            pieceTypeMutable.setValue(new PieceTypeModel(pieceType, newX, newY));
            moveMutableLiveData.setValue(displayMoveEntity.getMove());
        }
    }

    private class MakeMoveObserver extends BaseObserver<Boolean> {
        @Override
        public void onNext(Boolean made) {

        }
    }

    private class GetAvailableMovesObserver extends BaseObserver<List<Move>> {
        @Override
        public void onNext(List<Move> moves) {
            Log.e(TAG, "onNext: " + moves.size());
            availableMoves = moves;
            Position position;
            for (int i = 0; i < moves.size(); i++) {
                position = moves.get(i).getDestination();
                cellTypes[position.getX()][position.getY()] = CellType.AVAILABLE_MOVE;
                cellTypeMutable.setValue(new CellTypeModel(CellType.AVAILABLE_MOVE, position.getX(), position.getY()));
            }
        }
    }

    private class PutPieceInPositionObserver extends BaseObserver<PutPieceInPositionEntity> {

        @Override
        public void onNext(PutPieceInPositionEntity putPieceInPositionEntity) {
            Log.e(TAG, "onNext: " + putPieceInPositionEntity.getPieceType() + " " + putPieceInPositionEntity.getPosition().getX() + putPieceInPositionEntity.getPosition().getY());
            Position position = putPieceInPositionEntity.getPosition();
            cellTypes[position.getX()][position.getY()] = CellType.PIECE;
            pieceTypes[position.getX()][position.getY()] = putPieceInPositionEntity.getPieceType();
            cellTypeMutable.setValue(new CellTypeModel(CellType.PIECE, position.getX(), position.getY()));
            pieceTypeMutable.setValue(new PieceTypeModel(putPieceInPositionEntity.getPieceType(), position.getX(), position.getY()));
        }
    }

    private class StartTurnObserver extends BaseObserver<Player.PlayerType> {
        @Override
        public void onNext(Player.PlayerType playerType) {
            Log.e(TAG, "onNext: lasadas" + lastSelectedCell.getX() );
            lastPieceMovedMutable.setValue(lastPieceMoved);
            lastSelectedCellMutable.setValue(lastSelectedCell);
            lastSelectedCell = null;
            playerTurn.setValue(playerType);
        }

        @Override
        public void onError(Throwable e) {
            Log.e(TAG, "onError: " );
        }
    }

    private class RemovePieceObserver extends BaseObserver<Position> {
        @Override
        public void onNext(Position position) {
            cellTypes[position.getX()][position.getY()] = CellType.NONE;
            pieceTypes[position.getX()][position.getY()] = PieceType.NULL;
            cellTypeMutable.setValue(new CellTypeModel(CellType.NONE, position.getX(), position.getY()));
            pieceTypeMutable.setValue(new PieceTypeModel(PieceType.NULL, position.getX(), position.getY()));
        }
    }

    private class UpgradePieceObserver extends BaseObserver<Position> {
        @Override
        public void onNext(Position position) {
            PieceType pieceType1 = pieceTypes[position.getX()][position.getY()];
            if (pieceType1.equals(PieceType.whitePiece)) {
                pieceTypes[position.getX()][position.getY()] = PieceType.whiteKing;
                pieceTypeMutable.setValue(new PieceTypeModel(PieceType.whiteKing, position.getX(), position.getY()));
            } else {
                pieceTypes[position.getX()][position.getY()] = PieceType.blackKing;
                pieceTypeMutable.setValue(new PieceTypeModel(PieceType.blackKing, position.getX(), position.getY()));
            }
        }
    }

    private class SaveGameObserver extends BaseObserver<SaveModel> {
        @Override
        public void onNext(SaveModel saveModel) {
            saveModel.gameName = "PERFECT";
            sharedPref.saveGame(saveModel);
            showMessage("SAVED");
        }
    }

}
