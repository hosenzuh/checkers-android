package com.toolsforfools.checkers.presention.ui.game.board.online.fragments;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class CreateRoomFragmentProvider {

    @ContributesAndroidInjector()
    abstract CreateRoomFragment injectRoomControlFragment();
}
