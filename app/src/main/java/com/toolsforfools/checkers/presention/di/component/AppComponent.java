package com.toolsforfools.checkers.presention.di.component;


import android.app.Application;

import com.toolsforfools.checkers.App;
import com.toolsforfools.checkers.presention.di.module.ActivityModule;
import com.toolsforfools.checkers.presention.di.module.AppModule;
import com.toolsforfools.checkers.presention.di.module.DataSourceModule;
import com.toolsforfools.checkers.presention.di.module.GameModule;
import com.toolsforfools.checkers.presention.di.module.NetworkModule;
import com.toolsforfools.checkers.presention.di.module.RepositoryModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityModule.class,
        GameModule.class,
        RepositoryModule.class,
        NetworkModule.class,
        DataSourceModule.class

})

public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder app(Application application);

        AppComponent build();
    }

}
