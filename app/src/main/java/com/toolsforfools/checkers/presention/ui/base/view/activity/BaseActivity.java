package com.toolsforfools.checkers.presention.ui.base.view.activity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.toolsforfools.checkers.presention.callback.OnAnimationFinished;
import com.toolsforfools.checkers.presention.ui.base.IBaseView;
import com.toolsforfools.checkers.util.AnimationUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;

public abstract class BaseActivity extends AppCompatActivity implements IBaseView, HasAndroidInjector {

    @Inject
    DispatchingAndroidInjector<Object> androidInjector;

    private boolean isRunning;

    private List<View> viewsForSlidingUp = new ArrayList<>();
    private List<View> viewsForSlidingDown = new ArrayList<>();

    protected boolean isAnyClicked = false;

    protected abstract int getLayoutId();

    protected void onFetchParams() {
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        onFetchParams();
        onInject();
        super.onCreate(savedInstanceState);
        setView();
    }

    protected void registerForSlideUp(View... views) {
        if (views == null) return;
        viewsForSlidingUp.addAll(Arrays.asList(views));
    }

    protected void registerForDropDown(View... views) {
        if (views == null) return;
        viewsForSlidingDown.addAll(Arrays.asList(views));
    }

    protected void registerAllForSlideUp() {
        ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView().getRootView();
        registerAllChildren(viewGroup, true, false);
    }

    protected void registerAllForSlideDown() {
        ViewGroup viewGroup = (ViewGroup) getWindow().getDecorView();
        registerAllChildren(viewGroup, true, true);
    }

    protected void slideUp(OnAnimationFinished onAnimationFinished) {
        for (int i = 0; i < viewsForSlidingUp.size(); i++) {
            if (i != viewsForSlidingUp.size() - 1) {
                AnimationUtil.slideUpSpring(viewsForSlidingUp.get(i));
            } else {
                AnimationUtil.slideUpSpring(viewsForSlidingUp.get(i), onAnimationFinished);
            }
        }
    }

    protected void slidUpWithCheck(OnAnimationFinished onAnimationFinished) {
        if (isAnyClicked) return;

        isAnyClicked = true;
        slideUp(onAnimationFinished);
    }


    protected void slideUp() {
        for (int i = 0; i < viewsForSlidingUp.size(); i++) {
            AnimationUtil.slideUp(viewsForSlidingUp.get(i), this);
        }
    }


    protected void slideDown(OnAnimationFinished onAnimationFinished) {
        for (int i = 0; i < viewsForSlidingDown.size(); i++) {
            if (i != viewsForSlidingDown.size() - 1) {
                AnimationUtil.dropDown(viewsForSlidingDown.get(i), this);
            } else {
                AnimationUtil.dropDown(viewsForSlidingDown.get(i), this, onAnimationFinished);
            }
        }
    }

    protected void slideDown() {
        for (int i = 0; i < viewsForSlidingDown.size(); i++) {
            AnimationUtil.dropDownSpring(viewsForSlidingDown.get(i));
        }
    }

    private void registerAllChildren(View view, boolean isRoot, boolean isDown) {
        ViewGroup viewGroup = (ViewGroup) view;
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View child = viewGroup.getChildAt(i);
            if (child instanceof ViewGroup) registerAllChildren(child, false, isDown);
            else {
                if (isDown)
                    viewsForSlidingDown.add(child);
                else
                    viewsForSlidingUp.add(child);
            }
        }
    }

    protected void setView() {
        setContentView(getLayoutId());
    }

    protected void onInject() {
        AndroidInjection.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
        isAnyClicked = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected boolean isActivityRunning() {
        return isRunning;
    }

    // base methods
    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int stringId) {
        showMessage(getString(stringId));
    }

    @Override
    public void showDialogMessage(String title, String message) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, (dialog, id) -> dialog.dismiss())
                .setNegativeButton(android.R.string.cancel, (dialog, id) -> dialog.dismiss())
                .create().show();
    }

    @Override
    public void showDialogMessage(int title, int message) {
        showDialogMessage(getString(title), getString(message));
    }

    @Override
    public void showDialogMessage(String message) {
        showDialogMessage("", message);
    }

    @Override
    public void showDialogMessage(int message) {
        showDialogMessage(getString(message));
    }

    @Override
    public void hideKeyboard() {
        View currentFocus = getCurrentFocus();
        if (currentFocus == null)
            currentFocus = new View(this);
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
    }

    @Override
    public AndroidInjector<Object> androidInjector() {
        return androidInjector;
    }
}
