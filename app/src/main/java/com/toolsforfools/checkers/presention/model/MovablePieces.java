package com.toolsforfools.checkers.presention.model;

import com.toolsforfools.checkers.domain.checkersEngine.board.Piece;

import java.util.List;

public class MovablePieces {

    private List<Piece> pieces;
    private boolean isRemove;

    public MovablePieces(List<Piece> pieces, boolean isRemove) {
        this.pieces = pieces;
        this.isRemove = isRemove;
    }

    public List<Piece> getPieces() {
        return pieces;
    }

    public boolean isRemove() {
        return isRemove;
    }
}
