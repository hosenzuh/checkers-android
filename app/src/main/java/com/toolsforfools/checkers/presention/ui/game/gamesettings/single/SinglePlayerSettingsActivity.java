package com.toolsforfools.checkers.presention.ui.game.gamesettings.single;

import android.os.Bundle;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivitySinglePlayerSettingsBinding;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.presention.callback.OnAnimationFinished;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class SinglePlayerSettingsActivity extends MVVMActivity<SinglePlayerSettingsViewModel, ActivitySinglePlayerSettingsBinding> {

    @Inject
    ViewModelProvider.Factory factory;


    @Override
    protected SinglePlayerSettingsViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(SinglePlayerSettingsViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_single_player_settings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerForDropDown(mActivityBinding.optionsLayout);
        slideDown();
        registerForSlideUp(mActivityBinding.optionsLayout);
        mActivityBinding.startGameButton.setOnClickListener(
                view -> slideUp(onAnimationFinished)
        );
    }

    OnAnimationFinished onAnimationFinished = () -> NavigationUtils.goToActivity(this, OfflineBoardActivity.getOfflineBoardForSingle(this, viewModel.difficultyLevel.getValue(), viewModel.isBlack.getValue() ? Player.PlayerType.Black : Player.PlayerType.White));

    @Override
    protected void onResume() {
        super.onResume();
        slideDown();
    }
}
