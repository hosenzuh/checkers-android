package com.toolsforfools.checkers.presention.di.module;

import com.toolsforfools.checkers.presention.ui.auth.all.AllActivity;
import com.toolsforfools.checkers.presention.ui.auth.all.AllModule;
import com.toolsforfools.checkers.presention.ui.auth.login.LoginActivity;
import com.toolsforfools.checkers.presention.ui.auth.login.LoginActivityModule;
import com.toolsforfools.checkers.presention.ui.auth.signup.SignUpActivity;
import com.toolsforfools.checkers.presention.ui.auth.signup.SignUpActivityModule;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardModule;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardActivity;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardModule;
import com.toolsforfools.checkers.presention.ui.game.board.online.fragments.JoinRoomFragmentProvider;
import com.toolsforfools.checkers.presention.ui.game.board.online.fragments.CreateRoomFragmentProvider;
import com.toolsforfools.checkers.presention.ui.game.board.online.fragments.OnlineBoardFragmentProvider;
import com.toolsforfools.checkers.presention.ui.game.board.online.fragments.RoomControlFragmentProvider;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.multi.MultiPlayerSettingsActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.multi.MultiPlayerSettingsModule;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.single.SinglePlayerSettingsActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.single.SinglePlayerSettingsModule;
import com.toolsforfools.checkers.presention.ui.game.startgame.StartGameActivity;
import com.toolsforfools.checkers.presention.ui.game.startgame.StartGameModule;
import com.toolsforfools.checkers.presention.ui.main.MainActivity;
import com.toolsforfools.checkers.presention.ui.main.MainModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector(modules = MainModule.class)
    abstract MainActivity injectMainActivity();


    @ContributesAndroidInjector(modules = LoginActivityModule.class)
    abstract LoginActivity injectLoginActivity();

    @ContributesAndroidInjector(modules = SignUpActivityModule.class)
    abstract SignUpActivity injectSignUpActivity();

    @ContributesAndroidInjector(modules = AllModule.class)
    abstract AllActivity injectAllActivity();


    @ContributesAndroidInjector(modules = StartGameModule.class)
    abstract StartGameActivity injectStartGameActivity();

    @ContributesAndroidInjector(modules = SinglePlayerSettingsModule.class)
    abstract SinglePlayerSettingsActivity injectSinglePlayerSettingsActivity();

    @ContributesAndroidInjector(modules = OfflineBoardModule.class)
    abstract OfflineBoardActivity injectBoardActivity();

    @ContributesAndroidInjector(modules = MultiPlayerSettingsModule.class)
    abstract MultiPlayerSettingsActivity injectMultiPlayerSettingsActivity();

    @ContributesAndroidInjector(modules = {OnlineBoardModule.class, JoinRoomFragmentProvider.class, CreateRoomFragmentProvider.class, OnlineBoardFragmentProvider.class, RoomControlFragmentProvider.class})
    abstract OnlineBoardActivity injectOnlineBoardActivity();
}
