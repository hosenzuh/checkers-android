package com.toolsforfools.checkers.presention.ui.auth.signup;

import androidx.lifecycle.ViewModel;

import com.toolsforfools.checkers.presention.di.ViewModelKey;
import com.toolsforfools.checkers.presention.ui.auth.login.LoginViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class SignUpActivityModule {
    @ViewModelKey(SignUpViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindSignUpViewModel(SignUpViewModel viewModel);
}
