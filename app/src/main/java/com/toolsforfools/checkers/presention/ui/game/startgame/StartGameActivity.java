package com.toolsforfools.checkers.presention.ui.game.startgame;

import android.os.Bundle;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityStartGameBinding;
import com.toolsforfools.checkers.presention.callback.OnAnimationFinished;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.multi.MultiPlayerSettingsActivity;
import com.toolsforfools.checkers.presention.ui.game.gamesettings.single.SinglePlayerSettingsActivity;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class StartGameActivity extends MVVMActivity<StartGameViewModel, ActivityStartGameBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected StartGameViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(StartGameViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_start_game;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerAllForSlideDown();
        slideDown();
        registerAllForSlideUp();
        mActivityBinding.singlePlayerButton.setOnClickListener(
                view -> slideUp(onSinglePlayerAnimationFinished)
                );
        mActivityBinding.multiPlayerButton.setOnClickListener(
                view -> slideUp(onMultiPlayerAnimationFinished)
        );
        mActivityBinding.onlinePlayerButton.setOnClickListener(
                view -> slideUp(onOnlinePlayerAnimationFinished)
        );
    }

    OnAnimationFinished onSinglePlayerAnimationFinished = () -> NavigationUtils.goToActivity(this, SinglePlayerSettingsActivity.class);
    OnAnimationFinished onMultiPlayerAnimationFinished = ()-> NavigationUtils.goToActivity(this, MultiPlayerSettingsActivity.class);
    OnAnimationFinished onOnlinePlayerAnimationFinished = ()-> NavigationUtils.goToActivity(this, OnlineBoardActivity.class);

    @Override
    protected void onResume() {
        super.onResume();
        slideDown();
    }
}
