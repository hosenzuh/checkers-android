package com.toolsforfools.checkers.presention.bus;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class Bus {
    private static final Bus INSTANCE = new Bus();

    private PublishSubject<Object> publisher = PublishSubject.create();

    private Map<Consumer, Disposable> disposables = new HashMap<>();

    public static Bus getInstance() {
        return INSTANCE;
    }

    private Bus() {
    }

    public <T> void publish(T value) {
        publisher.onNext(value);
    }

    public <T> void register(Class<T> type, Observer<T> observer) {
        Disposable disposable = publisher.ofType(type)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

        disposables.put(observer, disposable);
    }

    public <T> void unregister(Observer<T> observer) {
        Disposable disposable = disposables.get(observer);
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }


    public interface Observer<T> extends Consumer<T> {

    }
}
