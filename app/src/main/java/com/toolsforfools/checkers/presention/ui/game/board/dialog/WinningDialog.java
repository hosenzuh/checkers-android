package com.toolsforfools.checkers.presention.ui.game.board.dialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.presention.ui.base.view.fragment.BaseFragment;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class WinningDialog extends DialogFragment {

    String winner;

    public WinningDialog() {
    }

    public WinningDialog setWinner(String winner) {
        this.winner = winner;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_winning,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView winnerName = view.findViewById(R.id.winner_name);
        winnerName.setText(winner+ " Won!");
    }
}
