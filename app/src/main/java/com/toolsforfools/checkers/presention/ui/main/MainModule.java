package com.toolsforfools.checkers.presention.ui.main;


import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MainModule {

    @ViewModelKey(MainViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindMainViewModel(MainViewModel viewModel);
}
