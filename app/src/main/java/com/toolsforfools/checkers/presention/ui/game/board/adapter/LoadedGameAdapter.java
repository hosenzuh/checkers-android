package com.toolsforfools.checkers.presention.ui.game.board.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ListItemLoadedGameBinding;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.presention.callback.OnItemClickListener;
import com.toolsforfools.checkers.presention.ui.base.adapter.BaseListAdapter;
import com.toolsforfools.checkers.presention.ui.base.adapter.BaseViewHolder;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

public class LoadedGameAdapter extends BaseListAdapter<SaveModel, LoadedGameAdapter.LoadedGameViewHolder> {

    OnItemClickListener<SaveModel> onItemClickListener;

    public LoadedGameAdapter(Context context, OnItemClickListener<SaveModel> onItemClickListener) {
        super(context);
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public LoadedGameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListItemLoadedGameBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.list_item_loaded_game,
                parent,
                false
        );
        return new LoadedGameViewHolder(binding);
    }

    public class LoadedGameViewHolder extends BaseViewHolder<SaveModel,ListItemLoadedGameBinding> {

        public LoadedGameViewHolder(@NonNull ListItemLoadedGameBinding itemView) {
            super(itemView);
        }

        @Override
        public void onBind(SaveModel item, int position) {
            binding.setItem(item);
            itemView.setOnClickListener(view -> onItemClickListener.onClick(item,position));
        }
    }
}
