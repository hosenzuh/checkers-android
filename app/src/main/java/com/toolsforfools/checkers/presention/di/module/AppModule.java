package com.toolsforfools.checkers.presention.di.module;


import android.app.Application;
import android.content.Context;

import com.toolsforfools.checkers.ViewModelProviderFactory;
import com.toolsforfools.checkers.data.executor.ThreadExecutorImpl;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.data.local.sharedpref.SharedPrefImpl;
import com.toolsforfools.checkers.data.remote.networkchecker.INetworkChecker;
import com.toolsforfools.checkers.data.remote.networkchecker.NetworkCheckerImpl;
import com.toolsforfools.checkers.domain.executor.ThreadExecutor;
import com.toolsforfools.checkers.domain.executor.UIExecutor;
import com.toolsforfools.checkers.presention.executor.UIExecutorImpl;

import javax.inject.Singleton;

import androidx.lifecycle.ViewModelProvider;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Singleton
    @Provides
    static Context provideContext(Application application) {
        return application;
    }

    @Singleton
    @Provides
    static ISharedPref provideSharedPref(SharedPrefImpl sharedPref) {
        return sharedPref;
    }

    @Singleton
    @Provides
    static INetworkChecker provideNetworkChecker(NetworkCheckerImpl networkChecker) {
        return networkChecker;
    }

    @Provides
    ViewModelProvider.Factory provideViewModelFactory(ViewModelProviderFactory factory) {
        return factory;
    }

    @Singleton
    @Provides
    UIExecutor provideUIExecutor(UIExecutorImpl uiExecutor) {
        return uiExecutor;
    }

    @Singleton
    @Provides
    ThreadExecutor provideThreadExecutor(ThreadExecutorImpl threadExecutor) {
        return threadExecutor;
    }

}
