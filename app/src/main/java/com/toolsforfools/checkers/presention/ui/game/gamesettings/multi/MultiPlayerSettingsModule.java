package com.toolsforfools.checkers.presention.ui.game.gamesettings.multi;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class MultiPlayerSettingsModule {

    @ViewModelKey(MultiPlayerSettingsViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindMultiPlayerSettingsViewModel(MultiPlayerSettingsViewModel viewModel);
}
