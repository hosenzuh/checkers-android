package com.toolsforfools.checkers.presention.ui.game.board.online.fragments;

import android.os.Bundle;
import android.view.View;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.FragmentConnectToRoomBinding;
import com.toolsforfools.checkers.databinding.FragmentRoomControlBinding;
import com.toolsforfools.checkers.presention.ui.base.view.fragment.MVVMFragment;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardViewModel;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class JoinRoomFragment extends MVVMFragment<OnlineBoardViewModel, FragmentConnectToRoomBinding> {

    @Inject
    ViewModelProvider.Factory factory;

    @Override
    protected OnlineBoardViewModel provideViewModel() {
        return new ViewModelProvider(requireActivity(),factory).get(OnlineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_connect_to_room;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mFragmentBinding.exitButton.setOnClickListener(view1 -> {
            getActivity().getSupportFragmentManager().popBackStack();
            viewModel.roomId.postValue("");
        });
        mFragmentBinding.joinRoomButton.setOnClickListener(view1 -> {
            viewModel.joinRoom();
        });
    }
}
