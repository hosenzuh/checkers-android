package com.toolsforfools.checkers.presention.ui.auth.signup;

import android.text.TextUtils;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.domain.entity.UserEntity;
import com.toolsforfools.checkers.domain.interactor.BaseObserver;
import com.toolsforfools.checkers.domain.interactor.Result;
import com.toolsforfools.checkers.domain.interactor.usecase.auth.SignUpUseCase;
import com.toolsforfools.checkers.presention.exception.ExceptionFactory;
import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;

public class SignUpViewModel extends BaseViewModel {
    public MutableLiveData<String> userName = new MutableLiveData<>("");
    public MutableLiveData<String> password = new MutableLiveData<>("");
    public MutableLiveData<String> confirmPassword = new MutableLiveData<>("");
    public MutableLiveData<String> email = new MutableLiveData<>("");

    private SignUpUseCase signUpUseCase;

    private MutableLiveData<Boolean> registerStatus = new MutableLiveData<>();


    @Inject
    public SignUpViewModel(SignUpUseCase signUpUseCase) {
        this.signUpUseCase = signUpUseCase;
    }


    public MutableLiveData<Boolean> getRegisterStatus() {
        return registerStatus;
    }

    public void signup() {
        if (!checkValues())
            return;

        hideKeyboard();
        startLoading();
        SignUpUseCase.SignUpParams signUpParams = SignUpUseCase.SignUpParams.create(
                userName.getValue(),
                email.getValue(),
                password.getValue(),
                confirmPassword.getValue()
        );
        signUpUseCase.execute(
                new SignUpObserver(),
                signUpParams
        );
    }

    private boolean checkValues() {
        if (isLoading.get()) {
            return false;
        }

        if (TextUtils.isEmpty(userName.getValue()) || TextUtils.isEmpty(email.getValue()) ||
                TextUtils.isEmpty(password.getValue()) || TextUtils.isEmpty(confirmPassword.getValue())) {
            showMessage(R.string.fill_all_fields);
            return false;
        }


        if (!password.getValue().equals(confirmPassword.getValue())) {
            showMessage("Password dosnot match confirm");
            return false;
        }

        return true;
    }

    private class SignUpObserver extends BaseObserver<Result<UserEntity>> {
        @Override
        public void onNext(Result<UserEntity> result) {
            if (result.getStatus() == Result.Status.SUCCESS)
                registerStatus.setValue(true);

        }

        @Override
        public void onError(Throwable e) {
            if (e.getMessage() != null)
                showMessage(e.getMessage());
            else
                showMessage(ExceptionFactory.getString(e));
            stopLoading();
        }

        @Override
        public void onComplete() {
            stopLoading();
        }
    }
}
