package com.toolsforfools.checkers.presention.ui.auth.all;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.toolsforfools.checkers.BR;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityAllBinding;
import com.toolsforfools.checkers.presention.ui.auth.login.LoginActivity;
import com.toolsforfools.checkers.presention.ui.auth.signup.SignUpActivity;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.main.MainActivity;
import com.toolsforfools.checkers.util.NavigationUtils;

import javax.inject.Inject;

public class AllActivity extends MVVMActivity<AllViewModel, ActivityAllBinding> {
    @Inject
    ViewModelProvider.Factory viewModelProviderFactory;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBinding.btnLogin.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        });

        mActivityBinding.btnSignup.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
            startActivity(intent);
        });

        mActivityBinding.settingsButton.setOnClickListener(view -> {
            NavigationUtils.goToActivity(this, MainActivity.class);
        });

    }

    @Override
    protected AllViewModel provideViewModel() {
        return new ViewModelProvider(this, viewModelProviderFactory).get(AllViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_all;
    }
}
