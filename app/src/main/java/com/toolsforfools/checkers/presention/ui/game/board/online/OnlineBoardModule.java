package com.toolsforfools.checkers.presention.ui.game.board.online;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class OnlineBoardModule {

    @ViewModelKey(OnlineBoardViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindsOnlineBoardModel(OnlineBoardViewModel viewModel);
}
