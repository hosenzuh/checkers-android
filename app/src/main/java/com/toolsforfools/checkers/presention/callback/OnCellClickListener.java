package com.toolsforfools.checkers.presention.callback;

import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.presention.model.CellType;

public interface OnCellClickListener {
    void onClick(int row, int column, PieceType pieceType, CellType cellType);
}
