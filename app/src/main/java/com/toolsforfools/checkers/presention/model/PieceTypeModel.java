package com.toolsforfools.checkers.presention.model;

import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;

public class PieceTypeModel {
    PieceType pieceType;
    int x,y;

    public PieceTypeModel(PieceType pieceType, int x, int y) {
        this.pieceType = pieceType;
        this.x = x;
        this.y = y;
    }

    public PieceType getPieceType() {
        return pieceType;
    }

    public void setPieceType(PieceType pieceType) {
        this.pieceType = pieceType;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
