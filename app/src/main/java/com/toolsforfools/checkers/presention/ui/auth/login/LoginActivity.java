package com.toolsforfools.checkers.presention.ui.auth.login;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityLoginBinding;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.main.MainActivity;

import javax.inject.Inject;

public class LoginActivity extends MVVMActivity<LoginViewModel, ActivityLoginBinding> {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerObservers();
    }

    private void registerObservers(){
        viewModel.getLoginStatus().observe(this, aBoolean -> {
            if (aBoolean){
               goToMainActivity();
            }
        });

    }

    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finishAffinity();
    }
    @Inject
    ViewModelProvider.Factory viewModelProviderFactory;

    @Override
    protected LoginViewModel provideViewModel() {
        return new ViewModelProvider(this, viewModelProviderFactory).get(LoginViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_login;
    }
}
