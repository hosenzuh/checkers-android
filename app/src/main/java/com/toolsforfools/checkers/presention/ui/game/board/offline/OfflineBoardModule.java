package com.toolsforfools.checkers.presention.ui.game.board.offline;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class OfflineBoardModule {

    @ViewModelKey(OfflineBoardViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindBoardViewModel(OfflineBoardViewModel viewModel);
}
