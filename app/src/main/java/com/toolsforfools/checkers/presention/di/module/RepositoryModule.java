package com.toolsforfools.checkers.presention.di.module;

import com.toolsforfools.checkers.data.repository.AuthRepositoryImpl;
import com.toolsforfools.checkers.data.repository.GameRepositoryImpl;
import com.toolsforfools.checkers.data.repository.OnlineGameRepositoryImpl;
import com.toolsforfools.checkers.domain.repository.AuthRepository;
import com.toolsforfools.checkers.domain.repository.GameRepository;
import com.toolsforfools.checkers.domain.repository.OnlineGameRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryModule {

    @Binds
    abstract OnlineGameRepository bindOnlineGameRepository(OnlineGameRepositoryImpl repository);

    @Singleton
    @Binds
    abstract GameRepository bindGameRepository(GameRepositoryImpl repository);


    @Binds
    @Singleton
    abstract AuthRepository bindAuthRepository(AuthRepositoryImpl repository);


}
