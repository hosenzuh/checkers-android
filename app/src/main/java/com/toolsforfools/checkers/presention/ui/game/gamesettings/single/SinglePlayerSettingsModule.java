package com.toolsforfools.checkers.presention.ui.game.gamesettings.single;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class SinglePlayerSettingsModule {

    @ViewModelKey(SinglePlayerSettingsViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindSinglePlayerSettingsViewModel(SinglePlayerSettingsViewModel viewModel);
}
