package com.toolsforfools.checkers.presention.ui.auth.signup;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.toolsforfools.checkers.BR;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivitySignupBinding;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;
import com.toolsforfools.checkers.presention.ui.main.MainActivity;

import javax.inject.Inject;

public class SignUpActivity extends MVVMActivity<SignUpViewModel, ActivitySignupBinding> {

    @Inject
    ViewModelProvider.Factory factory;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerObservers();
    }


    private void registerObservers(){
        viewModel.getRegisterStatus().observe(this, aBoolean -> {
            if (aBoolean){
                goToMainActivity();
            }
        });
    }



    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finishAffinity();
    }
    @Override
    protected SignUpViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(SignUpViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_signup;
    }
}
