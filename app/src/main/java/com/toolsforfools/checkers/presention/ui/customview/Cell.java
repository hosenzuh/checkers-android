package com.toolsforfools.checkers.presention.ui.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.presention.callback.OnCellClickListener;
import com.toolsforfools.checkers.presention.model.CellType;

import androidx.appcompat.widget.AppCompatButton;

public class Cell extends AppCompatButton implements View.OnClickListener {

    private boolean isBeige;
    private boolean isAvailable;
    private PieceType pieceType;
    private CellType cellType;
    private int row;
    private int column;
    private OnCellClickListener onCellClickListener;

    private static final String TAG = "CELL";
    private boolean isLastSelected = false;

    public Cell(Context context, int row, int column, boolean isBeige, OnCellClickListener onCellClickListener) {
        super(context);
        this.row = row;
        this.column = column;
        this.onCellClickListener = onCellClickListener;
        this.isBeige = isBeige;
        setParams();
        setOnClickListener(this::onClick);
    }

    public void setView(PieceType pieceType, CellType cellType) {
        this.pieceType = pieceType;
        this.cellType = cellType;
        setView();
    }


    public void setView() {

        if (pieceType == PieceType.NULL) {
            setEmpty();
            return;
        } else if (isAvailable) {
            setPieceAvailable();
        } else if (isBeige) {
            switch (pieceType) {
                case whitePiece:
                    setWhite();
                    break;
                case blackPiece:
                    setBlack();
                    break;
                case blackKing:
                    setBlackKing();
                    break;
                case whiteKing:
                    setWhiteKing();
                    break;
            }
        } else {
            setBrownBackground();
        }
    }

    @SuppressLint("ResourceAsColor")
    private void setBeigeBackground() {
        Log.e(TAG, "setBeigeBackground: ");
        setBackground(getResources().getDrawable(R.drawable.ic_beige_cell));
    }

    @SuppressLint("ResourceAsColor")
    private void setBrownBackground() {
        Log.e(TAG, "setBrownBackground: " + row + "  " + column);
        setBackground(getResources().getDrawable(R.drawable.ic_brown_piece));
    }

    private void setBlack() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_piece));
    }

    private void setWhite() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_piece));
    }

    private void setBlackKing() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_king_piece));
    }

    private void setWhiteKing() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_king_piece));
    }

    public void setPieceClicked() {
        switch (pieceType) {
            case whitePiece:
                setWhiteClicked();
                break;
            case blackPiece:
                setBlackClicked();
                break;
            case blackKing:
                setBlackKingClicked();
                break;
            case whiteKing:
                setWhiteKingClicked();
                break;
        }
    }

    public void removePieceClicked() {
        setView();
    }

    public void setPieceAvailable() {
        isAvailable = true;
        switch (pieceType) {
            case whitePiece:
                setWhiteAvailable();
                break;
            case blackPiece:
                setBlackAvailable();
                break;
            case blackKing:
                setBlackKingAvailable();
                break;
            case whiteKing:
                setWhiteKingAvailable();
                break;
        }
    }

    public void removePieceAvailable() {
        isAvailable = false;
        if (isLastSelected)
            setCellLastSelected();
        else
            setView();
    }

    public void setPieceLastMoved() {
        switch (pieceType) {
            case whitePiece:
                setWhiteLastMoved();
                break;
            case blackPiece:
                setBlackLastMoved();
                break;
            case blackKing:
                setBlackKingLastMoved();
                break;
            case whiteKing:
                setWhiteKingLastMoved();
                break;
        }
    }
    public void removePieceLastMoved(){
        setView();
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    private void setBlackAvailable() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_piece_available));
    }

    private void setWhiteAvailable() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_piece_available));
    }

    private void setBlackKingAvailable() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_king_piece_available));
    }

    private void setWhiteKingAvailable() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_king_piece_available));
    }

    private void setBlackClicked() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_piece_clicked));
    }

    private void setWhiteClicked() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_piece_clicked));
    }

    private void setBlackKingClicked() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_king_piece_clicked));
    }

    private void setWhiteKingClicked() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_king_piece_clicked));
    }

    private void setBlackLastMoved() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_piece_last_moved));
    }

    private void setWhiteLastMoved() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_piece_last_moved));
    }

    private void setBlackKingLastMoved() {
        setBackground(getResources().getDrawable(R.drawable.ic_black_king_piece_last_moved));
    }

    private void setWhiteKingLastMoved() {
        setBackground(getResources().getDrawable(R.drawable.ic_white_king_piece_last_moved));
    }

    private void setCellLastSelected() {
        setBackground(getResources().getDrawable(R.drawable.ic_cell_last_selected));
    }

    private void setCellLastSelectedAndAvailable() {
        setBackground(getResources().getDrawable(R.drawable.ic_cell_last_selected_and_available));
    }

    private void setEmpty() {
        Log.e(TAG, "setEmpty: " + isBeige);
        if (isBeige)
            setBrownBackground();
        else
            setBeigeBackground();
    }

    public void setAvailableMove() {
        Log.e(TAG, "removeAvailableMove: "+isLastSelected );
        cellType = CellType.AVAILABLE_MOVE;
        if (isLastSelected)
            setCellLastSelectedAndAvailable();
        else
            setBackground(getResources().getDrawable(R.drawable.ic_available_move));
    }

    public void removeAvailableMove() {
        cellType = CellType.NONE;
        if (isLastSelected)
            setCellLastSelected();
        else
            setEmpty();
    }

    public void setEmptyCellLastSelected() {
        isLastSelected = true;
        setCellLastSelected();
    }

    public void removeEmptyCellLastSelected() {
        Log.e(TAG, "removeEmptyCellLastSelected: " +row + " "+  column);
        isLastSelected = false;
        setEmpty();
    }

    //view methods

    void setParams() {
        int width = (int) (getContext().getResources().getConfiguration().screenHeightDp + 4 * getContext().getResources().getDimension(R.dimen.margin_medium));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, width / 8, 1);
        layoutParams.setMargins(0, 0, 0, 0);
        setLayoutParams(layoutParams);
    }


    @Override
    public void onClick(View view) {
        onCellClickListener.onClick(row, column, pieceType, cellType);
    }
}
