package com.toolsforfools.checkers.presention.ui.game.board.online.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.toolsforfools.checkers.App;
import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.data.local.sharedpref.ISharedPref;
import com.toolsforfools.checkers.databinding.FragmentOnlineBoardBinding;
import com.toolsforfools.checkers.domain.checkersEngine.board.PieceType;
import com.toolsforfools.checkers.domain.checkersEngine.board.Position;
import com.toolsforfools.checkers.domain.checkersEngine.game.repository.SaveModel;
import com.toolsforfools.checkers.domain.checkersEngine.move.moveModel.EatMove;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.presention.callback.OnCellClickListener;
import com.toolsforfools.checkers.presention.model.CellType;
import com.toolsforfools.checkers.presention.ui.base.view.fragment.MVVMFragment;
import com.toolsforfools.checkers.presention.ui.customview.Cell;
import com.toolsforfools.checkers.presention.ui.game.board.dialog.SaveGameDialog;
import com.toolsforfools.checkers.presention.ui.game.board.dialog.WinningDialog;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardActivity;
import com.toolsforfools.checkers.presention.ui.game.board.offline.OfflineBoardViewModel;
import com.toolsforfools.checkers.presention.ui.game.board.online.OnlineBoardViewModel;
import com.toolsforfools.checkers.util.sounds.SoundsManager;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class OnlineBoardFragment extends MVVMFragment<OnlineBoardViewModel, FragmentOnlineBoardBinding> {

    private static final String IS_LOADING = "isLoading";
    @Inject
    ViewModelProvider.Factory factory;

    private static final String TAG = "ACTIVITY";

    public static final int CELLS_NUMBER = 9;
    private Cell[][] cells = new Cell[9][9];

    private Player.PlayerType playerType = Player.PlayerType.White;


    @Override
    protected OnlineBoardViewModel provideViewModel() {
        return new ViewModelProvider(requireActivity(), factory).get(OnlineBoardViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_online_board;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        App.getInstance().pauseMusic();
        createBoard();
        initBoard();
        setupObservers();
        viewModel.startGame();
    }

    private void initBoard() {
        for (int i = 1; i < CELLS_NUMBER; i++) {
            for (int j = 1; j < CELLS_NUMBER; j++) {
                cells[i][j].removeAvailableMove();
                cells[i][j].setView(PieceType.NULL, CellType.NONE);
            }
        }
    }


    private void setupObservers() {
        viewModel.cellTypeMutable.observe(requireActivity(), cellTypeModel -> {
            CellType cellType = cellTypeModel.getCellType();
            int row = cellTypeModel.getX();
            int column = cellTypeModel.getY();
            Log.e(TAG, "setupObservers: " +row+ "  " + column + " " + cellType);
            if (cellType.equals(CellType.AVAILABLE_MOVE))
                cells[row][column].setAvailableMove();
            else if (cellType.equals(CellType.NONE))
                cells[row][column].removeAvailableMove();
            else { //there is piece
                PieceType pieceType = viewModel.pieceTypes[row][column];
                Log.e(TAG, "setupObservers: " + pieceType);
                cells[row][column].setView(pieceType, CellType.PIECE);
            }
        });

        viewModel.pieceTypeMutable.observe(requireActivity(), pieceTypeModel -> {
            PieceType pieceType = pieceTypeModel.getPieceType();
            int row = pieceTypeModel.getX();
            int column = pieceTypeModel.getY();
            if (pieceType.isKing())
                SoundsManager.playKingSound(getContext());
            cells[row][column].setView(pieceType, viewModel.cellTypes[row][column]);
        });

        // move type observing (for sound)
        viewModel.moveMutableLiveData.observe(requireActivity(), move -> {

            Log.e(TAG, "setupObservers: " + (move instanceof EatMove));
            if (move instanceof EatMove) {
                SoundsManager.playEatSound(getContext());
            } else {
                SoundsManager.playMoveSound(getContext());
            }
        });

        // movable pieces observing
        viewModel.movablePieces.observe(requireActivity(), movablePieces -> {
            Position position;
            for (int k = 0; k < movablePieces.getPieces().size(); k++) {
                position = movablePieces.getPieces().get(k).getPosition();
                if (movablePieces.isRemove())
                    cells[position.getX()][position.getY()].removePieceAvailable();
                else
                    cells[position.getX()][position.getY()].setPieceAvailable();
            }
        });

        // Last clicked Piece observing
        final boolean[] isInit = {false};
        viewModel.lastClickedPiece.observe(requireActivity(), piece -> {
            if (piece.getType().equals(PieceType.NULL) && isInit[0])
                cells[piece.getPosition().getX()][piece.getPosition().getY()].removePieceClicked();
            else if (isInit[0])
                cells[piece.getPosition().getX()][piece.getPosition().getY()].setPieceClicked();
            isInit[0] = true;
        });

        // Winning observing
        viewModel.isWinner.observe(requireActivity(), playerType -> {
            new WinningDialog().setWinner(playerType.name()).show(getActivity().getSupportFragmentManager(), "win dialog");
        });

        // change turn observing
        viewModel.playerTurn.observe(requireActivity(), playerType -> {
            if (playerType.equals(Player.PlayerType.Black)) {
                mFragmentBinding.blackPlayerBoard.setText("Your turn");
                mFragmentBinding.whitePlayerBoard.setText("Waiting black player");
            } else {
                mFragmentBinding.whitePlayerBoard.setText("Your turn");
                mFragmentBinding.blackPlayerBoard.setText("Waiting white player");
            }
        });

        // flip board // temporary
        viewModel.flipBoard.observe(requireActivity(), flipped->{
            if(flipped)
                flipBoard();
        });

        Log.e(TAG, "setupObservers: SETUPED");
    }

    private void createBoard() {
        LinearLayout row;

        for (int i = 1; i < CELLS_NUMBER; i++) {

            row = new LinearLayout(getContext());

            for (int j = 1; j < CELLS_NUMBER; j++) {
                row.addView(createCell(i, j));
            }
            mFragmentBinding.boardLayout.addView(row);
        }

//        if (playerType.equals(Player.PlayerType.White)) {
//            flipBoard();
//            List<View> viewList = new ArrayList<>();
//            for (int i = 0; i < mFragmentBinding.activityLayout.getChildCount(); i++) {
//                viewList.add(mFragmentBinding.activityLayout.getChildAt(i));
//            }
//            mFragmentBinding.activityLayout.removeAllViews();
//            for (int i = 2; i >= 0; i--) {
//                mFragmentBinding.activityLayout.addView(viewList.get(i));
//            }
//            for (int i = 3; i < viewList.size(); i++) {
//                mFragmentBinding.activityLayout.addView(viewList.get(i));
//            }
//        }

    }

    boolean flipped = true;

    private Cell createCell(int row, int column) {
        OnCellClickListener onCellClickListener = (pieceRow, pieceColumn, pieceType, cellType) -> {
            if (cellType.equals(CellType.AVAILABLE_MOVE)) {
                viewModel.makeMove(pieceRow, pieceColumn);
//                flipBoard();
            } else if (cellType.equals(CellType.NONE)) {
                viewModel.clearAvailableMoves();
            } else {
                viewModel.checkMoves(pieceRow, pieceColumn);
            }
        };
        Cell cell = new Cell(getContext(), row, column, isBeige(row, column), onCellClickListener);
        cells[row][column] = cell;
        return cell;
    }

    private void flipBoard() {
        flipped = !flipped;
        if (!flipped) {
            mFragmentBinding.playersLayout.setRotation(180);
            mFragmentBinding.blackPlayerLayout.setRotation(180);
            mFragmentBinding.whitePlayerLayout.setRotation(180);
        } else
            mFragmentBinding.boardLayout.setRotation(0);
    }

    private boolean isBeige(int row, int column) {
        return (row % 2 != 0 && column % 2 != 0) || (row % 2 == 0 && column % 2 == 0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        App.getInstance().resumeMusic();
    }
}
