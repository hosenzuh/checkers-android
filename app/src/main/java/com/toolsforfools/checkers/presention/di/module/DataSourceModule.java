package com.toolsforfools.checkers.presention.di.module;

import com.toolsforfools.checkers.data.repository.datasource.GameRemoteDataSource;
import com.toolsforfools.checkers.data.repository.datasource.IGameRemoteDataSource;
import com.toolsforfools.checkers.data.repository.datasource.IOnlineGameRemoteDataSource;
import com.toolsforfools.checkers.data.repository.datasource.OnlineGameRemoteDataSource;
import com.toolsforfools.checkers.data.repository.datasource.auth.AuthRemoteDataSourceImpl;
import com.toolsforfools.checkers.data.repository.datasource.auth.IAuthRemoteDataSource;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class DataSourceModule {

    @Binds
    abstract IOnlineGameRemoteDataSource bindOnlineGameRemoteDataSource(OnlineGameRemoteDataSource dataSource);

    @Binds
    abstract IGameRemoteDataSource bindGameRemoteDataSource(GameRemoteDataSource dataSource);

    @Binds
    @Singleton
    abstract IAuthRemoteDataSource bindAuthRemoteDataSource(AuthRemoteDataSourceImpl dataSource);

}
