package com.toolsforfools.checkers.presention.ui.game.result;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.databinding.ActivityResultBinding;
import com.toolsforfools.checkers.domain.checkersEngine.player.Player;
import com.toolsforfools.checkers.presention.ui.base.view.activity.MVVMActivity;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.ViewModelProvider;

public class ResultActivity extends MVVMActivity<ResultViewModel, ActivityResultBinding> {

    private static final String WINNER = "winner";
    private String winnerType;

    @Inject
    ViewModelProvider.Factory factory;

    public static Intent getResultActivity(Context context, Player.PlayerType playerType) {
        Intent intent = new Intent(context, ResultActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(WINNER, playerType.toString());
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onFetchParams() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(WINNER) != null) {
            winnerType = bundle.getString(WINNER);
        }
    }

    @Override
    protected ResultViewModel provideViewModel() {
        return new ViewModelProvider(this, factory).get(ResultViewModel.class);
    }

    @Override
    protected int getViewModelId() {
        return BR.viewModel;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
