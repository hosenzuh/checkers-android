package com.toolsforfools.checkers.presention.callback;

public interface OnAnimationFinished {
    void onFinished();
}
