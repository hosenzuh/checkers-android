package com.toolsforfools.checkers.presention.ui.game.startgame;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class StartGameModule {

    @ViewModelKey(StartGameViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindStartGameViewModel(StartGameViewModel viewModel);
}
