package com.toolsforfools.checkers.presention.ui.game.result;

import com.toolsforfools.checkers.presention.di.ViewModelKey;

import androidx.lifecycle.ViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ResultModule {

    @ViewModelKey(ResultViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindsResultViewModel(ResultViewModel viewModel);
}
