package com.toolsforfools.checkers.presention.ui.auth.login;

import android.text.TextUtils;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.toolsforfools.checkers.R;
import com.toolsforfools.checkers.domain.entity.UserEntity;
import com.toolsforfools.checkers.domain.interactor.BaseObserver;
import com.toolsforfools.checkers.domain.interactor.Result;
import com.toolsforfools.checkers.domain.interactor.usecase.auth.LoginUseCase;
import com.toolsforfools.checkers.presention.exception.ExceptionFactory;
import com.toolsforfools.checkers.presention.ui.base.BaseViewModel;

import javax.inject.Inject;

public class LoginViewModel extends BaseViewModel {
    public MutableLiveData<String> userName = new  MutableLiveData<>("");
    public MutableLiveData<String> password = new  MutableLiveData<>("");

    private LoginUseCase loginUseCase;
    private MutableLiveData<Boolean> loginStatus = new MutableLiveData<>();


    @Inject
    public LoginViewModel(LoginUseCase loginUseCase) {
        this.loginUseCase = loginUseCase;
    }


    public void login(){
        if (!checkValues()) {
            return;
        }
        hideKeyboard();
        startLoading();
        loginUseCase.execute(
                new LoginObserver(),
        LoginUseCase.LoginParams.create(userName.getValue(), password.getValue()));
    }
    private boolean checkValues() {
        if (isLoading.get())
            return false;
        if (TextUtils.isEmpty(userName.getValue()) || TextUtils.isEmpty(password.getValue())) {
            showMessage(R.string.fill_all_fields);
            return false;
        }

        return true;
    }

    public LiveData<Boolean> getLoginStatus() {
        return loginStatus;
    }


    private class LoginObserver extends BaseObserver<Result<UserEntity>> {
        @Override
        public void onNext(Result<UserEntity> result) {
            stopLoading();
            if (result.getStatus() == Result.Status.SUCCESS) {
                loginStatus.setValue(true);
            }
        }

        @Override
        public void onComplete() {
            stopLoading();
        }

        @Override
        public void onError(Throwable e) {
            stopLoading();
            if (e.getMessage() != null)
                showMessage(e.getMessage());
            else
                showMessage(ExceptionFactory.getString(e));
        }
    }
}
