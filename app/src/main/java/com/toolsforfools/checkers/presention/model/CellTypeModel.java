package com.toolsforfools.checkers.presention.model;

public class CellTypeModel {

    private CellType cellType;
    private int x;
    private int y;

    public CellTypeModel(CellType cellType, int x, int y) {
        this.cellType = cellType;
        this.x = x;
        this.y = y;
    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
