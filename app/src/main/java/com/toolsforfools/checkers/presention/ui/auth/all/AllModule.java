package com.toolsforfools.checkers.presention.ui.auth.all;

import androidx.lifecycle.ViewModel;

import com.toolsforfools.checkers.presention.di.ViewModelKey;
import com.toolsforfools.checkers.presention.ui.auth.login.LoginViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class AllModule {
    @ViewModelKey(AllViewModel.class)
    @IntoMap
    @Binds
    abstract ViewModel bindAllViewModel(AllViewModel viewModel);
}
